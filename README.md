# apdtool

http://azsky2.html.xdomain.jp/

AzPainter/AzDrawing で保存された APD/ADW ファイルを処理する、コマンドラインツールです。

- レイヤを合成した一枚絵画像を PNG/BMP/PSD で出力。
- 各レイヤを画像として、PNG/BMP/PSD で出力。
- APD v4 以外のファイルを、APD v4 に変換して出力。<br>
(AzPainter ver 3.x で保存される最新の形式)
- レイヤの情報、または、レイヤテクスチャの画像パスを表示。

## 動作環境

Linux、macOS ほか

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
