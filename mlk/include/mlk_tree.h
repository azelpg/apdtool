/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_TREE_H
#define MLK_TREE_H

#ifdef __cplusplus
extern "C" {
#endif

/* mTree */

mTreeItem *mTreeAppendNew(mTree *p,mTreeItem *parent,int size);
mTreeItem *mTreeAppendNew_top(mTree *p,mTreeItem *parent,int size);
mTreeItem *mTreeInsertNew(mTree *p,mTreeItem *insert,int size);

void mTreeLinkBottom(mTree *p,mTreeItem *parent,mTreeItem *item);
void mTreeLinkTop(mTree *p,mTreeItem *parent,mTreeItem *item);
void mTreeLinkInsert(mTree *p,mTreeItem *ins,mTreeItem *item);
void mTreeLinkInsert_parent(mTree *p,mTreeItem *parent,mTreeItem *ins,mTreeItem *item);
void mTreeLinkRemove(mTree *p,mTreeItem *item);

void mTreeDeleteAll(mTree *p);
void mTreeDeleteItem(mTree *p,mTreeItem *item);
void mTreeDeleteChildren(mTree *p,mTreeItem *root);
void mTreeMoveItem(mTree *p,mTreeItem *src,mTreeItem *dst);
void mTreeMoveItemToTop(mTree *p,mTreeItem *src,mTreeItem *parent);
void mTreeMoveItemToBottom(mTree *p,mTreeItem *src,mTreeItem *parent);

mTreeItem *mTreeGetBottomLastItem(mTree *p);
mTreeItem *mTreeGetFirstItem(mTree *p,mTreeItem *parent);

int mTreeItemGetNum(mTree *p);
void mTreeSortChildren(mTree *p,mTreeItem *root,int (*comp)(mTreeItem *,mTreeItem *,void *),void *param);
void mTreeSortAll(mTree *p,int (*comp)(mTreeItem *,mTreeItem *,void *),void *param);

/* mTreeItem */

mTreeItem *mTreeItemNew(int size);

mTreeItem *mTreeItemGetNext(mTreeItem *p);
mTreeItem *mTreeItemGetNext_root(mTreeItem *p,mTreeItem *root);
mTreeItem *mTreeItemGetNextPass(mTreeItem *p);
mTreeItem *mTreeItemGetNextPass_root(mTreeItem *p,mTreeItem *root);

mTreeItem *mTreeItemGetPrev(mTreeItem *p);
mTreeItem *mTreeItemGetPrev_root(mTreeItem *p,mTreeItem *root);
mTreeItem *mTreeItemGetPrevPass(mTreeItem *p);

mTreeItem *mTreeItemGetBottom(mTreeItem *p);
mlkbool mTreeItemIsChild(mTreeItem *p,mTreeItem *parent);

#ifdef __cplusplus
}
#endif

#endif
