/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_ZLIB_H
#define MLK_ZLIB_H

typedef struct _mZlib mZlib;

#define MZLIB_WINDOWBITS_ZLIB  15
#define MZLIB_WINDOWBITS_ZLIB_NO_HEADER -15


#ifdef __cplusplus
extern "C" {
#endif

void mZlibFree(mZlib *p);
void mZlibSetIO_stdio(mZlib *p,void *fp);

mZlib *mZlibEncNew(int bufsize,int level,int windowbits,int memlevel,int strategy);
mZlib *mZlibEncNew_default(int bufsize,int level);
uint32_t mZlibEncGetSize(mZlib *p);
mlkbool mZlibEncReset(mZlib *p);
mlkerr mZlibEncSend(mZlib *p,void *buf,uint32_t size);
mlkerr mZlibEncFinish(mZlib *p);

mZlib *mZlibDecNew(int bufsize,int windowbits);
void mZlibDecSetSize(mZlib *p,uint32_t size);
void mZlibDecReset(mZlib *p);
mlkerr mZlibDecReadOnce(mZlib *p,void *buf,int bufsize,uint32_t insize);
mlkerr mZlibDecRead(mZlib *p,void *buf,int size);
mlkerr mZlibDecFinish(mZlib *p);

#ifdef __cplusplus
}
#endif

#endif
