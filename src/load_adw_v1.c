/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ADW ver 1 読み込み
 *****************************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_zlib.h"
#include "mlk_stdio.h"
#include "mlk_imagebuf.h"
#include "mlk_tree.h"

#include "def.h"
#include "tileimage.h"


//-----------------

typedef struct
{
	FILE *fp;
	mZlib *zlib;
	mImageBuf2 *img;
	
	int width,
		height,
		pitch;
}_data;

//-----------------


/* レイヤイメージ読み込み */

static mlkerr _read_layer_image(_data *p,LayerItem *pl,uint32_t cmpsize)
{
	uint8_t **ppbuf;
	int i,pitch;
	mlkerr ret;

	//全体のイメージを読み込み (8bit A)

	mZlibDecReset(p->zlib);
	mZlibDecSetSize(p->zlib, cmpsize);

	ppbuf = p->img->ppbuf;
	pitch = p->pitch;

	for(i = p->height; i; i--, ppbuf++)
	{
		ret = mZlibDecRead(p->zlib, *ppbuf, pitch);
		if(ret) return ret;
	}

	ret = mZlibDecFinish(p->zlib);
	if(ret) return ret;

	//イメージ作成

	pl->img = TileImage_new(TILEIMAGE_COLTYPE_ALPHA, p->width, p->height);
	if(!pl->img) return MLKERR_ALLOC;

	//変換

	if(!TileImage_convertFromImage_alpha8(pl->img, p->img->ppbuf, p->width, p->height))
		return MLKERR_ALLOC;

	return MLKERR_OK;
}

/* レイヤ読み込み */

static mlkerr _read_layer(FILE *fp,_data *p)
{
	LayerItem *pl;
	char name[32];
	int i;
	uint32_t size,col;
	uint8_t opacity,flags;
	mlkerr ret;

	for(i = g_work.img.layernum; i; i--)
	{
		//レイヤ情報

		if(mFILEreadOK(fp, name, 32)
			|| mFILEreadFormatLE(fp, "bbii", &opacity, &flags, &col, &size))
			return MLKERR_DAMAGED;

		//レイヤ追加
		// :最下層から順のため、上に追加していく。

		pl = layer_add_top();
		if(!pl) return MLKERR_ALLOC;

		//レイヤ名

		name[31] = 0;
		pl->name = dupname_shiftjis(name, -1);

		//セット

		pl->type = LAYERTYPE_ALPHA;
		pl->opacity = opacity;
		pl->col = col;

		if(flags & 1)
			pl->flags |= LAYERITEM_F_VISIBLE;

		//イメージ

		if(PROC_IS_SKIPIMAGE)
		{
			if(fseek(fp, size, SEEK_CUR))
				return MLKERR_DAMAGED;
		}
		else
		{
			ret = _read_layer_image(p, pl, size);
			if(ret) return ret;

			TileImage_setColor(pl->img, col);
		}
	}

	return MLKERR_OK;
}

/* 読み込みメイン */

static mlkerr _main_proc(FILE *fp,_data *p)
{
	uint32_t size;
	uint16_t imgw,imgh,layernum,layercnt,layersel;
	uint8_t comptype;

	//メイン情報

	if(mFILEreadFormatLE(fp, "hhhhhb",
		&imgw, &imgh, &layernum, &layercnt, &layersel, &comptype))
		return MLKERR_DAMAGED;

	if(comptype != 0)
		return MLKERR_INVALID_VALUE;
	
	p->width = imgw;
	p->height = imgh;
	p->pitch = (imgw + 3) & (~3);

	set_image_info(imgw, imgh, 8, 0, layernum);

	//プレビューイメージをスキップ

	if(mFILEreadLE32(fp, &size)
		|| fseek(fp, size, SEEK_CUR))
		return MLKERR_DAMAGED;
	
	//全体イメージ作成 (8bit)

	if(PROC_IS_NEEDIMAGE)
	{
		p->img = mImageBuf2_new(imgw, imgh, 8, -4);
		if(!p->img) return MLKERR_ALLOC;
	}

	//レイヤ読み込み

	return _read_layer(fp, p);
}

/** ADW v1 読み込み (LE) */

mlkerr load_adw_v1(FILE *fp)
{
	_data dat;
	mlkerr ret;

	mMemset0(&dat, sizeof(_data));

	dat.fp = fp;

	ret = init_zlib(&dat.zlib, fp, FALSE);
	if(ret) return ret;

	//

	ret = _main_proc(fp, &dat);

	//

	mZlibFree(dat.zlib);
	mImageBuf2_free(dat.img);

	return ret;
}

