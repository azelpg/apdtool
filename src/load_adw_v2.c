/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ADW ver 2 読み込み
 *****************************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_zlib.h"
#include "mlk_stdio.h"
#include "mlk_unicode.h"
#include "mlk_tree.h"

#include "def.h"
#include "tileimage.h"


//-----------------

typedef struct
{
	FILE *fp;
	mZlib *zlib;
}_data;

//-----------------



/* タイルイメージ読み込み */

static mlkerr _read_layer_image(_data *p,TileImage *img,uint32_t tilenum)
{
	FILE *fp = p->fp;
	uint8_t *buf;
	uint16_t tx,ty,size;
	mlkerr ret;

	for(; tilenum; tilenum--)
	{
		//タイル情報
		
		if(mFILEreadFormatLE(fp, "hhh", &tx, &ty, &size)
			|| size == 0
			|| size > 4096)
			return MLKERR_DAMAGED;

		//タイル確保

		buf = TileImage_getTileAlloc_atpos(img, tx, ty);
		if(!buf) return MLKERR_ALLOC;

		//タイル読み込み

		if(size == 4096)
		{
			if(mFILEreadOK(fp, buf, 4096))
				return MLKERR_DAMAGED;
		}
		else
		{
			mZlibDecReset(p->zlib);

			ret = mZlibDecReadOnce(p->zlib, buf, 4096, size);
			if(ret) return ret;
		}
	}

	return MLKERR_OK;
}

/* タイルイメージスキップ */

static mlkerr _skip_tiles(FILE *fp,uint32_t tilenum)
{
	uint16_t tx,ty,size;

	for(; tilenum; tilenum--)
	{
		if(mFILEreadFormatLE(fp, "hhh", &tx, &ty, &size)
			|| fseek(fp, size, SEEK_CUR))
			return MLKERR_DAMAGED;
	}

	return MLKERR_OK;
}

/* レイヤ読み込み */

static mlkerr _read_layer(FILE *fp,_data *p,int layerinfo_seek)
{
	LayerItem *pl;
	uint16_t name[25];
	mRect rc;
	int i;
	uint32_t col,tilenum;
	uint8_t opacity,flags;
	mlkerr ret;

	for(i = g_work.img.layernum; i; i--)
	{
		//レイヤ情報

		if(mFILEreadFormatLE(fp, "iiiii", &rc.x1, &rc.y1, &rc.x2, &rc.y2, &tilenum)
			|| mFILEreadArrayLE16(fp, name, 25) != 25
			|| mFILEreadFormatLE(fp, "bib", &opacity, &col, &flags)
			|| fseek(fp, 3 + layerinfo_seek, SEEK_CUR))
			return MLKERR_DAMAGED;
	
		//レイヤ追加
		// :最下層から順のため、上に追加していく。

		pl = layer_add_top();
		if(!pl) return MLKERR_ALLOC;

		//レイヤ名 (UTF-16 -> UTF-8)

		pl->name = mUTF16toUTF8_alloc(name, 24, NULL);

		//セット

		pl->type = LAYERTYPE_ALPHA;
		pl->opacity = opacity;
		pl->col = col;

		if(flags & 1)
			pl->flags |= LAYERITEM_F_VISIBLE;

		//イメージ

		if(PROC_IS_SKIPIMAGE)
		{
			ret = _skip_tiles(fp, tilenum);
			if(ret) return ret;
		}
		else
		{
			//イメージ作成

			pl->img = TileImage_newFromRect_forFile(TILEIMAGE_COLTYPE_ALPHA, &rc);
			if(!pl->img) return MLKERR_ALLOC;

			TileImage_setColor(pl->img, col);

			//読み込み

			ret = _read_layer_image(p, pl->img, tilenum);
			if(ret) return ret;
		}
	}

	return MLKERR_OK;
}

/* 読み込みメイン */

static mlkerr _main_proc(FILE *fp,_data *p)
{
	uint32_t size;
	uint16_t wsize,imgw,imgh,dpi,layernum,layersel,layerinfosize;

	//プレビューイメージをスキップ

	if(fseek(fp, 4, SEEK_CUR)	//w, h
		|| mFILEreadLE32(fp, &size)
		|| fseek(fp, size, SEEK_CUR))
		return MLKERR_DAMAGED;

	//メイン情報

	if(mFILEreadFormatLE(fp, "hhhhhhh",
		&wsize, &imgw, &imgh, &dpi, &layernum, &layersel, &layerinfosize)
		|| fseek(fp, wsize - 12, SEEK_CUR))
		return MLKERR_DAMAGED;

	set_image_info(imgw, imgh, 8, dpi, layernum);

	//レイヤ読み込み

	return _read_layer(fp, p, layerinfosize - 79);
}

/** ADW v2 読み込み */

mlkerr load_adw_v2(FILE *fp)
{
	_data dat;
	mlkerr ret;

	mMemset0(&dat, sizeof(_data));

	dat.fp = fp;

	ret = init_zlib(&dat.zlib, fp, FALSE);
	if(ret) return ret;

	//

	ret = _main_proc(fp, &dat);

	//

	mZlibFree(dat.zlib);

	return ret;
}
