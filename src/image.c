/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * 画像処理
 *****************************/

#include <string.h>

#include "mlk.h"
#include "mlk_tree.h"
#include "mlk_imagebuf.h"
#include "mlk_saveimage.h"
#include "mlk_util.h"

#include "def.h"
#include "tileimage.h"


/** 8bit色から 16bit 色を取得 */

void color_8to16(uint16_t *dst,uint32_t col)
{
	dst[0] = (int)(((col >> 16) & 255) / 255.0 * 0x8000 + 0.5);
	dst[1] = (int)(((col >> 8) & 255) / 255.0 * 0x8000 + 0.5);
	dst[2] = (int)((col & 255) / 255.0 * 0x8000 + 0.5);
}

/** ゼロクリア */

void image_clear0(mImageBuf2 *p)
{
	uint8_t **ppbuf;
	int iy,pitch;

	ppbuf = p->ppbuf;
	pitch = p->line_bytes;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		memset(*ppbuf, 0, pitch);
	}
}

/** 背景色でクリア (8bit) */

void image_clear8bit(mImageBuf2 *p,uint32_t col)
{
	uint8_t **ppbuf;
	uint32_t *pd;
	int ix,iy;

	col = mRGBAtoHostOrder(col);

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		pd = (uint32_t *)*ppbuf;

		for(ix = p->width; ix; ix--)
			*(pd++) = col;
	}
}

/** 背景色でクリア (16bit) */

void image_clear16bit(mImageBuf2 *p,uint32_t col)
{
	uint8_t **ppbuf;
	uint64_t *pd,c;
	uint16_t ar[4];
	int ix,iy;

	color_8to16(ar, col);
	ar[3] = 0;

	c = *((uint64_t *)ar);

	//

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		pd = (uint64_t *)*ppbuf;

		for(ix = p->width; ix; ix--)
			*(pd++) = c;
	}
}


//============================
// 画像ファイルに保存
//============================

static mFuncSaveImage g_funcs_save[] = {
	mSaveImagePNG, mSaveImageBMP, mSaveImagePSD
};


/* RGB -> GRAY 8bit に変換 */

static void _convert_rgb_to_gray(mImageBuf2 *p)
{
	uint8_t **ppbuf,*pd,*ps;
	int ix,iy;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		pd = ps = *ppbuf;

		for(ix = p->width; ix; ix--, ps += 3)
			*(pd++) = (ps[0] * 77 + ps[1] * 150 + ps[2] * 29) >> 8;
	}
}

/* RGB -> 1bit に変換 */

static void _convert_rgb_to_1bit(mImageBuf2 *p)
{
	uint8_t **ppbuf,*pd,*ps,f,val;
	int ix,iy,c;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		pd = ps = *ppbuf;
		f = 0x80;
		val = 0;

		for(ix = p->width; ix; ix--, ps += 3)
		{
			c = (ps[0] * 77 + ps[1] * 150 + ps[2] * 29) >> 8;
			
			if(c & 128)
				val |= f;

			f >>= 1;
			if(!f)
			{
				*(pd++) = val;
				f = 0x80, val = 0;
			}
		}

		if(f != 0x80)
			*pd = val;
	}
}

/* Y1行イメージセット */

static mlkerr _save_setrow(mSaveImage *p,int y,uint8_t *buf,int line_bytes)
{
	memcpy(buf, *((uint8_t **)p->param1 + y), line_bytes);

	return MLKERR_OK;
}

/* Y1行イメージセット (PSD) */

static mlkerr _save_setrow_ch(mSaveImage *p,int y,int ch,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;
	int i;

	ps = *((uint8_t **)p->param1 + y) + ch;

	if(p->coltype == MSAVEIMAGE_COLTYPE_GRAY)
		//GRAY
		memcpy(buf, ps, line_bytes);
	else if(p->samples_per_pixel == 3)
	{
		//RGB
	
		for(i = p->width; i; i--, ps += 3)
			*(buf++) = *ps;
	}
	else
	{
		//RGBA
	
		for(i = p->width; i; i--, ps += 4)
			*(buf++) = *ps;
	}

	return MLKERR_OK;
}

/** 画像ファイル書き込み */

mlkerr image_savefile(mImageBuf2 *img,const char *filename)
{
	mSaveImage si;
	int format;

	format = g_work.out_format;

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.width = img->width;
	si.height = img->height;
	si.bits_per_sample = 8;
	si.setrow = _save_setrow;
	si.setrow_ch = _save_setrow_ch;
	si.param1 = img->ppbuf;

	//解像度

	if(g_work.img.dpi)
	{
		si.reso_unit = MSAVEIMAGE_RESOUNIT_DPI;
		si.reso_horz = g_work.img.dpi;
		si.reso_vert = g_work.img.dpi;
	}

	//カラータイプ

	if(PROC_IS_LAYER)
	{
		//レイヤ (RGBA)

		si.coltype = MSAVEIMAGE_COLTYPE_RGB;
		si.samples_per_pixel = 4;
	}
	else if(format != OUTFORMAT_BMP && (g_work.flags & FLAGS_IMAGE_TO_MONO))
	{
		//GRAY 1bit

		si.coltype = MSAVEIMAGE_COLTYPE_GRAY;
		si.bits_per_sample = 1;
		si.samples_per_pixel = 1;

		_convert_rgb_to_1bit(img);
	}
	else if(format != OUTFORMAT_BMP && (g_work.flags & FLAGS_IMAGE_TO_GRAY))
	{
		//GRAY 8bit

		si.coltype = MSAVEIMAGE_COLTYPE_GRAY;
		si.samples_per_pixel = 1;

		_convert_rgb_to_gray(img);
	}
	else
	{
		//RGB

		si.coltype = MSAVEIMAGE_COLTYPE_RGB;
		si.samples_per_pixel = 3;
	}

	//保存

	return (g_funcs_save[format])(&si, NULL);
}


//============================
// レイヤ合成
//============================


/* 自身を含む合成対象アイテムを取得 */

static LayerItem *_get_blenditem(LayerItem *pi)
{
	while(pi)
	{
		if(!(pi->flags & LAYERITEM_F_VISIBLE))
			//非表示 -> 前のアイテムへ
			pi = (LayerItem *)mTreeItemGetPrevPass(MTREEITEM(pi));
		else
		{
			//表示

			if(pi->img)  //通常レイヤなら終了
				break;
			else if(pi->i.last)   //フォルダで子がある場合、最後の子
				pi = (LayerItem *)pi->i.last;
			else                 //フォルダで子がなければ、前のアイテム
				pi = (LayerItem *)mTreeItemGetPrevPass(MTREEITEM(pi));
		}
	}

	return pi;
}

/* 次の合成アイテム取得 */

static LayerItem *_get_blend_next(LayerItem *pi)
{
	pi = (LayerItem *)mTreeItemGetPrevPass(MTREEITEM(pi));

	if(pi)
		pi = _get_blenditem(pi);

	return pi;
}

/* 合成時の不透明度取得 (親の状態も適用) */

static int _get_blend_opacity(LayerItem *p)
{
	int n = p->opacity;

	for(p = (LayerItem *)p->i.parent; p; p = (LayerItem *)p->i.parent)
	{
		if(p->opacity != 128)
			n = (n * p->opacity + 64) >> 7;
	}

	return n;
}

/* 8bit RGBX -> RGB */

static void _convert_rgb8bit(mImageBuf2 *p)
{
	uint8_t **ppbuf,*ps,*pd;
	int ix,iy;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppbuf++);
		ps = pd;

		for(ix = p->width; ix; ix--, ps += 4, pd += 3)
		{
			pd[0] = ps[0];
			pd[1] = ps[1];
			pd[2] = ps[2];
		}
	}
}

/* 固定小数15bit -> 8bit 変換用のテーブルを作成 */

static uint8_t *_create_table_fix15_to_8(void)
{
	uint8_t *buf,*pd;
	int i;

	pd = buf = (uint8_t *)mMalloc(0x8000 + 1);
	if(!buf) return NULL;

	for(i = 0; i <= 0x8000; i++)
		*(pd++) = (uint8_t)((double)i / 0x8000 * 255 + 0.5);

	return buf;
}

/* 16bit RGBX -> 8bit RGB */

static void _convert_rgb16to8(mImageBuf2 *p)
{
	uint8_t **ppbuf,*pd,*tbl;
	uint16_t *ps;
	int ix,iy;

	tbl = _create_table_fix15_to_8();
	if(!tbl) return;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppbuf++);
		ps = (uint16_t *)pd;

		for(ix = p->width; ix; ix--, ps += 4, pd += 3)
		{
			pd[0] = tbl[ps[0]];
			pd[1] = tbl[ps[1]];
			pd[2] = tbl[ps[2]];
		}
	}

	mFree(tbl);
}

/** レイヤ合成
 *
 * 合成中は現在のビットで処理するため、現在のビット分のイメージが必要。
 * 最終的に、8bit RGB に変換する。 */

void image_layerblend(mImageBuf2 *imgdst)
{
	LayerItem *pi;
	mBox box;
	TileImageBlendSrcInfo info;

	box.x = box.y = 0;
	box.w = imgdst->width;
	box.h = imgdst->height;

	//背景色でクリア

	if(g_work.img.bits == 8)
		image_clear8bit(imgdst, g_work.img.bkgndcol);
	else
		image_clear16bit(imgdst, g_work.img.bkgndcol);

	//レイヤ合成

	pi = _get_blenditem((LayerItem *)g_work.list_layer.bottom);

	for(; pi; pi = _get_blend_next(pi))
	{
		//合成情報
		
		info.opacity = _get_blend_opacity(pi);
		info.blendmode = pi->blendmode;
		info.texitem = pi->texitem;
		info.tone_lines = 0;

		if((pi->flags & LAYERITEM_F_TONE)
			&& (pi->type == LAYERTYPE_GRAY || pi->type == LAYERTYPE_ALPHA1BIT))
		{
			info.tone_lines = pi->tone_lines;
			info.tone_angle = pi->tone_angle;
			info.tone_density = pi->tone_density;
			info.ftone_white = ((pi->flags & LAYERITEM_F_TONE_WHITE) != 0);
		}

		//合成

		TileImage_blend(pi->img, imgdst, &box, &info);
	}

	//変換

	if(g_work.img.bits == 8)
		_convert_rgb8bit(imgdst);
	else
		_convert_rgb16to8(imgdst);
}


//===========================
// サムネイル画像
//===========================


/* 縮小 (水平) */

static mlkbool _thumbnail_resize_horz(mImageBuf2 *p,int dstw)
{
	uint8_t *buf,**ppbuf,*ps,*pd,*psY;
	int ix,iy,x1,x2,r,g,b,num,srcw,pitchd,fx,finc;

	pitchd = dstw * 3;

	buf = (uint8_t *)mMalloc(pitchd);
	if(!buf) return FALSE;

	srcw = p->width;
	finc = (int)((double)srcw / dstw * (1<<12) + 0.5);

	//

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		//buf に縮小後の RGB セット
		
		psY = *ppbuf;
		pd = buf;
		fx = 0;

		for(ix = dstw; ix; ix--, pd += 3, fx += finc)
		{
			x1 = fx >> 12;
			x2 = (fx + finc + (1<<11)) >> 12;

			if(x2 > srcw) x2 = srcw;
			if(x1 == x2) x2++;
			
			num = x2 - x1;
			
			ps = psY + x1 * 3;
			r = g = b = 0;

			for(; x1 < x2; x1++, ps += 3)
			{
				r += ps[0];
				g += ps[1];
				b += ps[2];
			}

			pd[0] = r / num;
			pd[1] = g / num;
			pd[2] = b / num;
		}

		//コピー

		memcpy(psY, buf, pitchd);
	}

	mFree(buf);

	return TRUE;
}

/* 縮小 (垂直) */

static mlkbool _thumbnail_resize_vert(mImageBuf2 *p,int dstw,int dsth)
{
	uint8_t *buf,**ppbuf,*ps,*pd;
	int ix,iy,y1,y2,r,g,b,num,srch,xpos,fy,finc;

	buf = (uint8_t *)mMalloc(dsth * 3);
	if(!buf) return FALSE;

	srch = p->height;
	finc = (int)((double)srch / dsth * (1<<12) + 0.5);

	//

	xpos = 0;

	for(ix = dstw; ix; ix--, xpos += 3)
	{
		//buf に縮小後の RGB セット

		pd = buf;
		fy = 0;

		for(iy = dsth; iy; iy--, fy += finc)
		{
			y1 = fy >> 12;
			y2 = (fy + finc + (1<<11)) >> 12;

			if(y2 > srch) y2 = srch;
			if(y1 == y2) y2++;
			
			num = y2 - y1;
			
			ppbuf = p->ppbuf + y1;
			r = g = b = 0;

			for(; y1 < y2; y1++, ppbuf++)
			{
				ps = *ppbuf + xpos;
				
				r += ps[0];
				g += ps[1];
				b += ps[2];
			}

			pd[0] = r / num;
			pd[1] = g / num;
			pd[2] = b / num;
			pd += 3;
		}

		//Y1列にセット

		ps = buf;
		ppbuf = p->ppbuf;

		for(iy = dsth; iy; iy--, ps += 3, ppbuf++)
		{
			pd = *ppbuf + xpos;

			pd[0] = ps[0];
			pd[1] = ps[1];
			pd[2] = ps[2];
		}
	}

	mFree(buf);

	return TRUE;
}

/** (8bit) RGB のイメージから、RGB サムネイルイメージに変換
 *
 * 元サイズ <= 変換サイズの場合は、何もしない。 */

mlkbool image_conv_thumbnail(mImageBuf2 *p,int width,int height)
{
	if(p->width > width || p->height > height)
	{
		if(!_thumbnail_resize_horz(p, width))
			return FALSE;

		if(!_thumbnail_resize_vert(p, width, height))
			return FALSE;
	}

	return TRUE;
}


