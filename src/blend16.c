/*$
apdtool
Copyright (c) 2020 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * レイヤ合成 (16bit)
 *****************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_imagebuf.h"
#include "mlk_rectbox.h"

#include "def.h"


#define _VALMAX   0x8000
#define _VALHALF  0x4000


/* 通常 */

static int _blend_normal(uint16_t *ps,uint16_t *pd,int a)
{
	return 0;
}

/* 乗算 */

static int _blend_multiply(uint16_t *ps,uint16_t *pd,int a)
{
	ps[0] = pd[0] * ps[0] >> 15;
	ps[1] = pd[1] * ps[1] >> 15;
	ps[2] = pd[2] * ps[2] >> 15;

	return 0;
}

/* 加算 */

static int _blend_add(uint16_t *ps,uint16_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = ps[i] + pd[i];
		if(n > _VALMAX) n = _VALMAX;

		ps[i] = n;
	}

	return 0;
}

/* 減算 */

static int _blend_sub(uint16_t *ps,uint16_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = pd[i] - ps[i];
		if(n < 0) n = 0;

		ps[i] = n;
	}

	return 0;
}

/* スクリーン */

static int _blend_screen(uint16_t *ps,uint16_t *pd,int a)
{
	int i,ns,nd;

	for(i = 0; i < 3; i++)
	{
		ns = ps[i];
		nd = pd[i];
		ps[i] = ns + nd - (ns * nd >> 15);
	}
	
	return 0;
}

/* オーバーレイ */

static int _blend_overlay(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(dst < _VALHALF)
			src = src * dst >> 14;
		else
			src = _VALMAX - ((_VALMAX - dst) * (_VALMAX - src) >> 14);
		
		ps[i] = src;
	}
	
	return 0;
}

/* ハードライト */

static int _blend_hardlight(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(src < _VALHALF)
			src = src * dst >> 14;
		else
			src = _VALMAX - ((_VALMAX - dst) * (_VALMAX - src) >> 14);
		
		ps[i] = src;
	}
	
	return 0;
}

/* ソフトライト */

static int _blend_softlight(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src,dst,n;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		n = src * dst >> 15;
		
		ps[i] = n + (dst * (_VALMAX - ((_VALMAX - src) * (_VALMAX - dst) >> 15) - n) >> 15);
	}
	
	return 0;
}

/* 覆い焼き */

static int _blend_dodge(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];

		if(src != _VALMAX)
		{
			src = (pd[i] << 15) / (_VALMAX - src);
			if(src > _VALMAX) src = _VALMAX;
		}
		
		ps[i] = src;
	}
	
	return 0;
}

/* 焼き込み */

static int _blend_burn(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];

		if(src)
		{
			src = _VALMAX - ((_VALMAX - pd[i]) << 15) / src;
			if(src < 0) src = 0;
		}
		
		ps[i] = src;
	}
	
	return 0;
}

/* 焼き込みリニア */

static int _blend_linear_burn(uint16_t *ps,uint16_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = ps[i] + pd[i] - _VALMAX;
		if(n < 0) n = 0;
		
		ps[i] = n;
	}
	
	return 0;
}

/* ビビッドライト */

static int _blend_vivid_light(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src,dst,n;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(src < _VALHALF)
		{
			n = _VALMAX - (src << 1);

			if(dst <= n || src == 0)
				n = 0;
			else
				n = ((dst - n) << 15) / (src << 1);
		}
		else
		{
			n = (1<<16) - (src << 1);

			if(dst >= n || n == 0)
				n = _VALMAX;
			else
				n = (dst << 15) / n;
		}
		
		ps[i] = n;
	}
	
	return 0;
}

/* リニアライト */

static int _blend_linear_light(uint16_t *ps,uint16_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = (ps[i] << 1) + pd[i] - _VALMAX;

		if(n < 0) n = 0;
		else if(n > _VALMAX) n = _VALMAX;
		
		ps[i] = n;
	}
	
	return 0;
}

/* ピンライト */

static int _blend_pin_light(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(src > _VALHALF)
		{
			src = (src << 1) - _VALMAX;
			if(src < dst) src = dst;
		}
		else
		{
			src <<= 1;
			if(src > dst) src = dst;
		}
		
		ps[i] = src;
	}
	
	return 0;
}

/* 比較 (暗) */

static int _blend_darken(uint16_t *ps,uint16_t *pd,int a)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(pd[i] < ps[i])
			ps[i] = pd[i];
	}
	
	return 0;
}

/* 比較 (明) */

static int _blend_lighten(uint16_t *ps,uint16_t *pd,int a)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(pd[i] > ps[i])
			ps[i] = pd[i];
	}
	
	return 0;
}

/* 差の絶対値 */

static int _blend_diff(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		ps[i] = (dst > src)? dst - src: src - dst;
	}
	
	return 0;
}

/* 発光 (加算) */

static int _blend_lum_add(uint16_t *ps,uint16_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = pd[i] + (ps[i] * a >> 15);
		if(n > _VALMAX) n = _VALMAX;

		ps[i] = n;
	}
	
	return 1;
}

/* 発光 (覆い焼き) */

static int _blend_lum_dodge(uint16_t *ps,uint16_t *pd,int a)
{
	int i,src;

	for(i = 0; i < 3; i++)
	{
		src = ps[i] * a >> 15;

		if(src != _VALMAX)
		{
			src = (pd[i] << 15) / (_VALMAX - src);
			if(src > _VALMAX) src = _VALMAX;
		}

		ps[i] = src;
	}
	
	return 1;
}


//=======================


static int (*g_func_blend16[])(uint16_t *,uint16_t *,int) = {
	_blend_normal, _blend_multiply, _blend_add, _blend_sub, _blend_screen, _blend_overlay,
	_blend_hardlight, _blend_softlight, _blend_dodge, _blend_burn,
	_blend_linear_burn, _blend_vivid_light, _blend_linear_light, _blend_pin_light,
	_blend_darken, _blend_lighten, _blend_diff, _blend_lum_add, _blend_lum_dodge,
};

#define _GETVAL16(p)   ((*(p) << 8) | *(p + 1))
#define _COL8_TO_16(n) ((((n) << 15) + 127) / 255)


/* 8bit RGB から 16bit RGB 取得 */

static void _rgb8_to_rgb16(uint16_t *dst,uint32_t col)
{
	int i,c[3];

	c[0] = MLK_RGB_R(col);
	c[1] = MLK_RGB_G(col);
	c[2] = MLK_RGB_B(col);

	for(i = 0; i < 3; i++)
		dst[i] = ((c[i] << 15) + 127) / 255;
}


/** タイルごとにレイヤ合成 (RGBA-16bit) */

void blendtile_rgba16(mImageBuf2 *img,uint8_t *tilebuf,
	int x,int y,int mode,int opacity,uint32_t col,TexItem *tex)
{
	uint16_t **ppd,*pd,tmp[3];
	uint8_t *ps;
	mRect rc;
	int ix,iy,w,xtop,pitchs,a,xx,yy;
	int (*func)(uint16_t *,uint16_t *,int);

	//イメージ範囲内にクリッピング

	rc.x1 = x, rc.y1 = y;
	rc.x2 = x + 63, rc.y2 = y + 63;

	if(!mRectClipBox_d(&rc, 0, 0, img->width, img->height))
		return;

	//

	func = g_func_blend16[mode];

	ps = tilebuf + (rc.y1 - y) * 128 + (rc.x1 - x) * 2;
	ppd = (uint16_t **)img->ppbuf + rc.y1;
	w = rc.x2 - rc.x1 + 1;
	xtop = rc.x1 * 3;
	pitchs = 128 - w * 2;

	for(iy = rc.y2 - rc.y1 + 1, yy = rc.y1; iy; iy--, yy++, ppd++)
	{
		pd = *ppd + xtop;
		xx = rc.x1;
		
		for(ix = w; ix; ix--, xx++, pd += 3, ps += 2)
		{
			a = _GETVAL16(ps + 8192*3) * opacity >> 7;

			if(tex)
				a = a * TextureItem_getOpacity(tex, xx, yy) / 255;

			if(a)
			{
				tmp[0] = _GETVAL16(ps);
				tmp[1] = _GETVAL16(ps + 8192);
				tmp[2] = _GETVAL16(ps + 8192*2);

				if((func)(tmp, pd, a) || a == (1<<15))
				{
					pd[0] = tmp[0];
					pd[1] = tmp[1];
					pd[2] = tmp[2];
				}
				else
				{
					pd[0] = ((tmp[0] - pd[0]) * a >> 15) + pd[0];
					pd[1] = ((tmp[1] - pd[1]) * a >> 15) + pd[1];
					pd[2] = ((tmp[2] - pd[2]) * a >> 15) + pd[2];
				}
			}
		}

		ps += pitchs;
	}
}

/** タイルごとにレイヤ合成 (GRAY+A-16bit) */

void blendtile_graya16(mImageBuf2 *img,uint8_t *tilebuf,
	int x,int y,int mode,int opacity,uint32_t col,TexItem *tex)
{
	uint16_t **ppd,*pd,tmp[3];
	uint8_t *ps;
	mRect rc;
	int ix,iy,w,xtop,pitchs,a,xx,yy;
	int (*func)(uint16_t *,uint16_t *,int);

	//イメージ範囲内にクリッピング

	rc.x1 = x, rc.y1 = y;
	rc.x2 = x + 63, rc.y2 = y + 63;

	if(!mRectClipBox_d(&rc, 0, 0, img->width, img->height))
		return;

	//

	func = g_func_blend16[mode];

	ps = tilebuf + (rc.y1 - y) * 128 + (rc.x1 - x) * 2;
	ppd = (uint16_t **)img->ppbuf + rc.y1;
	w = rc.x2 - rc.x1 + 1;
	xtop = rc.x1 * 3;
	pitchs = 128 - w * 2;

	for(iy = rc.y2 - rc.y1 + 1, yy = rc.y1; iy; iy--, yy++, ppd++)
	{
		pd = *ppd + xtop;
		xx = rc.x1;
		
		for(ix = w; ix; ix--, xx++, pd += 3, ps += 2)
		{
			a = _GETVAL16(ps + 8192) * opacity >> 7;

			if(tex)
				a = a * TextureItem_getOpacity(tex, xx, yy) / 255;

			if(a)
			{
				tmp[0] = tmp[1] = tmp[2] = _GETVAL16(ps);

				if((func)(tmp, pd, a) || a == (1<<15))
				{
					pd[0] = tmp[0];
					pd[1] = tmp[1];
					pd[2] = tmp[2];
				}
				else
				{
					pd[0] = ((tmp[0] - pd[0]) * a >> 15) + pd[0];
					pd[1] = ((tmp[1] - pd[1]) * a >> 15) + pd[1];
					pd[2] = ((tmp[2] - pd[2]) * a >> 15) + pd[2];
				}
			}
		}

		ps += pitchs;
	}
}

/** タイルごとにレイヤ合成 (A-16bit) */

void blendtile_a16(mImageBuf2 *img,uint8_t *tilebuf,
	int x,int y,int mode,int opacity,uint32_t col,TexItem *tex)
{
	uint16_t **ppd,*pd,tmp[3],rgb[3];
	uint8_t *ps;
	mRect rc;
	int ix,iy,w,xtop,pitchs,a,xx,yy;
	int (*func)(uint16_t *,uint16_t *,int);

	//イメージ範囲内にクリッピング

	rc.x1 = x, rc.y1 = y;
	rc.x2 = x + 63, rc.y2 = y + 63;

	if(!mRectClipBox_d(&rc, 0, 0, img->width, img->height))
		return;

	//

	func = g_func_blend16[mode];

	_rgb8_to_rgb16(rgb, col);

	ps = tilebuf + (rc.y1 - y) * 128 + (rc.x1 - x) * 2;
	ppd = (uint16_t **)img->ppbuf + rc.y1;
	w = rc.x2 - rc.x1 + 1;
	xtop = rc.x1 * 3;
	pitchs = 128 - w * 2;

	for(iy = rc.y2 - rc.y1 + 1, yy = rc.y1; iy; iy--, yy++, ppd++)
	{
		pd = *ppd + xtop;
		xx = rc.x1;
		
		for(ix = w; ix; ix--, xx++, pd += 3, ps += 2)
		{
			a = _GETVAL16(ps) * opacity >> 7;

			if(tex)
				a = a * TextureItem_getOpacity(tex, xx, yy) / 255;

			if(a)
			{
				tmp[0] = rgb[0];
				tmp[1] = rgb[1];
				tmp[2] = rgb[2];

				if((func)(tmp, pd, a) || a == (1<<15))
				{
					pd[0] = tmp[0];
					pd[1] = tmp[1];
					pd[2] = tmp[2];
				}
				else
				{
					pd[0] = ((tmp[0] - pd[0]) * a >> 15) + pd[0];
					pd[1] = ((tmp[1] - pd[1]) * a >> 15) + pd[1];
					pd[2] = ((tmp[2] - pd[2]) * a >> 15) + pd[2];
				}
			}
		}

		ps += pitchs;
	}
}

/** タイルごとにレイヤ合成 (A-1bit) */

void blendtile_a1(mImageBuf2 *img,uint8_t *tilebuf,
	int x,int y,int mode,int opacity,uint32_t col,TexItem *tex)
{
	uint16_t **ppd,*pd,tmp[3],rgb[3];
	uint8_t *ps,*psY,f,fleft;
	mRect rc;
	int ix,iy,w,xtop,a,srca,xx,yy;
	int (*func)(uint16_t *,uint16_t *,int);

	//イメージ範囲内にクリッピング

	rc.x1 = x, rc.y1 = y;
	rc.x2 = x + 63, rc.y2 = y + 63;

	if(!mRectClipBox_d(&rc, 0, 0, img->width, img->height))
		return;

	//

	func = g_func_blend16[mode];

	_rgb8_to_rgb16(rgb, col);

	psY = tilebuf + (rc.y1 - y) * 8 + ((rc.x1 - x) >> 3);
	ppd = (uint16_t **)img->ppbuf + rc.y1;
	w = rc.x2 - rc.x1 + 1;
	xtop = rc.x1 * 3;
	fleft = 1 << (7 - ((rc.x1 - x) & 7));
	srca = 0x8000 * opacity >> 7;

	for(iy = rc.y2 - rc.y1 + 1, yy = rc.y1; iy; iy--, yy++, ppd++)
	{
		pd = *ppd + xtop;
		ps = psY;
		f = fleft;
		xx = rc.x1;
		
		for(ix = w; ix; ix--, xx++, pd += 3)
		{
			if(*ps & f)
			{
				a = srca;

				if(tex)
					a = a * TextureItem_getOpacity(tex, xx, yy) / 255;

				if(a)
				{
					tmp[0] = rgb[0];
					tmp[1] = rgb[1];
					tmp[2] = rgb[2];

					if((func)(tmp, pd, a) || a == (1<<15))
					{
						pd[0] = tmp[0];
						pd[1] = tmp[1];
						pd[2] = tmp[2];
					}
					else
					{
						pd[0] = ((tmp[0] - pd[0]) * a >> 15) + pd[0];
						pd[1] = ((tmp[1] - pd[1]) * a >> 15) + pd[1];
						pd[2] = ((tmp[2] - pd[2]) * a >> 15) + pd[2];
					}
				}
			}

			f >>= 1;
			if(!f)
			{
				ps++;
				f = 0x80;
			}
		}

		psY += 8;
	}
}

/*
//RGBA 8bit => RGB 16bit

void blendtile_rgba8(mImageBuf2 *img,uint8_t *tilebuf,
	int x,int y,int mode,int opacity,uint32_t col,TexItem *tex)
{
	uint16_t **ppd,*pd,tmp[3];
	uint8_t *ps;
	mRect rc;
	int ix,iy,w,xtop,pitchs,a,xx,yy;
	int (*func)(uint16_t *,uint16_t *,int);

	//イメージ範囲内にクリッピング

	rc.x1 = x, rc.y1 = y;
	rc.x2 = x + 63, rc.y2 = y + 63;

	if(!mRectClipBox_d(&rc, 0, 0, img->width, img->height))
		return;

	//

	func = g_func_blend16[mode];

	ps = tilebuf + (rc.y1 - y) * 256 + (rc.x1 - x) * 4;
	ppd = (uint16_t **)img->ppbuf + rc.y1;
	w = rc.x2 - rc.x1 + 1;
	xtop = rc.x1 * 3;
	pitchs = 256 - w * 4;

	for(iy = rc.y2 - rc.y1 + 1, yy = rc.y1; iy; iy--, yy++, ppd++)
	{
		pd = *ppd + xtop;
		xx = rc.x1;
		
		for(ix = w; ix; ix--, xx++, pd += 3, ps += 4)
		{
			a = _COL8_TO_16(ps[3]);
			a = a * opacity >> 7;

			if(tex)
				a = a * TextureItem_getOpacity(tex, xx, yy) / 255;

			if(a)
			{
				tmp[0] = _COL8_TO_16(ps[0]);
				tmp[1] = _COL8_TO_16(ps[1]);
				tmp[2] = _COL8_TO_16(ps[2]);

				if((func)(tmp, pd, a) || a == (1<<15))
				{
					pd[0] = tmp[0];
					pd[1] = tmp[1];
					pd[2] = tmp[2];
				}
				else
				{
					pd[0] = ((tmp[0] - pd[0]) * a >> 15) + pd[0];
					pd[1] = ((tmp[1] - pd[1]) * a >> 15) + pd[1];
					pd[2] = ((tmp[2] - pd[2]) * a >> 15) + pd[2];
				}
			}
		}

		ps += pitchs;
	}
}
*/
