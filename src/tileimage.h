/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * TileImage
 ************************************/

typedef struct _TileImage TileImage;
typedef struct _TexItem TexItem;
typedef struct _mImageBuf2 mImageBuf2;

typedef struct
{
	struct{ uint8_t r,g,b; }c8;
	struct{ uint16_t r,g,b; }c16;
}RGBcombo;

struct _TileImage
{
	uint8_t **ppbuf;	//タイル配列
	int type,			//カラータイプ
		tilesize,		//1つのタイルのバイト数
		tilew,tileh,	//タイル配列の幅と高さ
		offx,offy;		//オフセット位置
	RGBcombo col;
};

/* 合成用情報 */

typedef struct _TileImageBlendSrcInfo
{
	int opacity,
		tone_lines,	//0 でトーン化なし。1=0.1
		tone_angle,
		tone_density; //0 で濃度固定なし
	uint8_t blendmode,
		ftone_white;	//トーン背景を白に
	TexItem *texitem;
}TileImageBlendSrcInfo;

/* 処理範囲情報 */

typedef struct
{
	mRect rctile,	//タイル範囲
		rcclip;		//pxクリッピング範囲: x2,y2 は+1
	mPoint pxtop;	//タイル先頭の px 位置
	int tilew,tileh,//タイル範囲の幅
		pitch_tile;	//タイルのY移動幅
}TileImageTileRectInfo;

/* カラータイプ */
enum
{
	TILEIMAGE_COLTYPE_RGBA,
	TILEIMAGE_COLTYPE_GRAY,
	TILEIMAGE_COLTYPE_ALPHA,
	TILEIMAGE_COLTYPE_ALPHA1BIT
};

#define TILEIMAGE_GETTILE_BUFPT(p,tx,ty)  ((p)->ppbuf + (ty) * (p)->tilew + (tx))
#define TILEIMAGE_GETTILE_PT(p,tx,ty)     *((p)->ppbuf + (ty) * (p)->tilew + (tx))


/*----------*/

void TileImage_init(void);
void TileImage_finish(void);

void TileImage_global_setBits(int bits);
void TileImage_global_setDPI(int dpi);
void TileImage_global_setToneToGray(int enable);
int TileImage_global_getTileSize(int type,int bits);

mlkbool TileImage_tile_isTransparent_forA(uint8_t *tile,int size);

void TileImage_free(TileImage *p);
TileImage *TileImage_new(int type,int w,int h);
TileImage *TileImage_newFromRect_forFile(int type,const mRect *rc);

void TileImage_setColor(TileImage *p,uint32_t col);

uint8_t *TileImage_allocTile(TileImage *p);
mlkbool TileImage_allocTile_atptr(TileImage *p,uint8_t **ppbuf);
uint8_t *TileImage_getTileAlloc_atpos(TileImage *p,int tx,int ty);

void TileImage_tile_to_pixel(TileImage *p,int tx,int ty,int *px,int *py);
void TileImage_pixel_to_tile_nojudge(TileImage *p,int x,int y,int *tilex,int *tiley);
mlkbool TileImage_getHaveImageRect_pixel(TileImage *p,mRect *rcdst,uint32_t *tilenum);

mlkerr TileImage_allocTile_fromSave(TileImage *p,int tx,int ty,uint8_t *srcbuf,int to8bit);
mlkerr TileImage_allocTile_fromSave_APDv3(TileImage *p,int tx,int ty,uint8_t *srcbuf,int to8bit);

mlkerr TileImage_saveTiles_apd4(TileImage *p,mRect *rc,uint8_t *buf,
	mlkerr (*func)(TileImage *p,void *param),void *param);

void TileImage_blend(TileImage *p,mImageBuf2 *dst,const mBox *boxdst,const TileImageBlendSrcInfo *sinfo);
void TileImage_to_RGBA8bit(TileImage *p,mImageBuf2 *dst,const mBox *boxdst);

mlkbool TileImage_convertFromImage_RGBA8(TileImage *p,uint8_t **ppsrc,int srcw,int srch);
mlkbool TileImage_convertFromImage_alpha8(TileImage *p,uint8_t **ppsrc,int srcw,int srch);

