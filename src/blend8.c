/*$
apdtool
Copyright (c) 2020 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * レイヤ合成 (8bit)
 *****************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_imagebuf.h"
#include "mlk_rectbox.h"

#include "def.h"


/* 通常 */

static int _blend_normal(uint8_t *ps,uint8_t *pd,int a)
{
	return 0;
}

/* 乗算 */

static int _blend_multiply(uint8_t *ps,uint8_t *pd,int a)
{
	ps[0] = pd[0] * ps[0] / 255;
	ps[1] = pd[1] * ps[1] / 255;
	ps[2] = pd[2] * ps[2] / 255;

	return 0;
}

/* 加算 (AzPainterB ではこちら) */

static int _blend_add(uint8_t *ps,uint8_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = ps[i] + pd[i];
		if(n > 255) n = 255;

		ps[i] = n;
	}

	return 0;
}

/* 減算 */

static int _blend_sub(uint8_t *ps,uint8_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = pd[i] - ps[i];
		if(n < 0) n = 0;

		ps[i] = n;
	}

	return 0;
}

/* スクリーン */

static int _blend_screen(uint8_t *ps,uint8_t *pd,int a)
{
	int i,ns,nd;

	for(i = 0; i < 3; i++)
	{
		ns = ps[i];
		nd = pd[i];
		ps[i] = ns + nd - ns * nd / 255;
	}
	
	return 0;
}

/* オーバーレイ */

static int _blend_overlay(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(dst < 128)
			src = src * dst >> 7;
		else
			src = 255 - ((255 - dst) * (255 - src) >> 7);
		
		ps[i] = src;
	}
	
	return 0;
}

/* ハードライト */

static int _blend_hardlight(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(src < 128)
			src = src * dst >> 7;
		else
			src = 255 - ((255 - dst) * (255 - src) >> 7);
		
		ps[i] = src;
	}
	
	return 0;
}

/* ソフトライト */

static int _blend_softlight(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src,dst,n;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		n = src * dst / 255;
		
		ps[i] = n + (dst * (255 - ((255 - src) * (255 - dst) / 255) - n) / 255);
	}
	
	return 0;
}

/* 覆い焼き */

static int _blend_dodge(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];

		if(src != 255)
		{
			src = pd[i] * 255 / (255 - src);
			if(src > 255) src = 255;
		}
		
		ps[i] = src;
	}
	
	return 0;
}

/* 焼き込み */

static int _blend_burn(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];

		if(src)
		{
			src = 255 - (255 - pd[i]) * 255 / src;
			if(src < 0) src = 0;
		}
		
		ps[i] = src;
	}
	
	return 0;
}

/* 焼き込みリニア */

static int _blend_linear_burn(uint8_t *ps,uint8_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = ps[i] + pd[i] - 255;
		if(n < 0) n = 0;
		
		ps[i] = n;
	}
	
	return 0;
}

/* ビビッドライト */

static int _blend_vivid_light(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src,dst,n;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(src < 128)
		{
			n = 255 - (src << 1);

			if(dst <= n || src == 0)
				n = 0;
			else
				n = (dst - n) * 255 / (src << 1);
		}
		else
		{
			n = 255 * 2 - (src << 1);

			if(dst >= n || n == 0)
				n = 255;
			else
				n = dst * 255 / n;
		}
		
		ps[i] = n;
	}
	
	return 0;
}

/* リニアライト */

static int _blend_linear_light(uint8_t *ps,uint8_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = (ps[i] << 1) + pd[i] - 255;

		if(n < 0) n = 0;
		else if(n > 255) n = 255;
		
		ps[i] = n;
	}
	
	return 0;
}

/* ピンライト */

static int _blend_pin_light(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		if(src >= 128)
		{
			src = (src << 1) - 255;
			if(src < dst) src = dst;
		}
		else
		{
			src <<= 1;
			if(src > dst) src = dst;
		}
		
		ps[i] = src;
	}
	
	return 0;
}

/* 比較 (暗) */

static int _blend_darken(uint8_t *ps,uint8_t *pd,int a)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(pd[i] < ps[i])
			ps[i] = pd[i];
	}
	
	return 0;
}

/* 比較 (明) */

static int _blend_lighten(uint8_t *ps,uint8_t *pd,int a)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(pd[i] > ps[i])
			ps[i] = pd[i];
	}
	
	return 0;
}

/* 差の絶対値 */

static int _blend_diff(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src,dst;

	for(i = 0; i < 3; i++)
	{
		src = ps[i];
		dst = pd[i];

		ps[i] = (dst > src)? dst - src: src - dst;
	}
	
	return 0;
}

/* 発光 (加算) */

static int _blend_lum_add(uint8_t *ps,uint8_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = pd[i] + ps[i] * a / 255;
		if(n > 255) n = 255;

		ps[i] = n;
	}
	
	return 1;
}

/* 発光 (覆い焼き) */

static int _blend_lum_dodge(uint8_t *ps,uint8_t *pd,int a)
{
	int i,src;

	for(i = 0; i < 3; i++)
	{
		src = ps[i] * a / 255;

		if(src != 255)
		{
			src = pd[i] * 255 / (255 - src);
			if(src > 255) src = 255;
		}

		ps[i] = src;
	}
	
	return 1;
}

/* 減算 (A)
 *
 * AzPainter(Windows) の減算はこちら */

static int _blend_lum_sub(uint8_t *ps,uint8_t *pd,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = pd[i] - ps[i] * a / 255;
		if(n < 0) n = 0;
	
		ps[i] = n;
	}
	
	return 1;
}


//=======================


/* RGB -> HSL */

static void _rgb_to_hsl(int r,int g,int b,float *dst)
{
	float h,s,fr,fg,fb,min,max;

	fr = r / 255.0f;
	fg = g / 255.0f;
	fb = b / 255.0f;

	dst[2] = fr * 0.3f + fg * 0.59f + fb * 0.11f;

	max = (r >= g)? fr: fg; if(fb > max) max = fb;
	min = (r <= g)? fr: fg; if(fb < min) min = fb;

	s = max - min;
	dst[1] = s;

	if(s == 0)
	{
		dst[0] = 0;
		return;
	}

	if(fr == max)
		h = (fg - fb) / s;
	else if(fg == max)
		h = (fb - fr) / s + 2.0f;
	else
		h = (fr - fg) / s + 4.0f;

	if(h < 0) h += 6.0f;
	
	dst[0] = h;
}

/* HSL -> RGB */

void _hsl_to_rgb(float *hsl,uint8_t *dst)
{
    float r,g,b,h,s,l,max,min,x,t;
    int n;

	h = hsl[0];
	s = hsl[1];
	l = hsl[2];

	if(h < 0 || s == 0)
	{
		dst[0] = dst[1] = dst[2] = (int)(l * 255 + 0.5f);
		return;
	}

	//

	if(h <= 1.0f)
	{
		b = -0.3f - h * 0.59f;
		r = 1.0f + b;
		g = b + h;
	}
	else if(h <= 2.0f)
	{
		h -= 2.0f;

		b = -0.59f + h * 0.3f;
		r = b - h;
		g = 1.0f + b;
	}
	else if(h <= 3.0f)
	{
		h -= 2.0f;
		
		r = -0.59f - h * 0.11f;
		g = 1.0f + r;
		b = r + h;
	}
	else if(h <= 4.0f)
	{
		h -= 4.0f;
		
		r = -0.11f + h * 0.59f;
		g = r - h;
		b = 1.0f + r;
	}
	else if(h <= 5.0f)
	{
		h -= 4.0f;
		
		g = -0.11f - h * 0.3f;
		r = g + h;
		b = 1.0f + g;
	}
	else
	{
		h -= 6.0f;
		
		g = -0.3f + h * 0.11f;
		r = 1.0f + g;
		b = g - h;
	}

	//

	r *= s;
	g *= s;
	b *= s;

	max = (r >= g)? r: g; if(b > max) max = b;
	min = (r <= g)? r: g; if(b < min) min = b;

	if(max + l > 1.0f)
		x = (1.0f - l) / max;
	else
		x = 1.0f;

	if(min + l < 0)
	{
		t = l / (-min);
		if(t < x) x = t;
	}

	if(x != 1.0f)
	{
		r *= x;
		g *= x;
		b *= x;
	}

	r += l;
	g += l;
	b += l;

	//

	n = (int)(r * 255 + 0.5f);
	if(n < 0) n = 0; else if(n > 255) n = 255;
	dst[0] = n;

	n = (int)(g * 255 + 0.5f);
	if(n < 0) n = 0; else if(n > 255) n = 255;
	dst[1] = n;

	n = (int)(b * 255 + 0.5f);
	if(n < 0) n = 0; else if(n > 255) n = 255;
	dst[2] = n;
}

/* 色相 */

static int _blend_hue(uint8_t *ps,uint8_t *pd,int a)
{
	float src[3],dst[3];

	_rgb_to_hsl(ps[0], ps[1], ps[2], src);
	_rgb_to_hsl(pd[0], pd[1], pd[2], dst);

	if(src[1] == 0)
		//彩度が0の場合
		dst[1] = 0;
	else
		dst[0] = src[0];

	_hsl_to_rgb(dst, ps);
	
	return 0;
}

/* 彩度 */

static int _blend_saturation(uint8_t *ps,uint8_t *pd,int a)
{
	float src[3],dst[3];

	_rgb_to_hsl(ps[0], ps[1], ps[2], src);
	_rgb_to_hsl(pd[0], pd[1], pd[2], dst);

	dst[1] = src[1];

	_hsl_to_rgb(dst, ps);
	
	return 0;
}

/* カラー */

static int _blend_color(uint8_t *ps,uint8_t *pd,int a)
{
	float src[3];

	_rgb_to_hsl(ps[0], ps[1], ps[2], src);

	src[2] = (pd[0] * 0.3f + pd[1] * 0.59f + pd[2] * 0.11f) / 255.0f;

	_hsl_to_rgb(src, ps);
	
	return 0;
}

/* 輝度 */

static int _blend_lum(uint8_t *ps,uint8_t *pd,int a)
{
	float dst[3];

	_rgb_to_hsl(pd[0], pd[1], pd[2], dst);

	dst[2] = (ps[0] * 0.3f + ps[1] * 0.59f + ps[2] * 0.11f) / 255.0f;

	_hsl_to_rgb(dst, ps);
	
	return 0;
}


//=======================


static int (*g_func_blend[])(uint8_t *,uint8_t *,int) = {
	_blend_normal, _blend_multiply, _blend_add, _blend_sub, _blend_screen, _blend_overlay,
	_blend_hardlight, _blend_softlight, _blend_dodge, _blend_burn,
	_blend_linear_burn, _blend_vivid_light, _blend_linear_light, _blend_pin_light,
	_blend_darken, _blend_lighten, _blend_diff, _blend_lum_add, _blend_lum_dodge,
	_blend_lum_sub, _blend_hue, _blend_saturation, _blend_color, _blend_lum
};

/** レイヤ合成 (8bit) */

void blendimage_8bit(mImageBuf2 *imgdst,mImageBuf2 *imgsrc,int mode,int alpha)
{
	uint8_t **pps,**ppd,*ps,*pd,tmp[3];
	int ix,iy,a;
	int (*func)(uint8_t *,uint8_t *,int);

	func = g_func_blend[mode];

	pps = imgsrc->ppbuf;
	ppd = imgdst->ppbuf;

	for(iy = imgdst->height; iy; iy--)
	{
		ps = *(pps++);
		pd = *(ppd++);
	
		for(ix = imgdst->width; ix; ix--)
		{
			a = ps[3] * alpha >> 7;

			if(a)
			{
				tmp[0] = ps[0];
				tmp[1] = ps[1];
				tmp[2] = ps[2];

				if((func)(tmp, pd, a) || a == 255)
				{
					pd[0] = tmp[0];
					pd[1] = tmp[1];
					pd[2] = tmp[2];
				}
				else
				{
					pd[0] = (tmp[0] - pd[0]) * a / 255 + pd[0];
					pd[1] = (tmp[1] - pd[1]) * a / 255 + pd[1];
					pd[2] = (tmp[2] - pd[2]) * a / 255 + pd[2];
				}
			}
		
			ps += 4;
			pd += 3;
		}
	}
}

/** タイルごとにレイヤ合成 (RGBA 8bit => RGB 8bit) */

void blendtile_rgba8(mImageBuf2 *img,uint8_t *tilebuf,
	int x,int y,int mode,int opacity,uint32_t col,TexItem *tex)
{
	uint8_t *ps,**ppd,*pd,tmp[3];;
	mRect rc;
	int ix,iy,w,xtop,pitchs,a,xx,yy;
	int (*func)(uint8_t *,uint8_t *,int);

	//イメージ範囲内にクリッピング

	rc.x1 = x, rc.y1 = y;
	rc.x2 = x + 63, rc.y2 = y + 63;

	if(!mRectClipBox_d(&rc, 0, 0, img->width, img->height))
		return;

	//

	func = g_func_blend[mode];

	ps = tilebuf + (rc.y1 - y) * 256 + (rc.x1 - x) * 4;
	ppd = img->ppbuf + rc.y1;
	w = rc.x2 - rc.x1 + 1;
	xtop = rc.x1 * 3;
	pitchs = 256 - w * 4;

	for(iy = rc.y2 - rc.y1 + 1, yy = rc.y1; iy; iy--, yy++, ppd++)
	{
		pd = *ppd + xtop;
		xx = rc.x1;
		
		for(ix = w; ix; ix--, xx++, pd += 3, ps += 4)
		{
			a = ps[3] * opacity >> 7;

			if(tex)
				a = a * TextureItem_getOpacity(tex, xx, yy) / 255;

			if(a)
			{
				tmp[0] = ps[0];
				tmp[1] = ps[1];
				tmp[2] = ps[2];

				if((func)(tmp, pd, a) || a == 255)
				{
					pd[0] = tmp[0];
					pd[1] = tmp[1];
					pd[2] = tmp[2];
				}
				else
				{
					pd[0] = (tmp[0] - pd[0]) * a / 255 + pd[0];
					pd[1] = (tmp[1] - pd[1]) * a / 255 + pd[1];
					pd[2] = (tmp[2] - pd[2]) * a / 255 + pd[2];
				}
			}
		}

		ps += pitchs;
	}
}
