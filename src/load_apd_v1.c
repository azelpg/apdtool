/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * APD ver1 読み込み (LE)
 *****************************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_stdio.h"
#include "mlk_tree.h"
#include "mlk_zlib.h"
#include "mlk_imagebuf.h"
#include "mlk_imageconv.h"

#include "def.h"
#include "tileimage.h"
#include "blendmode.h"


//------------------

typedef struct
{
	FILE *fp;
	mZlib *zlib;
	mImageBuf2 *img; //全体イメージの読み込み用
	int width,
		height;
}_data;

/* 合成モード変換テーブル */

static const uint8_t g_v1_blendmode[] = {
	BLENDMODE_NORMAL, BLENDMODE_MUL, BLENDMODE_LUM_ADD, BLENDMODE_LUM_SUB,
	BLENDMODE_SCREEN, BLENDMODE_OVERLAY, BLENDMODE_SOFT_LIGHT, BLENDMODE_HARD_LIGHT,
	BLENDMODE_DODGE, BLENDMODE_BURN, BLENDMODE_LINEAR_BURN, BLENDMODE_VIVID_LIGHT,
	BLENDMODE_LINEAR_LIGHT, BLENDMODE_PIN_LIGHT, BLENDMODE_DIFF,
	BLENDMODE_LIGHTEN, BLENDMODE_DARKER,
	BLENDMODE_HUE, BLENDMODE_SATURATION, BLENDMODE_COLOR, BLENDMODE_LUM
};

//------------------


/* レイヤ画像読み込み */

static mlkerr _read_layer_image(_data *p,LayerItem *pl,uint32_t cmpsize)
{
	uint8_t **ppbuf;
	int i,pitch;
	mlkerr ret;

	//全体を読み込み (LE, ボトムアップ)

	mZlibDecReset(p->zlib);
	mZlibDecSetSize(p->zlib, cmpsize);

	ppbuf = p->img->ppbuf + p->height - 1;
	pitch = p->width * 4;

	for(i = p->height; i; i--, ppbuf--)
	{
		ret = mZlibDecRead(p->zlib, *ppbuf, pitch);
		if(ret) return ret;

		mImageConv_swap_rb_8(*ppbuf, p->width, 4);
	}

	ret = mZlibDecFinish(p->zlib);
	if(ret) return ret;

	//イメージ作成

	pl->img = TileImage_new(TILEIMAGE_COLTYPE_RGBA, p->width, p->height);
	if(!pl->img) return MLKERR_ALLOC;

	//変換

	if(!TileImage_convertFromImage_RGBA8(pl->img, p->img->ppbuf, p->width, p->height))
		return MLKERR_ALLOC;

	return MLKERR_OK;
}

/* レイヤ読み込み */

static mlkerr _read_layer(FILE *fp,_data *p,int layerinfo_seek)
{
	LayerItem *pl;
	char name[32];
	int i,n;
	uint32_t size;
	uint8_t blendmode,opacity,flags;
	mlkerr ret;

	for(i = g_work.img.layernum; i; i--)
	{
		//レイヤ情報

		if(mFILEreadOK(fp, name, 32)
			|| mFILEreadFormatLE(fp, "bbb", &blendmode, &opacity, &flags)
			|| fseek(fp, layerinfo_seek, SEEK_CUR)
			|| mFILEreadLE32(fp, &size))	//イメージ圧縮サイズ
			return MLKERR_DAMAGED;

		//レイヤ追加
		// :最下層から順のため、上に追加していく。

		pl = layer_add_top();
		if(!pl) return MLKERR_ALLOC;

		//レイヤ名

		name[31] = 0;
		pl->name = dupname_shiftjis(name, -1);

		//合成モード

		if(blendmode >= 21)
			blendmode = BLENDMODE_NORMAL;
		else
			blendmode = g_v1_blendmode[blendmode];

		pl->blendmode = blendmode;

		//ほか

		pl->opacity = opacity;

		if(flags & 1) pl->flags |= LAYERITEM_F_VISIBLE;

		if(flags & 8)
			pl->alphamask = 1;
		else
		{
			n = (flags >> 1) & 3;

			pl->alphamask = (n == 0)? 0: n + 1;
		}

		if(flags & 16)
			pl->flags |= LAYERITEM_F_FILLREF;

		//イメージ

		if(PROC_IS_SKIPIMAGE)
		{
			if(fseek(fp, size, SEEK_CUR))
				return MLKERR_DAMAGED;
		}
		else
		{
			ret = _read_layer_image(p, pl, size);
			if(ret) return ret;
		}
	}

	return MLKERR_OK;
}

/* 読み込みメイン */

static mlkerr _main_proc(FILE *fp,_data *p)
{
	uint32_t size,layerinfosize;
	uint16_t imgw,imgh,layernum,layercnt,layersel;

	//メイン情報

	if(mFILEreadFormatLE(fp, "ihhhhh",
		&size, &imgw, &imgh, &layernum, &layercnt, &layersel)
		|| fseek(fp, size - 10, SEEK_CUR))
		return MLKERR_DAMAGED;

	p->width = imgw;
	p->height = imgh;

	set_image_info(imgw, imgh, 8, 0, layernum);

	//プレビューイメージをスキップ, レイヤ情報サイズ

	if(fseek(fp, 4, SEEK_CUR)	//w,h
		|| mFILEreadLE32(fp, &size)
		|| fseek(fp, size, SEEK_CUR)
		|| mFILEreadLE32(fp, &layerinfosize))
		return MLKERR_DAMAGED;

	//読み込み用イメージ作成

	if(PROC_IS_NEEDIMAGE)
	{
		p->img = mImageBuf2_new(imgw, imgh, 32, 0);
		if(!p->img) return MLKERR_ALLOC;
	}

	//レイヤ読み込み

	return _read_layer(fp, p, layerinfosize - 35);
}

/** APD v1 読み込み */

mlkerr load_apd_v1(FILE *fp)
{
	_data dat;
	mlkerr ret;

	mMemset0(&dat, sizeof(_data));

	dat.fp = fp;

	ret = init_zlib(&dat.zlib, fp, FALSE);
	if(ret) return ret;

	//

	ret = _main_proc(fp, &dat);

	mImageBuf2_free(dat.img);
	mZlibFree(dat.zlib);

	return ret;
}

