/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/****************************
 * トーンテーブル
 ****************************/

#include <stdlib.h>
#include <math.h>

#include "mlk.h"

#include "tonetable.h"


//------------------

uint8_t *g_tone_table = NULL;
static int g_tone_bits = 0;

typedef struct
{
	int index;
	double val;
}_thval;

//------------------



/* _thval ソート関数 */

static int _tone_sort(const void *va,const void *vb)
{
	const _thval *a = (_thval *)va;
	const _thval *b = (_thval *)vb;

	if(a->val < b->val)
		return -1;
	else if(a->val > b->val)
		return 1;
	else
		return 0;
}

/* トーンテーブルのデータセット */

static void _set_data(uint8_t *tblbuf,int width,int bits)
{
	_thval *thbuf,*pth;
	uint8_t *pd8;
	uint16_t *pd16;
	int ww,ix,iy,index;
	double dx,dy,div,val;

	ww = width * width;

	//作業用バッファ

	thbuf = (_thval *)mMalloc(sizeof(_thval) * ww);
	if(!thbuf) return;

	//

	pth = thbuf;
	index = 0;
	div = 1.0 / (width - 1);

	for(iy = 0; iy < width; iy++)
	{
		//中心からの距離 (-1〜1) の絶対値
		dy = fabs((iy * div - 0.5) * 2);
	
		for(ix = 0; ix < width; ix++)
		{
			dx = fabs((ix * div - 0.5) * 2);

			//val = 中心が 1.0、縁が 0、外側が負

			if(dx + dy > 1.0)
				//外側はひし形
				val = ((dy - 1) * (dy - 1) + (dx - 1) * (dx - 1)) - 1;
			else
				//内側は円
				val = 1.0 - (dx * dx + dy * dy);
			
			if(val > 1)
				val = 1;
			else if(val < -1)
				val = -1;

			//

			pth->index = index++;
			pth->val = val;
			pth++;
		}
	}

	//val の小さい順にソート

	qsort(thbuf, ww, sizeof(_thval), _tone_sort);

	//しきい値バッファをセット

	if(bits == 8)
	{
		pd8 = tblbuf;
		
		for(ix = 0; ix < ww; ix++)
			pd8[thbuf[ix].index] = ix * 255 / ww;
	}
	else
	{
		pd16 = (uint16_t *)tblbuf;
		
		for(ix = 0; ix < ww; ix++)
			pd16[thbuf[ix].index] = ((int64_t)ix << 15) / ww;
	}

	//

	mFree(thbuf);
}


/** 解放 */

void ToneTable_free(void)
{
	mFree(g_tone_table);
}

/** 初期化 */

void ToneTable_init(void)
{
	g_tone_table = (uint8_t *)mMalloc(TABLEDATA_TONE_WIDTH * TABLEDATA_TONE_WIDTH * 2);
}

/** レイヤトーン化用テーブル作成 */

void ToneTable_setData(int bits)
{
	if(g_tone_bits == bits) return;

	_set_data(g_tone_table, TABLEDATA_TONE_WIDTH, bits);

	g_tone_bits = bits;
}

