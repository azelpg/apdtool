/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * テクスチャイメージリスト
 *****************************/

#include <string.h>

#include "mlk.h"
#include "mlk_list.h"
#include "mlk_loadimage.h"
#include "mlk_imagebuf.h"
#include "mlk_charset.h"
#include "mlk_util.h"


//----------------

typedef struct _TexItem
{
	mListItem i;

	uint8_t *buf;	//イメージ (8bit 濃度)
	int w,h;
	uint32_t hash;	//パス名ハッシュ
	char path[1];	//パス名
}TexItem;

static mFuncLoadImageCheck g_load_checks[] = {
	mLoadImage_checkPNG, mLoadImage_checkBMP, mLoadImage_checkGIF, 0
};

//----------------


/* 画像読み込み */

static mlkbool _load_image(TexItem *pi)
{
	mImageBuf *img;
	uint8_t *ps,*pd;
	mLoadImageOpen open;
	mLoadImageType type;
	int w,h,ix,iy,size,r,g,b,a;
	
	//フォーマット判定

	open.type = MLOADIMAGE_OPEN_FILENAME;
	open.filename = pi->path;

	if(mLoadImage_checkFormat(&type, &open, g_load_checks, 8))
		return FALSE;

	//読み込み (32bit)

	img = mImageBuf_loadImage(&open, &type, 32, 0);
	if(!img) return FALSE;

	w = img->width;
	h = img->height;

	pi->w = w;
	pi->h = h;

	//8bit バッファ確保 (全体は 4byte 単位)

	size = (w * h + 3) & (~3);

	pi->buf = (uint8_t *)mMalloc(size);
	if(!pi->buf)
	{
		mImageBuf_free(img);
		return FALSE;
	}

	//32bit -> 8bit 濃度変換

	ps = img->buf;
	pd = pi->buf;

	for(iy = h; iy; iy--)
	{
		for(ix = w; ix; ix--, ps += 4)
		{
			//背景白とアルファ合成
			
			a = ps[3];

			if(a == 0)
				r = g = b = 255;
			else if(a == 255)
			{
				r = ps[0];
				g = ps[1];
				b = ps[2];
			}
			else
			{
				r = (ps[0] - 255) * a / 255 + 255;
				g = (ps[1] - 255) * a / 255 + 255;
				b = (ps[2] - 255) * a / 255 + 255;
			}
			
			*(pd++) = 255 - ((r * 77 + g * 150 + b * 29) >> 8);
		}
	}

	mImageBuf_free(img);

	return TRUE;
}

/* パスから検索 */

static TexItem *_search_path(mList *list,const char *path,uint32_t hash)
{
	TexItem *pi;

	MLK_LIST_FOR(*list, pi, TexItem)
	{
		if(hash == pi->hash
			&& strcmp(path, pi->path) == 0)
			return pi;
	}

	return NULL;
}

/* アイテム削除ハンドラ */

static void _item_destroy(mList *list,mListItem *item)
{
	mFree(((TexItem *)item)->buf);
}


//====================


/** リスト初期化 */

void TextureList_init(mList *list)
{
	list->item_destroy = _item_destroy;
}

/** テクスチャイメージ読み込み */

TexItem *TextureList_load(mList *list,const char *path)
{
	TexItem *pi;
	uint32_t hash;
	int len;

	hash = mCalcStringHash(path);

	//同じパスが存在するか

	pi = _search_path(list, path, hash);
	if(pi) return pi;

	//追加

	len = strlen(path);

	pi = (TexItem *)mListAppendNew(list, sizeof(TexItem) + len);
	if(!pi) return NULL;

	pi->hash = hash;

	memcpy(pi->path, path, len);

	//画像読み込み

	if(!_load_image(pi))
	{
		mPutUTF8_format_stdout("  <!> texture load error: %s\n", path);
		mListDelete(list, MLISTITEM(pi));
		return NULL;
	}

	return pi;
}

/** 濃度を取得 */

int TextureItem_getOpacity(TexItem *pi,int x,int y)
{
	int w,h;

	w = pi->w;
	h = pi->h;

/*
	if(x < 0)
		x += (-x / w + 1) * w;

	if(y < 0)
		y += (-y / h + 1) * h;
*/

	return *(pi->buf + (y % h) * w + (x % w));
}

