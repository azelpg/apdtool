/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * サブ関数
 *****************************/

#include <stdio.h>
#include <stdarg.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_tree.h"
#include "mlk_zlib.h"
#include "mlk_charset.h"
#include "mlk_imagebuf.h"
#include "mlk_string.h"

#include "def.h"
#include "blendmode.h"
#include "tileimage.h"


//------------------

//合成モード名
static const char *g_blendmode_name[] = {
	"normal", "multiply", "add", "sub", "screen", "overlay",
	"hard light", "soft light", "dodge", "burn",
	"linear burn", "vivid light", "linear light", "pin light", "darker", "lighten",
	"diff", "lum add", "lum dodge", "lum sub",
	"hue", "saturation", "color", "lum"
};

//画像の拡張子
static const char *g_outformat_ext[] = {".png", ".bmp", ".psd"};

//------------------

mlkerr save_apd_v4(const char *filename,mImageBuf2 *img);

//------------------


/** エラーを表示して、エラー終了させる (通常)
 *
 * 終端には改行が出力される。 */

void puterr_exit(const char *fmt,...)
{
	va_list ap;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	putchar('\n');
	va_end(ap);

	exit_app(1);
}


//==============================
// 各処理
//==============================


/* 出力ファイル名を取得
 * (--output で、ファイル名指定可能な場合)
 *
 * ext: '.' を含む拡張子文字列 */

static void _get_output_path(mStr *dst,const char *ext)
{
	mStr str = MSTR_INIT;

	if(!(g_work.flags & FLAGS_OUTPUT_DIR)
		&& mStrIsnotEmpty(&g_work.str_output))
	{
		//出力ファイル名の指定

		mStrCopy(dst, &g_work.str_output);
	}
	else
	{
		//出力ディレクトリ + 元ファイル名 + 拡張子

		if(g_work.flags & FLAGS_OUTPUT_DIR)
			mStrCopy(dst, &g_work.str_output);

		mStrPathGetBasename_noext(&str, g_work.str_inputfile.buf);
		mStrAppendText(&str, ext);

		mStrPathJoin(dst, str.buf);
		mStrFree(&str);
	}
}

/* レイヤ出力のファイル名を取得 */

static void _get_output_path_layer(mStr *dst,int layerno,int dig)
{
	mStr str = MSTR_INIT;
	char m[8];

	mStrEmpty(dst);

	//ディレクトリ
	// :str_output が既存ディレクトリなら、指定ディレクトリ。
	// :それ以外はカレントディレクトリ

	if(g_work.flags & FLAGS_OUTPUT_DIR)
		mStrCopy(dst, &g_work.str_output);

	//先頭ファイル名

	if(g_work.flags & FLAGS_HAVE_LAYER_PREFIX)
	{
		//接頭語指定あり
		
		if(mStrIsEmpty(&g_work.str_layer_prefix))
			//空文字列の場合は、パス区切り追加
			mStrPathAppendDirSep(dst);
		else
			mStrPathJoin(dst, g_work.str_layer_prefix.buf);
	}
	else
	{
		//指定なし = "[入力ファイル名]_"

		mStrPathGetBasename_noext(&str, g_work.str_inputfile.buf);
		mStrAppendChar(&str, '_');

		mStrPathJoin(dst, str.buf);
		mStrFree(&str);
	}

	//連番

	mIntToStr_dig(m, layerno, dig);
	mStrAppendText(dst, m);

	//拡張子

	mStrAppendText(dst, g_outformat_ext[g_work.out_format]);
}

/* レイヤ出力時、対象レイヤかどうか */

static mlkbool _is_target_layer(int no)
{
	//対象外

	if(g_work.layerno_flags
		&& (no >= g_work.layerno_num
			|| !( g_work.layerno_flags[no >> 3] & (1 << (7 - (no & 7))) ) ) )
	{
		return FALSE;
	}

	return TRUE;
}

/** 合成画像を出力 */

mlkerr output_blendimage(void)
{
	mImageBuf2 *img;
	mStr str = MSTR_INIT;
	mlkerr ret;

	img = mImageBuf2_new(g_work.img.width, g_work.img.height,
		g_work.img.bits * 4, 0);

	if(!img) return MLKERR_ALLOC;

	//合成 (結果として 8bit RGB)

	image_layerblend(img);

	//ファイル名

	_get_output_path(&str, g_outformat_ext[g_work.out_format]);

	mPutUTF8_format_stdout("  => %s\n", str.buf);

	//保存

	ret = image_savefile(img, str.buf);

	//

	mImageBuf2_free(img);
	mStrFree(&str);

	return ret;
}

/** APDv4 出力 (APDv4 が入力の場合はスキップ) */

mlkerr output_apdfile(void)
{
	mImageBuf2 *img;
	mStr str = MSTR_INIT;
	mlkerr ret;

	img = mImageBuf2_new(g_work.img.width, g_work.img.height,
		g_work.img.bits * 4, 0);

	if(!img) return MLKERR_ALLOC;

	//合成 (結果として 8bit RGB)

	image_layerblend(img);

	//ファイル名
	// [!] --output 指定がない場合、入力が apd で同じディレクトリなら、上書き保存

	_get_output_path(&str, ".apd");

	printf("  => ");

	if(g_work.img.bits != g_work.img.srcbits)
		printf("(%dbit) ", g_work.img.bits);

	mPutUTF8_format_stdout("%s\n", str.buf);

	//出力

	ret = save_apd_v4(str.buf, img);

	//

	mImageBuf2_free(img);
	mStrFree(&str);

	return ret;
}

/** 各レイヤ画像を出力 */

mlkerr output_layerimage(void)
{
	mImageBuf2 *img;
	LayerItem *pi;
	mStr str = MSTR_INIT;
	mBox box;
	int dig,no;
	mlkerr ret;

	img = mImageBuf2_new(g_work.img.width, g_work.img.height, 32, 0);
	if(!img) return MLKERR_ALLOC;

	box.x = box.y = 0;
	box.w = g_work.img.width;
	box.h = g_work.img.height;

	//レイヤ連番の桁数
	// :番号は 0〜 のため、10個なら 0〜9

	if(g_work.img.layernum <= 10)
		dig = 1;
	else if(g_work.img.layernum <= 100)
		dig = 2;
	else
		dig = 3;

	//[!] フォルダも番号に数える

	pi = (LayerItem *)g_work.list_layer.top;
	no = 0;
	ret = MLKERR_OK;

	for(; pi; pi = (LayerItem *)mTreeItemGetNext(MTREEITEM(pi)), no++)
	{
		if(!(pi->img)) continue;

		//レイヤ番号指定がある場合、対象レイヤか

		if(!_is_target_layer(no)) continue;
	
		//RGBA 8bit 画像へ

		image_clear0(img);

		TileImage_to_RGBA8bit(pi->img, img, &box);

		//ファイル名

		_get_output_path_layer(&str, no, dig);

		mPutUTF8_format_stdout("  \"%s\":", pi->name);
		mPutUTF8_format_stdout(" %s\n", str.buf);

		//保存

		ret = image_savefile(img, str.buf);
		if(ret) break;
	}

	//

	mImageBuf2_free(img);
	mStrFree(&str);

	return ret;
}

/** レイヤ情報出力 */

void put_layerinfo(void)
{
	LayerItem *pl;
	mTreeItem *pl2;
	int no = 0;

	pl = (LayerItem *)g_work.list_layer.top;

	for(; pl; pl = (LayerItem *)mTreeItemGetNext(MTREEITEM(pl)), no++)
	{
		printf("  ");

		//ツリー

		for(pl2 = MTREEITEM(pl); pl2->parent; )
		{
			pl2 = pl2->parent;

			if(pl2->parent)
				printf(" | ");
			else
				printf(" |-");
		}

		//レイヤ番号, レイヤ名, 合成モード

		if(pl->is_folder)
		{
			//フォルダ
			
			printf("[%d] \"", no);
			mPutUTF8_stdout(pl->name);
			printf("\" <FOLDER>");
		}
		else
		{
			//通常イメージ
			
			printf("[%d] \"", no);
			mPutUTF8_stdout(pl->name);
			printf("\" <%s>", g_blendmode_name[pl->blendmode]);
		}

		//ほか

		if(!(pl->flags & LAYERITEM_F_VISIBLE))
			printf(" | hide");

		if(pl->flags & LAYERITEM_F_TEXT)
			printf(" | text");
		
		if(pl->flags & LAYERITEM_F_TONE)
			printf(" | tone");

		if(pl->texpath)
			mPutUTF8_format_stdout(" | texture(%s)", pl->texpath);

		putchar('\n');
	}

	fflush(stdout);
}

/** テクスチャ情報出力 */

void put_textureinfo(void)
{
	LayerItem *pl;
	int flag = 0;

	//テクスチャがあるか

	pl = (LayerItem *)g_work.list_layer.top;

	for(; pl; pl = (LayerItem *)mTreeItemGetNext(MTREEITEM(pl)))
	{
		if(pl->texpath)
		{
			flag = 1;
			break;
		}
	}

	if(!flag) return;

	//表示
	// :テクスチャがある場合のみ、ファイル名出力

	mPutUTF8_format_stdout("# %s\n", g_work.str_inputfile.buf);

	pl = (LayerItem *)g_work.list_layer.top;

	for(; pl; pl = (LayerItem *)mTreeItemGetNext(MTREEITEM(pl)))
	{
		if(pl->texpath)
			mPutUTF8_format_stdout("%s\n", pl->texpath);
	}

	fflush(stdout);
}


//=========================
// 読み込み用
//=========================


/** zlib 初期化 (情報表示時は作成しない) */

mlkerr init_zlib(mZlib **ppdst,void *fp,mlkbool no_header)
{
	mZlib *zlib = NULL;

	if(PROC_IS_NEEDIMAGE)
	{
		zlib = mZlibDecNew(16 * 1024, (no_header)? -15: 15);
		if(!zlib) return MLKERR_ALLOC;

		mZlibSetIO_stdio(zlib, fp);
	}

	*ppdst = zlib;

	return MLKERR_OK;
}

/** イメージのメイン情報セット
 *
 * [!] ソースのビットが異なる場合は、別途セットする。
 *
 * bits: 出力のビット
 * dpi: 0 でなし
 * layernum: 負の値でセットしない */

void set_image_info(int width,int height,int bits,int dpi,int layernum)
{
	g_work.img.width = width;
	g_work.img.height = height;
	g_work.img.bits = bits;
	g_work.img.srcbits = bits;
	g_work.img.dpi = dpi;

	if(layernum >= 0)
		g_work.img.layernum = layernum;

	TileImage_global_setBits(bits);
	TileImage_global_setDPI(dpi);
}

/** Shift-JIS から UTF-8 文字列を作成
 *
 * len: 負の値でヌル文字まで
 * return: 空文字列なら NULL */

char *dupname_shiftjis(const char *buf,int len)
{
	if(!buf || !(*buf))
		return NULL;
	
	return (char *)mConvertCharset(buf, len, "CP932", "UTF-8", NULL, 1);
}

/* レイヤアイテム初期化 */

static void _init_layeritem(LayerItem *p)
{
	if(p)
	{
		p->opacity = 128;
		p->tone_lines = 600;
		p->tone_angle = 45;
	}
}

/** ツリーの一番上にレイヤを追加
 *
 * 下層レイヤから順に読み込んでいく場合に使う。 */

LayerItem *layer_add_top(void)
{
	LayerItem *pl;

	pl = (LayerItem *)mTreeAppendNew_top(&g_work.list_layer, NULL, sizeof(LayerItem));

	_init_layeritem(pl);

	return pl;
}

/** 先頭からの親番号を指定してレイヤ追加
 *
 * no: 負の値でルート */

LayerItem *layer_add_on_parent(int no)
{
	LayerItem *pl;
	int lno;

	//親レイヤを取得

	if(no < 0)
		pl = 0;
	else
	{
		pl = (LayerItem *)g_work.list_layer.top;
		lno = 0;

		for(; pl; pl = (LayerItem *)mTreeItemGetNext(MTREEITEM(pl)), lno++)
		{
			if(lno == no) break;
		}
	}

	//追加

	pl = (LayerItem *)mTreeAppendNew(&g_work.list_layer, MTREEITEM(pl), sizeof(LayerItem));

	_init_layeritem(pl);

	return pl;
}

/** すべてのレイヤテクスチャ画像読み込み */

void load_texture_image(void)
{
	LayerItem *pl;
	char *path;
	mStr str = MSTR_INIT;

	pl = (LayerItem *)g_work.list_layer.top;

	for(; pl; pl = (LayerItem *)mTreeItemGetNext(MTREEITEM(pl)))
	{
		path = pl->texpath;
	
		if(!path) continue;
		
		if(path[0] == '/')
		{
			//システム
			mStrCopy(&str, &g_work.str_texpath_sys);
			mStrPathJoin(&str, path + 1);
		}
		else
		{
			//ユーザー
			mStrCopy(&str, &g_work.str_texpath_user);
			mStrPathJoin(&str, path);
		}

		pl->texitem = TextureList_load(&g_work.list_teximg, str.buf);
	}

	mStrFree(&str);
}


