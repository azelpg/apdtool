/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * 各定義
 *****************************/

#define PROC_IS_BLEND    (g_work.proctype == PROCTYPE_BLEND)
#define PROC_IS_APD      (g_work.proctype == PROCTYPE_APD)
#define PROC_IS_LAYER    (g_work.proctype == PROCTYPE_LAYER)
#define PROC_IS_SKIPIMAGE  (g_work.proctype == PROCTYPE_INFO || g_work.proctype == PROCTYPE_TEXINFO)
#define PROC_IS_NEEDIMAGE  (g_work.proctype != PROCTYPE_INFO && g_work.proctype != PROCTYPE_TEXINFO)

typedef struct _mZlib mZlib;
typedef struct _TileImage TileImage;
typedef struct _TexItem TexItem;


//============================
// イメージ情報

typedef struct
{
	int width,
		height,
		bits,		//出力ビット:8,16
		srcbits,	//入力ビット
		dpi,		//DPI (0 でなし)
		format,
		layernum;
	uint32_t bkgndcol;	//イメージ背景色
}ImageData;

enum
{
	FORMAT_APD_v1,
	FORMAT_APD_v2,
	FORMAT_APD_v3,
	FORMAT_APD_v4,
	FORMAT_ADW_v1,
	FORMAT_ADW_v2
};


//============================
// 作業用データ

typedef struct
{
	ImageData img;

	mTree list_layer;	//レイヤリスト
	mList list_teximg;	//テクスチャイメージリスト

	mStr str_inputfile,		//現在の入力ファイル名
		str_output,			//出力ファイル名 or ディレクトリ (空でカレントディレクトリ)
		str_layer_prefix,	//レイヤ抽出の出力ファイル名接頭語 (空で元ファイル名)
		str_texpath_sys,	//テクスチャパス (システム)
		str_texpath_user;	//テクスチャパス (ユーザー)

	int out_format,		//画像出力フォーマット
		proctype;		//処理タイプ
	uint32_t flags;

	int layerno_num;		//抽出レイヤ番号の数
	uint8_t *layerno_flags;	//抽出レイヤ番号のフラグ (NULL ですべて)
		//0 ~ layerno_num - 1 までの範囲で、1bit のフラグ。上位ビットから順。
}WorkData;

extern WorkData g_work;

/* out_format */
enum
{
	OUTFORMAT_PNG,
	OUTFORMAT_BMP,
	OUTFORMAT_PSD,
};

/* flags */
enum
{
	FLAGS_IMAGE_TO_GRAY = 1<<0,		//合成画像をグレイスケールで出力
	FLAGS_IMAGE_TO_MONO = 1<<1,		//合成画像を1bitモノクロで出力
	FLAGS_APD_TO_8BIT = 1<<2,		//APD 出力時、8bit 変換

	FLAGS_HAVE_LAYER_PREFIX = 1<<8,	//レイヤ出力ファイル名に接頭語あり
	FLAGS_OUTPUT_DIR = 1<<9			//--output がディレクトリなら ON
};

/* 処理タイプ (proctype) */
enum
{
	PROCTYPE_BLEND,		//合成絵を出力
	PROCTYPE_APD,		//APDv4 に変換
	PROCTYPE_LAYER,		//レイヤを出力
	PROCTYPE_INFO,		//レイヤ情報表示
	PROCTYPE_TEXINFO	//テクスチャ情報表示
};


//============================
// レイヤアイテム

typedef struct
{
	mTreeItem i;

	TileImage *img;		//レイヤ情報出力の場合、イメージは読み込まれないので NULL
	TexItem *texitem;	//テクスチャイメージアイテム

	char *name,		//レイヤ名
		*texpath;	//テクスチャパス

	int layerno;	//レイヤ番号
	uint32_t col,
		flags;
	int16_t tone_lines,	//線数 (1=0.1)
		tone_angle;
	uint8_t type,
		blendmode,
		opacity,		//不透明度 (0-128)
		alphamask,		//[0]off [1]アルファ維持 [2]透明保護 [3]不透明保護
		tone_density,	//トーン固定濃度 (0-100, 0でなし)
		is_folder;		//フォルダか
}LayerItem;

/* type */
enum
{
	LAYERTYPE_RGBA,
	LAYERTYPE_GRAY,
	LAYERTYPE_ALPHA,
	LAYERTYPE_ALPHA1BIT
};

/* flags (APDv4) */
enum
{
	LAYERITEM_F_VISIBLE = 1<<0,			//表示
	LAYERITEM_F_FOLDER_OPENED = 1<<1,	//フォルダ開いている
	LAYERITEM_F_LOCK     = 1<<2,		//描画ロック
	LAYERITEM_F_FILLREF  = 1<<3,		//塗りつぶし参照
	LAYERITEM_F_MASK_UNDER = 1<<4,		//下レイヤをマスクに
	LAYERITEM_F_CHECKED  = 1<<5,		//チェック
	LAYERITEM_F_TONE = 1<<6,			//トーン化
	LAYERITEM_F_TEXT = 1<<7,			//テキストレイヤ
	LAYERITEM_F_TONE_WHITE = 1<<8		//トーンの背景を白に
};


//======================
// 関数

/* main.c */

void exit_app(int ret);

/* sub.c */

void puterr_exit(const char *fmt,...);

mlkerr output_blendimage(void);
mlkerr output_apdfile(void);
mlkerr output_layerimage(void);
void put_layerinfo(void);
void put_textureinfo(void);

mlkerr init_zlib(mZlib **ppdst,void *fp,mlkbool no_header);
void set_image_info(int width,int height,int bits,int dpi,int layernum);
char *dupname_shiftjis(const char *buf,int len);
LayerItem *layer_add_top(void);
LayerItem *layer_add_on_parent(int no);
void load_texture_image(void);

/* image.c */

void color_8to16(uint16_t *dst,uint32_t col);

void image_clear0(mImageBuf2 *p);
void image_clear8bit(mImageBuf2 *p,uint32_t col);
void image_clear16bit(mImageBuf2 *p,uint32_t col);
mlkerr image_savefile(mImageBuf2 *img,const char *filename);
void image_layerblend(mImageBuf2 *imgdst);
mlkbool image_conv_thumbnail(mImageBuf2 *p,int width,int height);

/* texture.c */

void TextureList_init(mList *list);
TexItem *TextureList_load(mList *list,const char *path);
int TextureItem_getOpacity(TexItem *pi,int x,int y);

