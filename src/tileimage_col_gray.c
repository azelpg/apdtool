/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * TileImage
 * GRAY+A タイプ
 *****************************************/

#include "mlk.h"

#include "tileimage.h"
#include "pv_tileimage.h"
#include "tonetable.h"


int TextureItem_getOpacity(TexItem *pi,int x,int y);


//==========================
// 8bit
//==========================


/** すべて透明か */

static mlkbool _8bit_is_transparent_tile(uint8_t *tile)
{
	int i;
	
	tile += 1;

	for(i = 64 * 64; i; i--, tile += 2)
	{
		if(*tile) return FALSE;
	}

	return TRUE;
}

/** 合成 */

static void _8bit_blend_tile(TileImage *p,TileImageBlendInfo *infosrc)
{
	TileImageBlendInfo info;
	uint8_t **ppdst,*ps,*pd,r,g,b;
	int pitchs,ix,iy,dx,dy,a,c,dstx,cx,cy,thval;
	int32_t src[3],dst[3];
	int64_t fxx,fxy;

	info = *infosrc;

	ps = info.tile + (info.sy * 64 + info.sx) * 2;
	ppdst = info.dstbuf;
	dstx = info.dx * 4;
	pitchs = (64 - info.w) * 2;

	r = p->col.c8.r;
	g = p->col.c8.g;
	b = p->col.c8.b;

	//

	for(iy = info.h, dy = info.dy; iy; iy--, dy++)
	{
		pd = *ppdst + dstx;
		fxx = info.tone_fx;
		fxy = info.tone_fy;
		
		for(ix = info.w, dx = info.dx; ix; ix--, dx++, ps += 2, pd += 4,
				fxx += info.tone_fcos, fxy += info.tone_fsin)
		{
			a = ps[1];
			if(!a) continue;

			if(info.texitem)
				a = a * TextureItem_getOpacity(info.texitem, dx, dy) / 255;

			a = a * info.opacity >> 7;

			if(!a) continue;

			//トーン化

			if(!info.is_tone)
			{
				//通常 or トーンをグレイスケール表示
				
				if(info.tone_repcol == -1)
					c = ps[0];
				else
					c = info.tone_repcol;

				src[0] = src[1] = src[2] = c;
			}
			else
			{
				//トーン表示
				
				c = (info.tone_density)? 255 - info.tone_density: ps[0];

				cx = fxx >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);
				cy = fxy >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);

				if(c < 128)
				{
					cx += TABLEDATA_TONE_WIDTH / 2;
					cy += TABLEDATA_TONE_WIDTH / 2;
				}

				thval = TABLEDATA_TONE_GETVAL8(cx, cy);

				if(c < 128)
					thval = 255 - thval;
				
				if(c > thval)
				{
					//透明 or 白

					if(info.is_tone_bkgnd_tp) continue;

					src[0] = src[1] = src[2] = 255;
				}
				else
				{
					//黒 = レイヤ色
					
					src[0] = r;
					src[1] = g;
					src[2] = b;
				}
			}

			//RGB 合成

			dst[0] = pd[0];
			dst[1] = pd[1];
			dst[2] = pd[2];

			if((info.func_blend)(src, dst, a) && a != 255)
			{
				//アルファ合成

				src[0] = ((src[0] - dst[0]) * a / 255) + dst[0];
				src[1] = ((src[1] - dst[1]) * a / 255) + dst[1];
				src[2] = ((src[2] - dst[2]) * a / 255) + dst[2];
			}

			//セット

			pd[0] = src[0];
			pd[1] = src[1];
			pd[2] = src[2];
		}

		ps += pitchs;
		ppdst++;

		info.tone_fx -= info.tone_fsin;
		info.tone_fy += info.tone_fcos;
	}
}

/** (レイヤ画像用) タイルを、RGBA 8bit イメージに変換 */

static void _8bit_to_rgba8(TileImage *p,uint8_t **ppbuf,int dx,int w,int h,uint8_t *tile,int sx,int sy)
{
	uint8_t *ps,*pd;
	int ix,iy,pitchs;

	ps = tile + (sy * 64 + sx) * 2;
	pitchs = (64 - w) * 2;
	dx *= 4;

	for(iy = h; iy; iy--)
	{
		pd = *ppbuf + dx;
	
		for(ix = w; ix; ix--, pd += 4, ps += 2)
		{
			pd[0] = pd[1] = pd[2] = ps[0];
			pd[3] = ps[1];
		}

		ps += pitchs;
		ppbuf++;
	}
}

/** ファイル保存用タイルデータからタイルセット
 *
 * 各チャンネルごとに 64x64 分並んでいる */

static void _8bit_convert_from_save(uint8_t *dst,uint8_t *src)
{
	uint8_t *pd;
	int i,j;

	for(j = 0; j < 2; j++)
	{
		pd = dst + j;
	
		for(i = 64 * 64; i; i--, pd += 2)
			*pd = *(src++);
	}
}

/** ファイル保存用にタイルデータを変換 */

static void _8bit_convert_to_save(uint8_t *dst,uint8_t *src)
{
	int ch,i;
	uint8_t *ps;

	for(ch = 0; ch < 2; ch++)
	{
		ps = src + ch;

		for(i = 64 * 64; i; i--, ps += 2)
			*(dst++) = *ps;
	}
}


//==========================
// 16bit
//==========================


/** すべて透明か */

static mlkbool _16bit_is_transparent_tile(uint8_t *tile)
{
	uint16_t *ps = (uint16_t *)tile + 1;
	int i;

	for(i = 64 * 64; i; i--, ps += 2)
	{
		if(*ps) return FALSE;
	}

	return TRUE;
}

/** 合成 */

static void _16bit_blend_tile(TileImage *p,TileImageBlendInfo *infosrc)
{
	TileImageBlendInfo info;
	uint16_t **ppdst,*ps,*pd,r,g,b;
	int pitchs,ix,iy,dx,dy,a,c,dstx,cx,cy,thval;
	int32_t src[3],dst[3];
	int64_t fxx,fxy;

	info = *infosrc;

	ps = (uint16_t *)info.tile + (info.sy * 64 + info.sx) * 2;
	ppdst = (uint16_t **)info.dstbuf;
	dstx = info.dx * 4;
	pitchs = (64 - info.w) * 2;

	r = p->col.c16.r;
	g = p->col.c16.g;
	b = p->col.c16.b;

	//

	for(iy = info.h, dy = info.dy; iy; iy--, dy++)
	{
		pd = *ppdst + dstx;
		fxx = info.tone_fx;
		fxy = info.tone_fy;
		
		for(ix = info.w, dx = info.dx; ix; ix--, dx++, ps += 2, pd += 4,
				fxx += info.tone_fcos, fxy += info.tone_fsin)
		{
			a = ps[1];
			if(!a) continue;

			if(info.texitem)
				a = a * TextureItem_getOpacity(info.texitem, dx, dy) / 255;

			a = a * info.opacity >> 7;

			if(!a) continue;

			//トーン化

			if(!info.is_tone)
			{
				if(info.tone_repcol == -1)
					c = ps[0];
				else
					c = info.tone_repcol;

				src[0] = src[1] = src[2] = c;
			}
			else
			{
				//トーン表示
				
				c = (info.tone_density)? 0x8000 - info.tone_density: ps[0];

				cx = fxx >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);
				cy = fxy >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);

				if(c < 0x4000)
				{
					cx += TABLEDATA_TONE_WIDTH / 2;
					cy += TABLEDATA_TONE_WIDTH / 2;
				}

				thval = TABLEDATA_TONE_GETVAL16(cx, cy);

				if(c < 0x4000)
					thval = 0x8000 - thval;
				
				if(c > thval)
				{
					//透明 or 白

					if(info.is_tone_bkgnd_tp) continue;

					src[0] = src[1] = src[2] = 0x8000;
				}
				else
				{
					src[0] = r;
					src[1] = g;
					src[2] = b;
				}
			}

			//RGB 合成

			dst[0] = pd[0];
			dst[1] = pd[1];
			dst[2] = pd[2];

			if((info.func_blend)(src, dst, a) && a != 0x8000)
			{
				//アルファ合成

				src[0] = ((src[0] - dst[0]) * a >> 15) + dst[0];
				src[1] = ((src[1] - dst[1]) * a >> 15) + dst[1];
				src[2] = ((src[2] - dst[2]) * a >> 15) + dst[2];
			}

			//セット

			pd[0] = src[0];
			pd[1] = src[1];
			pd[2] = src[2];
		}

		ps += pitchs;
		ppdst++;

		info.tone_fx -= info.tone_fsin;
		info.tone_fy += info.tone_fcos;
	}
}

/** (レイヤ画像用) タイルを、RGBA 8bit イメージに変換 */

static void _16bit_to_rgba8(TileImage *p,uint8_t **ppbuf,int dx,int w,int h,uint8_t *tile,int sx,int sy)
{
	uint8_t *pd;
	uint16_t *ps;
	int ix,iy,pitchs;

	ps = (uint16_t *)tile + (sy * 64 + sx) * 2;
	pitchs = (64 - w) * 2;
	dx *= 4;

	for(iy = h; iy; iy--)
	{
		pd = *ppbuf + dx;
	
		for(ix = w; ix; ix--, pd += 4, ps += 2)
		{
			pd[0] = pd[1] = pd[2] = COLCONV_16TO8(ps[0]);
			pd[3] = COLCONV_16TO8(ps[1]);
		}

		ps += pitchs;
		ppbuf++;
	}
}

/** ファイル保存用タイルデータからタイルセット
 *
 * 各チャンネルごとに 64x64 分 BE で並んでいる */

static void _16bit_convert_from_save(uint8_t *dst,uint8_t *src)
{
	uint16_t *pd;
	int i,j;

	for(j = 0; j < 2; j++)
	{
		pd = (uint16_t *)dst + j;
	
		for(i = 64 * 64; i; i--, pd += 2, src += 2)
			*pd = (src[0] << 8) | src[1];
	}
}

/** ファイル保存用にタイルデータを変換 (BE) */

static void _16bit_convert_to_save(uint8_t *dst,uint8_t *src)
{
	int ch,i;
	uint16_t *ps;

	for(ch = 0; ch < 2; ch++)
	{
		ps = (uint16_t *)src + ch;

		for(i = 64 * 64; i; i--, ps += 2, dst += 2)
		{
			dst[0] = *ps >> 8;
			dst[1] = *ps & 255;
		}
	}
}

/** ファイル保存用タイルデータから 16bit->8bit 変換してセット */

static void _convert_16to8(uint8_t *dst,uint8_t *src)
{
	uint8_t *pd;
	int i,j;

	for(j = 0; j < 2; j++)
	{
		pd = dst + j;
	
		for(i = 64 * 64; i; i--, pd += 2, src += 2)
		{
			*pd = COLCONV_16TO8((src[0] << 8) | src[1]);
		}
	}
}


//==========================
// 関数をセット
//==========================


/** 関数をセット */

void __TileImage_setColFunc_Gray(TileImageColFuncData *p,int bits)
{
	p->convert_16to8 = _convert_16to8;

	if(bits == 8)
	{
		p->is_transparent_tile = _8bit_is_transparent_tile;
		p->blend_tile = _8bit_blend_tile;
		p->to_rgba8 = _8bit_to_rgba8;
		p->convert_from_save = _8bit_convert_from_save;
		p->convert_to_save = _8bit_convert_to_save;
	}
	else
	{
		p->is_transparent_tile = _16bit_is_transparent_tile;
		p->blend_tile = _16bit_blend_tile;
		p->to_rgba8 = _16bit_to_rgba8;
		p->convert_from_save = _16bit_convert_from_save;
		p->convert_to_save = _16bit_convert_to_save;
	}
}


