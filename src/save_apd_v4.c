/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * APD v4 保存
 ********************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_stdio.h"
#include "mlk_zlib.h"
#include "mlk_util.h"
#include "mlk_tree.h"
#include "mlk_imagebuf.h"
#include "mlk_rectbox.h"

#include "def.h"
#include "blendmode.h"
#include "tileimage.h"

#include "pv_apd_format.h"


//-----------------

typedef struct
{
	FILE *fp;
	mZlib *zlib;
	uint8_t *tilebuf;

	uint32_t tilenum, //総数
		curtsize;
	int curtnum;
	off_t fpos;
}_data;

//-----------------



/* 先頭情報書き込み */

static mlkerr _write_headinfo(_data *p)
{
	FILE *fp = p->fp;
	uint8_t *buf = p->tilebuf;
	uint32_t col;

	//ヘッダ

	fputs("AZPDATA", fp);

	mFILEwriteByte(fp, 3);

	//

	col = g_work.img.bkgndcol;

	mSetBuf_format(buf, ">hiiibbbbbh",
		19, g_work.img.width, g_work.img.height,
		(g_work.img.dpi)? g_work.img.dpi: 96,
		g_work.img.bits, 0,
		MLK_RGB_R(col), MLK_RGB_G(col), MLK_RGB_B(col),
		g_work.img.layernum);

	mFILEwriteOK(fp, buf, 19 + 2);

	return MLKERR_OK;
}


//------- チャンク


/* (チャンク) サムネイル画像書き込み */

mlkerr _write_chunk_thumbnail(_data *p,uint8_t **ppbuf,int width,int height)
{
	mlkerr ret;
	int size,iy;
	off_t pos;

	pos = ftello(p->fp) + 4;

	//情報

	size = mSetBuf_format(p->tilebuf, ">iihhi",
		_MAKE_ID('t','h','u','m'), 0,
		width, height, 0);

	if(mFILEwriteOK(p->fp, p->tilebuf, size))
		return MLKERR_IO;

	//圧縮

	size = width * 3;

	mZlibEncReset(p->zlib);

	for(iy = height; iy; iy--, ppbuf++)
	{
		ret = mZlibEncSend(p->zlib, *ppbuf, size);
		if(ret) return ret;
	}

	ret = mZlibEncFinish(p->zlib);
	if(ret) return ret;

	//サイズ

	size = mZlibEncGetSize(p->zlib);

	if(fseeko(p->fp, pos, SEEK_SET)
		|| mFILEwriteBE32(p->fp, 8 + size) //チャンクサイズ
		|| fseek(p->fp, 4, SEEK_CUR)
		|| mFILEwriteBE32(p->fp, size) //圧縮サイズ
		|| fseek(p->fp, 0, SEEK_END))
		return MLKERR_IO;

	return MLKERR_OK;
}

/* チャンク終端を書き込み */

static mlkerr _write_chunk_end(_data *p)
{
	mFILEwrite0(p->fp, 4);

	return MLKERR_OK;
}


//------ 拡張データ

/* 拡張データ:文字列書き込み
 *
 * text: NULL で書き込まない */

static mlkerr _write_layerex_str(_data *p,uint32_t id,const char *text)
{
	int len;

	if(text)
	{
		len = mStrlen(text);

		if(mFILEwriteBE32(p->fp, id)
			|| mFILEwriteBE32(p->fp, len)
			|| mFILEwriteOK(p->fp, text, len))
			return MLKERR_IO;
	}

	return MLKERR_OK;
}

/* 拡張データ書き込み */

static mlkerr _write_layerex(_data *p,LayerItem *pi)
{
	mlkerr ret;

	//名前

	ret = _write_layerex_str(p, _MAKE_ID('n','a','m','e'), pi->name);
	if(ret) return ret;

	//テクスチャパス

	ret = _write_layerex_str(p, _MAKE_ID('t','e','x','p'), pi->texpath);
	if(ret) return ret;

	//終端

	if(mFILEwrite0(p->fp, 4)) return MLKERR_IO;

	return MLKERR_OK;
}

//------ タイルイメージ

/* タイル書き込み関数 */

static mlkerr _func_savetile(TileImage *img,void *param)
{
	_data *p = (_data *)param;
	FILE *fp = p->fp;
	mlkerr ret;

	//先頭時、情報を仮書き込み

	if(p->curtnum == 0)
	{
		p->fpos = ftello(fp);

		if(mFILEwrite0(fp, 6))
			return MLKERR_IO;

		mZlibEncReset(p->zlib);
	}

	//圧縮

	ret = mZlibEncSend(p->zlib, p->tilebuf, img->tilesize + 4);
	if(ret) return ret;

	p->tilenum--;
	p->curtnum++;
	p->curtsize += img->tilesize;

	//ブロック終わり

	if(p->tilenum == 0 || p->curtsize >= 0x100000 || p->curtnum == 0xffff)
	{
		ret = mZlibEncFinish(p->zlib);
		if(ret) return ret;
		
		if(fseeko(fp, p->fpos, SEEK_SET)
			|| mFILEwriteBE16(fp, p->curtnum)
			|| mFILEwriteBE32(fp, mZlibEncGetSize(p->zlib))
			|| fseek(fp, 0, SEEK_END))
			return MLKERR_IO;

		p->curtnum = 0;
		p->curtsize = 0;
	}

	return MLKERR_OK;
}

/* タイルイメージ書き込み */

static mlkerr _write_tileimage(_data *p,TileImage *img,mRect *rc,uint32_t tilenum)
{
	//圧縮タイプ, タイル総数

	if(mFILEwriteByte(p->fp, 0)
		|| mFILEwriteBE32(p->fp, tilenum))
		return MLKERR_IO;

	//空イメージ

	if(!tilenum)
		return MLKERR_OK;

	//タイル書き込み

	p->tilenum = tilenum;
	p->curtsize = 0;
	p->curtnum = 0;

	return TileImage_saveTiles_apd4(img, rc, p->tilebuf, _func_savetile, p);
}

/* 合成モードの ID を取得 */

static uint32_t _get_blendmode_id(int no)
{
	if(no == BLENDMODE_LUM_SUB)
		//APDv1 の減算 -> 減算
		no = BLENDMODE_SUB;
	else if(no >= BLENDMODE_APDv4_NUM)
		//APDv1 のそのほか -> 通常
		no = BLENDMODE_NORMAL;

	return g_blendmode_id[no];
}

/* レイヤの書き込み */

static mlkerr _write_layer(_data *p,LayerItem *pi)
{
	uint32_t tilenum;
	uint16_t parent;
	uint8_t lflags;
	int size,fimg;
	mlkerr ret;
	mRect rc;

	fimg = (pi->img != NULL);

	//親のレイヤ番号

	if(!pi->i.parent)
		parent = 0xfffe;
	else
		parent = ((LayerItem *)pi->i.parent)->layerno;

	//フラグ

	lflags = 0;

	if(pi->is_folder) lflags |= 1;

	if(pi == (LayerItem *)g_work.list_layer.top) lflags |= 2;

	//イメージ範囲

	if(fimg && TileImage_getHaveImageRect_pixel(pi->img, &rc, &tilenum))
	{
		rc.x2++;
		rc.y2++;
	}
	else
		mMemset0(&rc, sizeof(mRect));

	//情報

	size = mSetBuf_format(p->tilebuf,
		">hbbiiiibbiiihhb",
		parent, lflags, pi->type,
		rc.x1, rc.y1, rc.x2, rc.y2,
		pi->opacity, pi->alphamask,
		_get_blendmode_id(pi->blendmode),
		pi->col, pi->flags,
		pi->tone_lines, pi->tone_angle, pi->tone_density);

	if(mFILEwriteOK(p->fp, p->tilebuf, size))
		return MLKERR_IO;

	//拡張データ

	ret = _write_layerex(p, pi);
	if(ret) return ret;

	//イメージデータ

	if(fimg)
	{
		ret = _write_tileimage(p, pi->img, &rc, tilenum);
		if(ret) return ret;
	}

	return MLKERR_OK;
}

/* レイヤ終端の書き込み */

static mlkerr _write_layer_end(_data *p)
{
	mFILEwriteBE16(p->fp, 0xffff);

	return MLKERR_OK;
}


//===========================
// main
//===========================


/* 閉じる */

static void _close(_data *p)
{
	if(p)
	{
		if(p->fp) fclose(p->fp);

		mZlibFree(p->zlib);
		mFree(p->tilebuf);
		
		mFree(p);
	}
}

/* 初期化 */

static mlkerr _init(_data *p,const char *filename)
{
	//開く

	p->fp = mFILEopen(filename, "wb");
	if(!p->fp) return MLKERR_OPEN;

	//作業用バッファ

	p->tilebuf = (uint8_t *)mMalloc(64 * 64 * 8 + 4); //タイル位置分 +4
	if(!p->tilebuf) return MLKERR_ALLOC;

	//zlib

	p->zlib = mZlibEncNew(8192, 6, -15, 8, 0);
	if(!p->zlib) return MLKERR_ALLOC;

	mZlibSetIO_stdio(p->zlib, p->fp);

	return MLKERR_OK;
}

/* メイン処理 */

static mlkerr _main_proc(_data *p,mImageBuf2 *img)
{
	LayerItem *pi;
	mBox box;
	mlkerr ret;

	//ヘッダ

	ret = _write_headinfo(p);
	if(ret) return ret;

	//チャンク (サムネイル画像)

	box.x = box.y = 0;
	box.w = g_work.img.width;
	box.h = g_work.img.height;

	mBoxResize_keepaspect(&box, 100, 100, TRUE); 

	image_conv_thumbnail(img, box.w, box.h);

	ret = _write_chunk_thumbnail(p, img->ppbuf, box.w, box.h);
	if(ret) return ret;

	//チャンク終了

	_write_chunk_end(p);

	//レイヤ

	pi = (LayerItem *)g_work.list_layer.top;

	for(; pi; pi = (LayerItem *)mTreeItemGetNext(MTREEITEM(pi)))
	{
		ret = _write_layer(p, pi);
		if(ret) return ret;
	}

	_write_layer_end(p);

	return ret;
}

/** APD v4 保存 */

mlkerr save_apd_v4(const char *filename,mImageBuf2 *img)
{
	_data *p;
	mlkerr ret;

	p = (_data *)mMalloc0(sizeof(_data));
	if(!p) return MLKERR_ALLOC;

	//初期化

	ret = _init(p, filename);
	if(ret) goto ERR;

	//処理メイン

	ret = _main_proc(p, img);

ERR:
	_close(p);

	return ret;
}

