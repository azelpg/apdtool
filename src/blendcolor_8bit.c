/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * レイヤの色合成 (8bit)
 *****************************************/

#include "mlk.h"

#include "blendmode.h"


#define _MAXVAL  255
#define _HALFVAL 128

/* dst の上に src を合成して、結果を src にセット。
 * 
 * 戻り値: TRUE でアルファ合成を行う */


/** 通常 */

static mlkbool _normal(int32_t *src,int32_t *dst,int a)
{
	return TRUE;
}

/** 乗算 */

static mlkbool _mul(int32_t *src,int32_t *dst,int a)
{
	src[0] = src[0] * dst[0] / 255;
	src[1] = src[1] * dst[1] / 255;
	src[2] = src[2] * dst[2] / 255;

	return TRUE;
}

/** 加算 */

static mlkbool _add(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = src[i] + dst[i];
		if(n > _MAXVAL) n = _MAXVAL;

		src[i] = n;
	}

	return TRUE;
}

/** 減算 */

static mlkbool _sub(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst[i] - src[i];
		if(n < 0) n = 0;

		src[i] = n;
	}

	return TRUE;
}

/** スクリーン */

static mlkbool _screen(int32_t *src,int32_t *dst,int a)
{
	int i,s,d;

	for(i = 0; i < 3; i++)
	{
		s = src[i];
		d = dst[i];
		
		src[i] = s + d - (s * d / 255);
	}

	return TRUE;
}

/** オーバーレイ */

static mlkbool _overlay(int32_t *src,int32_t *dst,int a)
{
	int i,s,d;

	for(i = 0; i < 3; i++)
	{
		s = src[i];
		d = dst[i];
		
		if(d < _HALFVAL)
			src[i] = s * d >> 7;
		else
			src[i] = _MAXVAL - ((_MAXVAL - d) * (_MAXVAL - s) >> 7);
	}

	return TRUE;
}

/** ハードライト */

static mlkbool _hardlight(int32_t *src,int32_t *dst,int a)
{
	int i,s,d;

	for(i = 0; i < 3; i++)
	{
		s = src[i];
		d = dst[i];
		
		if(s < _HALFVAL)
			src[i] = s * d >> 7;
		else
			src[i] = _MAXVAL - ((_MAXVAL - d) * (_MAXVAL - s) >> 7);
	}

	return TRUE;
}

/** ソフトライト */

static mlkbool _softlight(int32_t *src,int32_t *dst,int a)
{
	int i,s,d,n;

	for(i = 0; i < 3; i++)
	{
		s = src[i];
		d = dst[i];

		n = s * d / 255;

		src[i] = n + (d * (_MAXVAL - n - (_MAXVAL - s) * (_MAXVAL - d) / 255) / 255);
	}

	return TRUE;
}

/** 覆い焼き */

static mlkbool _dodge(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		if(src[i] != _MAXVAL)
		{
			n = dst[i] * 255 / (_MAXVAL - src[i]);
			if(n > _MAXVAL) n = _MAXVAL;

			src[i] = n;
		}
	}

	return TRUE;
}

/** 焼き込み */

static mlkbool _burn(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		if(src[i])
		{
			n = _MAXVAL - (_MAXVAL - dst[i]) * 255 / src[i];
			if(n < 0) n = 0;

			src[i] = n;
		}
	}

	return TRUE;
}

/** 焼き込みリニア */

static mlkbool _linearburn(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst[i] + src[i] - _MAXVAL;
		if(n < 0) n = 0;
		
		src[i] = n;
	}

	return TRUE;
}

/** ビビットライト */

static mlkbool _vividlight(int32_t *src,int32_t *dst,int a)
{
	int i,s,d,n;

	for(i = 0; i < 3; i++)
	{
		s = src[i];
		d = dst[i];

		if(s < _HALFVAL)
		{
			n = _MAXVAL - (s << 1);

			if(d <= n || s == 0)
				n = 0;
			else
				n = (d - n) * 255 / (s << 1);
		}
		else
		{
			n = _MAXVAL * 2 - (s << 1);

			if(d >= n || n == 0)
				n = _MAXVAL;
			else
				n = d * 255 / n;
		}

		src[i] = n;
	}

	return TRUE;
}

/** リニアライト */

static mlkbool _linearlight(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = (src[i] << 1) + dst[i] - _MAXVAL;

		if(n < 0) n = 0;
		else if(n > _MAXVAL) n = _MAXVAL;
		
		src[i] = n;
	}

	return TRUE;
}

/** ピンライト */

static mlkbool _pinlight(int32_t *src,int32_t *dst,int a)
{
	int i,s,d,n;

	for(i = 0; i < 3; i++)
	{
		s = src[i];
		d = dst[i];

		if(s > _HALFVAL)
		{
			n = (s << 1) - _MAXVAL;
			if(n < d) n = d;
		}
		else
		{
			n = s << 1;
			if(n > d) n = d;
		}

		src[i] = n;
	}

	return TRUE;
}

/** 比較(暗) */

static mlkbool _darken(int32_t *src,int32_t *dst,int a)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(dst[i] < src[i])
			src[i] = dst[i];
	}

	return TRUE;
}

/** 比較(明) */

static mlkbool _lighten(int32_t *src,int32_t *dst,int a)
{
	int i;

	for(i = 0; i < 3; i++)
	{
		if(dst[i] > src[i])
			src[i] = dst[i];
	}

	return TRUE;
}

/** 差の絶対値 */

static mlkbool _difference(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = src[i] - dst[i];
		if(n < 0) n = -n;

		src[i] = n;
	}

	return TRUE;
}

/** 発光(加算) */

static mlkbool _luminous_add(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = src[i] * a / 255 + dst[i];
		if(n > _MAXVAL) n = _MAXVAL;

		src[i] = n;
	}

	return FALSE;
}

/** 発光(覆い焼き) */

static mlkbool _luminous_dodge(int32_t *src,int32_t *dst,int a)
{
	int i,s,n;

	for(i = 0; i < 3; i++)
	{
		s = src[i] * a / 255;

		if(s == _MAXVAL)
			n = _MAXVAL;
		else
		{
			n = dst[i] * 255 / (_MAXVAL - s);
			if(n > _MAXVAL) n = _MAXVAL;
		}

		src[i] = n;
	}

	return FALSE;
}

/** 減算 (A)
 *
 * AzPainter(Windows) の減算 */

static mlkbool _luminous_sub(int32_t *src,int32_t *dst,int a)
{
	int i,n;

	for(i = 0; i < 3; i++)
	{
		n = dst[i] - src[i] * a / 255;
		if(n < 0) n = 0;
	
		src[i] = n;
	}
	
	return FALSE;
}


//=======================


/* RGB -> HSL */

static void _rgb_to_hsl(int r,int g,int b,float *dst)
{
	float h,s,fr,fg,fb,min,max;

	fr = r / 255.0f;
	fg = g / 255.0f;
	fb = b / 255.0f;

	dst[2] = fr * 0.3f + fg * 0.59f + fb * 0.11f;

	max = (r >= g)? fr: fg; if(fb > max) max = fb;
	min = (r <= g)? fr: fg; if(fb < min) min = fb;

	s = max - min;
	dst[1] = s;

	if(s == 0)
	{
		dst[0] = 0;
		return;
	}

	if(fr == max)
		h = (fg - fb) / s;
	else if(fg == max)
		h = (fb - fr) / s + 2.0f;
	else
		h = (fr - fg) / s + 4.0f;

	if(h < 0) h += 6.0f;
	
	dst[0] = h;
}

/* HSL -> RGB */

void _hsl_to_rgb(float *hsl,int32_t *dst)
{
    float r,g,b,h,s,l,max,min,x,t;
    int n;

	h = hsl[0];
	s = hsl[1];
	l = hsl[2];

	if(h < 0 || s == 0)
	{
		dst[0] = dst[1] = dst[2] = (int)(l * 255 + 0.5f);
		return;
	}

	//

	if(h <= 1.0f)
	{
		b = -0.3f - h * 0.59f;
		r = 1.0f + b;
		g = b + h;
	}
	else if(h <= 2.0f)
	{
		h -= 2.0f;

		b = -0.59f + h * 0.3f;
		r = b - h;
		g = 1.0f + b;
	}
	else if(h <= 3.0f)
	{
		h -= 2.0f;
		
		r = -0.59f - h * 0.11f;
		g = 1.0f + r;
		b = r + h;
	}
	else if(h <= 4.0f)
	{
		h -= 4.0f;
		
		r = -0.11f + h * 0.59f;
		g = r - h;
		b = 1.0f + r;
	}
	else if(h <= 5.0f)
	{
		h -= 4.0f;
		
		g = -0.11f - h * 0.3f;
		r = g + h;
		b = 1.0f + g;
	}
	else
	{
		h -= 6.0f;
		
		g = -0.3f + h * 0.11f;
		r = 1.0f + g;
		b = g - h;
	}

	//

	r *= s;
	g *= s;
	b *= s;

	max = (r >= g)? r: g; if(b > max) max = b;
	min = (r <= g)? r: g; if(b < min) min = b;

	if(max + l > 1.0f)
		x = (1.0f - l) / max;
	else
		x = 1.0f;

	if(min + l < 0)
	{
		t = l / (-min);
		if(t < x) x = t;
	}

	if(x != 1.0f)
	{
		r *= x;
		g *= x;
		b *= x;
	}

	r += l;
	g += l;
	b += l;

	//

	n = (int)(r * 255 + 0.5f);
	if(n < 0) n = 0; else if(n > 255) n = 255;
	dst[0] = n;

	n = (int)(g * 255 + 0.5f);
	if(n < 0) n = 0; else if(n > 255) n = 255;
	dst[1] = n;

	n = (int)(b * 255 + 0.5f);
	if(n < 0) n = 0; else if(n > 255) n = 255;
	dst[2] = n;
}

/** 色相 */

static mlkbool _hue(int32_t *src,int32_t *dst,int a)
{
	float fs[3],fd[3];

	_rgb_to_hsl(src[0], src[1], src[2], fs);
	_rgb_to_hsl(dst[0], dst[1], dst[2], fd);

	if(fs[1] == 0)
		//彩度が0の場合
		fd[1] = 0;
	else
		fd[0] = fs[0];

	_hsl_to_rgb(fd, src);
	
	return TRUE;
}

/** 彩度 */

static mlkbool _saturation(int32_t *src,int32_t *dst,int a)
{
	float fs[3],fd[3];

	_rgb_to_hsl(src[0], src[1], src[2], fs);
	_rgb_to_hsl(dst[0], dst[1], dst[2], fd);

	fd[1] = fs[1];

	_hsl_to_rgb(fd, src);
	
	return TRUE;
}

/** カラー */

static mlkbool _color(int32_t *src,int32_t *dst,int a)
{
	float fs[3];

	_rgb_to_hsl(src[0], src[1], src[2], fs);

	fs[2] = (dst[0] * 0.3f + dst[1] * 0.59f + dst[2] * 0.11f) / 255.0f;

	_hsl_to_rgb(fs, src);
	
	return TRUE;
}

/** 輝度 */

static mlkbool _luminous(int32_t *src,int32_t *dst,int a)
{
	float fd[3];

	_rgb_to_hsl(dst[0], dst[1], dst[2], fd);

	fd[2] = (src[0] * 0.3f + src[1] * 0.59f + src[2] * 0.11f) / 255.0f;

	_hsl_to_rgb(fd, src);
	
	return TRUE;
}


/** 関数テーブルをセット */

void BlendColorFunc_setTable_8bit(BlendColorFunc *p)
{
	p[0] = _normal;
	p[1] = _mul;
	p[2] = _add;
	p[3] = _sub;
	p[4] = _screen;
	p[5] = _overlay;
	p[6] = _hardlight;
	p[7] = _softlight;
	p[8] = _dodge;
	p[9] = _burn;
	p[10] = _linearburn;
	p[11] = _vividlight;
	p[12] = _linearlight;
	p[13] = _pinlight;
	p[14] = _darken;
	p[15] = _lighten;
	p[16] = _difference;
	p[17] = _luminous_add;
	p[18] = _luminous_dodge;

	//APDv1
	p[BLENDMODE_LUM_SUB] = _luminous_sub;
	p[BLENDMODE_HUE] = _hue;
	p[BLENDMODE_SATURATION] = _saturation;
	p[BLENDMODE_COLOR] = _color;
	p[BLENDMODE_LUM] = _luminous;
}

