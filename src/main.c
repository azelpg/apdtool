/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * apdtool
 *****************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_tree.h"
#include "mlk_charset.h"
#include "mlk_stdio.h"

#include "def.h"
#include "tileimage.h"
#include "tonetable.h"


//---------------

WorkData g_work;

//---------------

int get_app_options(int argc,char **argv);

mlkerr load_apd_v1(FILE *fp);
mlkerr load_apd_v2(FILE *fp);
mlkerr load_apd_v3(FILE *fp);
mlkerr load_apd_v4(FILE *fp);
mlkerr load_adw_v1(FILE *fp);
mlkerr load_adw_v2(FILE *fp);

static mlkerr (*g_func_load[])(FILE *) = {
	load_apd_v1, load_apd_v2, load_apd_v3, load_apd_v4,
	load_adw_v1, load_adw_v2
};

enum
{
	_ERR_FORMAT = -1,
	_ERR_VERSION = -2,
	_ERR_SKIP_APDv4 = -3
};

//---------------



/* ファイルからヘッダをチェック
 *
 * return: タイプ。負の値でエラー */

static int _check_header(FILE *fp)
{
	char m[8];
	uint8_t ver;
	int is_adw;

	//ヘッダ

	if(mFILEreadOK(fp, m, 7)
		|| mFILEreadByte(fp, &ver))
		return _ERR_FORMAT;

	if(strncmp(m, "AZPDATA", 7) == 0)
		is_adw = 0;
	else if(strncmp(m, "AZDWDAT", 7) == 0)
		is_adw = 1;
	else
		return _ERR_FORMAT;

	//バージョン

	if((!is_adw && ver > 3)
		|| (is_adw && ver > 1))
		return _ERR_VERSION;

	return (is_adw)? FORMAT_ADW_v1 + ver: FORMAT_APD_v1 + ver;
}

/* 読み込み成功時の処理 */

static void _after_load_file(void)
{
	LayerItem *pi;
	int no,ftonelayer = FALSE;

	//レイヤ番号のセット/トーンレイヤがあるか

	pi = (LayerItem *)g_work.list_layer.top;
	no = 0;

	for(; pi; pi = (LayerItem *)mTreeItemGetNext(MTREEITEM(pi)), no++)
	{
		pi->layerno = no;

		if((pi->flags & LAYERITEM_F_TONE)
			&& (pi->type == LAYERTYPE_GRAY || pi->type == LAYERTYPE_ALPHA1BIT))
			ftonelayer = TRUE;
	}

	//合成画像、APD 出力時(サムネイル画像で使う)

	if(PROC_IS_BLEND || PROC_IS_APD)
	{
		//トーンレイヤがある場合、トーンテーブルセット

		if(ftonelayer)
			ToneTable_setData(g_work.img.bits);

		//テクスチャ画像読み込み

		load_texture_image();
	}
}

/* ファイル読み込み */

static mlkerr _load_file(void)
{
	FILE *fp;
	int fmt;
	mlkerr ret;

	//開く

	fp = mFILEopen(g_work.str_inputfile.buf, "rb");
	if(!fp) return MLKERR_OPEN;

	//ヘッダからフォーマット判定

	fmt = _check_header(fp);

	if(fmt < 0)
	{
		fclose(fp);
		return fmt;
	}

	g_work.img.format = fmt;

	//APD 出力時、APDv4 はスキップ

	if(PROC_IS_APD && fmt == FORMAT_APD_v4)
	{
		printf("  APD ver 4 <skip>\n");
		fclose(fp);
		return _ERR_SKIP_APDv4;
	}

	//読み込み

	ret = (g_func_load[fmt])(fp);

	fclose(fp);

	//成功時

	if(ret == MLKERR_OK)
		_after_load_file();

	return ret;
}

/* エラー処理 */

static void _proc_error(int no)
{
	const char *mes;

	switch(no)
	{
		case _ERR_FORMAT:
			mes = "format error";
			break;
		case _ERR_VERSION:
			mes = "version error";
			break;
		case MLKERR_OPEN:
			mes = "open error";
			break;
		case MLKERR_ALLOC:
			mes = "memory not enough";
			break;
		case MLKERR_DAMAGED:
			mes = "file is damaged";
			break;
		default:
			mes = "error";
			break;
	}

	printf("  <!> %s\n", mes);

	exit_app(1);
}

/* イメージの情報表示 */

static void _put_info(void)
{
	ImageData *img = &g_work.img;

	//format

	if(img->format >= FORMAT_ADW_v1)
		printf("  ADW ver %d | ", img->format - FORMAT_ADW_v1 + 1);
	else
		printf("  APD ver %d | ", img->format - FORMAT_APD_v1 + 1);

	//info

	printf("(%d x %d) %d bit",
		img->width, img->height, img->srcbits);

	if(img->dpi)
		printf(" %d DPI", img->dpi);

	putchar('\n');
}

/* 各ファイル処理 */

static void _run_file(void)
{
	mlkerr ret;

	mMemset0(&g_work.img, sizeof(ImageData));

	g_work.img.bkgndcol = 0xffffff;

	//読み込み

	ret = _load_file();

	if(ret == _ERR_SKIP_APDv4)
		return;
	else if(ret)
		_proc_error(ret);

	//基本情報表示

	if(g_work.proctype != PROCTYPE_TEXINFO)
		_put_info();

	//各処理

	switch(g_work.proctype)
	{
		//合成画像
		case PROCTYPE_BLEND:
			ret = output_blendimage();
			break;
		//APDv4 出力
		case PROCTYPE_APD:
			ret = output_apdfile();
			break;
		//レイヤ出力
		case PROCTYPE_LAYER:
			ret = output_layerimage();
			break;
		//レイヤ情報
		case PROCTYPE_INFO:
			put_layerinfo();
			ret = MLKERR_OK;
			break;
		//テクスチャ情報
		case PROCTYPE_TEXINFO:
			put_textureinfo();
			ret = MLKERR_OK;
			break;
	}

	if(ret) _proc_error(ret);

	//

	mTreeDeleteAll(&g_work.list_layer);
	mListDeleteAll(&g_work.list_teximg);
}

/* レイヤアイテム破棄ハンドラ */

static void _layeritem_destroy(mTree *tree,mTreeItem *item)
{
	LayerItem *pi = (LayerItem *)item;

	mFree(pi->name);
	mFree(pi->texpath);

	TileImage_free(pi->img);
}

/** アプリ終了
 *
 * ret: 戻り値 */

void exit_app(int ret)
{
	WorkData *p = &g_work;

	TileImage_finish();

	ToneTable_free();

	mTreeDeleteAll(&p->list_layer);
	mListDeleteAll(&p->list_teximg);

	mStrFree(&p->str_inputfile);
	mStrFree(&p->str_output);
	mStrFree(&p->str_layer_prefix);
	mStrFree(&p->str_texpath_sys);
	mStrFree(&p->str_texpath_user);

	mFree(p->layerno_flags);

	exit(ret);
}

/* 初期化 */

static void _init_app(void)
{
	mMemset0(&g_work, sizeof(WorkData));

	TileImage_init();

	ToneTable_init();

	TextureList_init(&g_work.list_teximg);

	g_work.list_layer.item_destroy = _layeritem_destroy;

	mStrSetText(&g_work.str_texpath_sys, "/usr/local/share/azpainter3/texture");
	mStrPathSetHome_join(&g_work.str_texpath_user, ".config/azpainter/texture");
}

/** main */

int main(int argc,char **argv)
{
	int i;

	mInitLocale();

	//初期化

	_init_app();

	//オプション処理

	i = get_app_options(argc, argv);

	//各ファイル処理

	for(; i < argc; i++)
	{
		if(g_work.proctype != PROCTYPE_TEXINFO)
			printf("# %s:\n", argv[i]);

		mStrSetText_locale(&g_work.str_inputfile, argv[i], -1);
	
		_run_file();

		//1行空ける

		if(i != argc - 1 && g_work.proctype != PROCTYPE_TEXINFO)
			putchar('\n');
	}

	//終了

	exit_app(0);

	return 0;
}

