/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * APD v4 読み込み
 ************************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_zlib.h"
#include "mlk_stdio.h"
#include "mlk_util.h"

#include "def.h"
#include "tileimage.h"
#include "pv_apd_format.h"


//----------------

typedef struct
{
	FILE *fp;
	mZlib *zlib;
	uint8_t *tilebuf;

	off_t fpos;
}_data;

//----------------


/* 先頭情報の取得 */

static mlkerr _read_headinfo(_data *p)
{
	uint32_t width,height,dpi;
	uint16_t size,layernum;
	uint8_t bits,coltype,r,g,b;
	uint8_t buf[21];

	//情報

	if(mFILEreadOK(p->fp, buf, 21))
		return MLKERR_DAMAGED;

	mGetBuf_format(buf, ">hiiibbbbbh",
		&size, &width, &height, &dpi,
		&bits, &coltype, &r, &g, &b, &layernum);

	if(fseek(p->fp, size - 19, SEEK_CUR))
		return MLKERR_DAMAGED;

	//

	if((bits != 8 && bits != 16)
		|| coltype != 0)
		return MLKERR_UNSUPPORTED;

	//セット

	set_image_info(width, height, bits, dpi, layernum);

	g_work.img.bkgndcol = MLK_RGB(r,g,b);

	return MLKERR_OK;
}

/* 次のチャンクを読み込み
 *
 * return: -1 で終了 */

static mlkerr _next_chunk(_data *p,uint32_t *pid,uint32_t *psize)
{
	uint32_t id,size;

	//次の位置へ

	if(p->fpos)
	{
		if(fseeko(p->fp, p->fpos, SEEK_SET))
			return MLKERR_DAMAGED;
	}

	//ID とサイズ

	if(mFILEreadBE32(p->fp, &id))
		return MLKERR_DAMAGED;

	if(id == 0) return -1;

	if(mFILEreadBE32(p->fp, &size))
		return MLKERR_DAMAGED;

	*pid = id;
	*psize = size;

	//次の位置

	p->fpos = ftello(p->fp) + size;

	return MLKERR_OK;
}

/* すべてのチャンクをスキップしてレイヤ情報へ */

static mlkerr _skip_chunk(_data *p)
{
	mlkerr ret;
	uint32_t id,size;

	while(1)
	{
		ret = _next_chunk(p, &id, &size);

		if(ret == -1)
			break;
		else if(ret)
			return ret;
	}

	return MLKERR_OK;
}

//------- レイヤ: 拡張データ


/* 拡張データ: 文字列読み込み */

static mlkerr _load_layerex_str(_data *p,char **ppdst,uint32_t size)
{
	char *buf;

	buf = (char *)mMalloc(size + 1);
	if(!buf) return MLKERR_ALLOC;

	if(mFILEreadOK(p->fp, buf, size))
	{
		mFree(buf);
		return MLKERR_DAMAGED;
	}

	buf[size] = 0;

	*ppdst = buf;

	return MLKERR_OK;
}

/* 拡張データ読み込み */

static mlkerr _load_layer_ex(_data *p,LayerItem *pi)
{
	uint32_t id,size;
	mlkerr ret;

	while(1)
	{
		if(mFILEreadBE32(p->fp, &id))
			return MLKERR_DAMAGED;

		if(id == 0) break;
		
		if(mFILEreadBE32(p->fp, &size))
			return MLKERR_DAMAGED;

		//

		switch(id)
		{
			//レイヤ名
			case _MAKE_ID('n','a','m','e'):
				ret = _load_layerex_str(p, &pi->name, size);
				break;
			//テクスチャパス
			case _MAKE_ID('t','e','x','p'):
				ret = _load_layerex_str(p, &pi->texpath, size);
				break;

			//スキップ
			default:
				if(fseeko(p->fp, size, SEEK_CUR))
					ret = MLKERR_DAMAGED;
				else
					ret = MLKERR_OK;
				break;
		}

		if(ret) return ret;
	}

	return MLKERR_OK;
}


//----------- レイヤ


/* タイルイメージスキップ */

static mlkerr _skip_tileimage(_data *p)
{
	FILE *fp = p->fp;
	uint32_t size,tilenum;
	uint16_t tnum;
	uint8_t comptype;

	//圧縮タイプ, タイル総数

	if(mFILEreadByte(fp, &comptype)
		|| mFILEreadBE32(fp, &tilenum))
		return MLKERR_DAMAGED;

	if(comptype != 0)
		return MLKERR_INVALID_VALUE;

	//空イメージ

	if(tilenum == 0)
		return MLKERR_OK;

	//タイル

	while(tilenum)
	{
		if(mFILEreadBE16(fp, &tnum)
			|| mFILEreadBE32(fp, &size)
			|| fseek(fp, size, SEEK_CUR))
			return MLKERR_DAMAGED;

		tilenum -= tnum;
	}

	return MLKERR_OK;
}

/* タイルイメージ読み込み */

static mlkerr _read_layer_image(_data *p,TileImage *img)
{
	FILE *fp = p->fp;
	mZlib *zlib = p->zlib;
	uint8_t *buf = p->tilebuf;
	uint32_t size,tilenum;
	uint16_t tnum,tx,ty;
	uint8_t comptype;
	int i,tilesize;
	mlkerr ret;

	//圧縮タイプ, タイル総数

	if(mFILEreadByte(fp, &comptype)
		|| mFILEreadBE32(fp, &tilenum))
		return MLKERR_DAMAGED;

	if(comptype != 0)
		return MLKERR_INVALID_VALUE;

	//空イメージ

	if(tilenum == 0)
		return MLKERR_OK;

	//--- タイル

	tilesize = img->tilesize;

	while(tilenum)
	{
		//タイル数, 圧縮サイズ
		
		if(mFILEreadBE16(fp, &tnum)
			|| mFILEreadBE32(fp, &size))
			return MLKERR_DAMAGED;

		//ブロック読み込み

		mZlibDecReset(zlib);
		mZlibDecSetSize(zlib, size);

		for(i = tnum; i; i--)
		{
			ret = mZlibDecRead(zlib, buf, tilesize + 4);
			if(ret) return ret;

			tx = mGetBufBE16(buf);
			ty = mGetBufBE16(buf + 2);

			ret = TileImage_allocTile_fromSave(img, tx, ty, buf + 4, FALSE);
			if(ret) return ret;
		}

		ret = mZlibDecFinish(zlib);
		if(ret) return ret;

		//

		tilenum -= tnum;
	}

	return MLKERR_OK;
}

/* 一つのレイヤを読み込み
 *
 * return: -1 で終了 */

static mlkerr _read_layer(_data *p)
{
	FILE *fp = p->fp;
	LayerItem *pi;
	TileImage *img;
	uint32_t blendmode,col,flags;
	uint16_t parent_no,tone_lines,tone_angle;
	uint8_t lflags,coltype,opacity,amask,tone_density,fimg;
	int i;
	mlkerr ret;
	mRect rc;

	//親のレイヤ番号

	if(mFILEreadBE16(fp, &parent_no))
		return MLKERR_DAMAGED;

	if(parent_no == 0xffff) return -1;

	//情報

	if(mFILEreadOK(fp, p->tilebuf, 37))
		return MLKERR_DAMAGED;

	mGetBuf_format(p->tilebuf,
		">bbiiiibbiiihhb",
		&lflags, &coltype,
		&rc.x1, &rc.y1, &rc.x2, &rc.y2,
		&opacity, &amask, &blendmode, &col, &flags,
		&tone_lines, &tone_angle, &tone_density);

	fimg = !(lflags & 1);

	//レイヤ作成

	pi = layer_add_on_parent((parent_no == 0xfffe)? -1: parent_no);
	if(!pi) return MLKERR_ALLOC;

	//情報セット

	pi->is_folder = !fimg;

	pi->type = coltype;
	pi->opacity = opacity;
	pi->alphamask = amask;
	pi->col = col;
	pi->flags = flags;
	pi->tone_lines = tone_lines;
	pi->tone_angle = tone_angle;
	pi->tone_density = tone_density;

	for(i = 0; g_blendmode_id[i]; i++)
	{
		if(blendmode == g_blendmode_id[i])
		{
			pi->blendmode = i;
			break;
		}
	}

	//拡張データ

	ret = _load_layer_ex(p, pi);
	if(ret) return ret;

	//タイルイメージ

	if(fimg)
	{
		if(PROC_IS_SKIPIMAGE)
		{
			ret = _skip_tileimage(p);
			if(ret) return ret;
		}
		else
		{
			pi->img = img = TileImage_newFromRect_forFile(coltype, &rc);
			if(!img) return MLKERR_ALLOC;

			TileImage_setColor(img, pi->col);

			ret = _read_layer_image(p, img);
			if(ret) return ret;
		}
	}

	return MLKERR_OK;
}

/* メイン処理 */

static mlkerr _main_proc(_data *p)
{
	mlkerr ret;

	ret = _read_headinfo(p);
	if(ret) return ret;

	ret = _skip_chunk(p);
	if(ret) return ret;

	//レイヤ読み込み

	while(1)
	{
		ret = _read_layer(p);

		if(ret == -1)
			break;
		else if(ret)
			return ret;
	}

	return MLKERR_OK;
}

/** APD v4 読み込み */

mlkerr load_apd_v4(FILE *fp)
{
	_data dat;
	mlkerr ret;

	mMemset0(&dat, sizeof(_data));

	dat.fp = fp;

	ret = init_zlib(&dat.zlib, fp, TRUE);
	if(ret) return ret;

	//[!] イメージの読み込み以外でも使う

	dat.tilebuf = (uint8_t *)mMalloc(64 * 64 * 2 * 4 + 4);
	if(!dat.tilebuf)
	{
		ret = MLKERR_ALLOC;
		goto ERR;
	}

	//

	ret = _main_proc(&dat);

	//

ERR:
	mZlibFree(dat.zlib);
	mFree(dat.tilebuf);

	return ret;
}

