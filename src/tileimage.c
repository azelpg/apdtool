/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * TileImage
 **********************************/

#include <string.h>
#include <math.h>

#include "mlk.h"
#include "mlk_rectbox.h"
#include "mlk_imagebuf.h"
#include "mlk_util.h"

#include "tileimage.h"
#include "pv_tileimage.h"


//-------------------

TileImageWorkData *g_tileimgwork = NULL;

//タイルサイズ
static const int g_tilesize_8bit[4] = {64*64*4, 64*64*2, 64*64, 64*64/8},
	g_tilesize_16bit[4] = {64*64*8, 64*64*4, 64*64*2, 64*64/8};

//-------------------

void __TileImage_setColFunc_RGBA(TileImageColFuncData *p,int bits);
void __TileImage_setColFunc_Gray(TileImageColFuncData *p,int bits);
void __TileImage_setColFunc_alpha(TileImageColFuncData *p,int bits);
void __TileImage_setColFunc_alpha1bit(TileImageColFuncData *p,int bits);

void BlendColorFunc_setTable_8bit(BlendColorFunc *p);
void BlendColorFunc_setTable_16bit(BlendColorFunc *p);

/* image.c */
void color_8to16(uint16_t *dst,uint32_t col);

//-------------------


/* TileImage 作成 */

static TileImage *_create_tileimg(int type,int tilew,int tileh)
{
	TileImage *p;

	p = (TileImage *)mMalloc0(sizeof(TileImage));
	if(!p) return NULL;

	p->type = type;
	p->tilew = tilew;
	p->tileh = tileh;

	if(g_tileimgwork->bits == 8)
		p->tilesize = g_tilesize_8bit[type];
	else
		p->tilesize = g_tilesize_16bit[type];

	//タイルバッファ確保

	p->ppbuf = (uint8_t **)mMalloc0(p->tilew * p->tileh * sizeof(void *));
	if(!p->ppbuf)
	{
		mFree(p);
		return NULL;
	}

	return p;
}


//===========================
// main
//===========================


/** 初期化 */

void TileImage_init(void)
{
	g_tileimgwork = (TileImageWorkData *)mMalloc0(sizeof(TileImageWorkData));

}

/** 終了 */

void TileImage_finish(void)
{
	mFree(g_tileimgwork);
}

/** ビット数を変更 */

void TileImage_global_setBits(int bits)
{
	if(bits == g_tileimgwork->bits) return;
	
	g_tileimgwork->bits = bits;

	//タイル用関数

	__TileImage_setColFunc_RGBA(g_tileimgwork->colfunc + TILEIMAGE_COLTYPE_RGBA, bits);
	__TileImage_setColFunc_Gray(g_tileimgwork->colfunc + TILEIMAGE_COLTYPE_GRAY, bits);
	__TileImage_setColFunc_alpha(g_tileimgwork->colfunc + TILEIMAGE_COLTYPE_ALPHA, bits);
	__TileImage_setColFunc_alpha1bit(g_tileimgwork->colfunc + TILEIMAGE_COLTYPE_ALPHA1BIT, bits);

	//合成関数

	if(bits == 8)
		BlendColorFunc_setTable_8bit(g_tileimgwork->blendfunc);
	else
		BlendColorFunc_setTable_16bit(g_tileimgwork->blendfunc);
}

/** DPI をセット */

void TileImage_global_setDPI(int dpi)
{
	g_tileimgwork->dpi = dpi;
}

/** トーンをグレイスケール化するか */

void TileImage_global_setToneToGray(int enable)
{
	g_tileimgwork->is_tone_to_gray = enable;
}

/** 指定タイプとビット数でのタイルサイズを取得 */

int TileImage_global_getTileSize(int type,int bits)
{
	int size;

	if(type == TILEIMAGE_COLTYPE_ALPHA1BIT)
		size = 64 * 64 / 8;
	else
	{
		switch(type)
		{
			case TILEIMAGE_COLTYPE_RGBA:
				size = 64 * 64 * 4;
				break;
			case TILEIMAGE_COLTYPE_GRAY:
				size = 64 * 64 * 2;
				break;
			default:
				size = 64 * 64;
				break;
		}

		if(bits == 16) size *= 2;
	}

	return size;
}

/** タイルがすべて透明か (A/A1用、共通) */

mlkbool TileImage_tile_isTransparent_forA(uint8_t *tile,int size)
{
	uint64_t *ps = (uint64_t *)tile;
	int i;

	//8byte 単位でゼロ比較
	
	for(i = size / 8; i; i--, ps++)
	{
		if(*ps) return FALSE;
	}

	return TRUE;
}


//======================


/** すべて解放 */

void TileImage_free(TileImage *p)
{
	uint8_t **pp;
	uint32_t i;

	if(p)
	{
		if(p->ppbuf)
		{
			pp = p->ppbuf;
			
			for(i = p->tilew * p->tileh; i; i--, pp++)
			{
				if(*pp)
					mFree(*pp);
			}

			mFree(p->ppbuf);
		}

		mFree(p);
	}
}

/** 作成 (px サイズ指定) */

TileImage *TileImage_new(int type,int w,int h)
{
	return _create_tileimg(type, (w + 63) / 64, (h + 63) / 64);
}

/** 作成 (ピクセル範囲から。ファイル読み込み用)
 *
 * rc: x2,y2 は +1。x1 >= x2 or y1 >= y2 で空状態。 */

TileImage *TileImage_newFromRect_forFile(int type,const mRect *rc)
{
	TileImage *p;
	int offx,offy,tw,th;

	if(rc->x1 >= rc->x2 && rc->y1 >= rc->y2)
	{
		//空
		
		offx = offy = 0;
		tw = th = 1;
	}
	else
	{
		tw = (rc->x2 - rc->x1) >> 6;
		th = (rc->y2 - rc->y1) >> 6;

		if(tw <= 0) tw = 1;
		if(th <= 0) th = 1;

		offx = rc->x1;
		offy = rc->y1;
	}

	//作成

	p = _create_tileimg(type, tw, th);
	if(p)
	{
		p->offx = offx;
		p->offy = offy;
	}

	return p;
}

/** 色をセット */

void TileImage_setColor(TileImage *p,uint32_t col)
{
	p->col.c8.r = MLK_RGB_R(col);
	p->col.c8.g = MLK_RGB_G(col);
	p->col.c8.b = MLK_RGB_B(col);

	color_8to16(&p->col.c16.r, col);
}

/** 一つのタイルを確保 */

uint8_t *TileImage_allocTile(TileImage *p)
{
	return (uint8_t *)mMallocAlign(p->tilesize, 16);
}

/** ポインタの位置にタイルがなければ確保
 *
 * return: FALSE で、タイルの新規確保に失敗 */

mlkbool TileImage_allocTile_atptr(TileImage *p,uint8_t **ppbuf)
{
	if(!(*ppbuf))
	{
		*ppbuf = TileImage_allocTile(p);
		if(!(*ppbuf)) return FALSE;
	}

	return TRUE;
}

/** 指定タイル位置のタイルを取得 (タイルが確保されていなければ確保)
 *
 * return: タイルポインタ。位置が範囲外や、確保失敗の場合は NULL */

uint8_t *TileImage_getTileAlloc_atpos(TileImage *p,int tx,int ty)
{
	uint8_t **pp;

	if(tx < 0 || ty < 0 || tx >= p->tilew || ty >= p->tileh)
		return NULL;
	else
	{
		pp = TILEIMAGE_GETTILE_BUFPT(p, tx, ty);

		if(!(*pp))
		{
			*pp = TileImage_allocTile(p);
		}

		return *pp;
	}
}

/** タイル位置から左上の px 位置取得 */

void TileImage_tile_to_pixel(TileImage *p,int tx,int ty,int *px,int *py)
{
	*px = (tx << 6) + p->offx;
	*py = (ty << 6) + p->offy;
}

/** px 位置からタイル位置を取得 (範囲外判定を行わない) */

void TileImage_pixel_to_tile_nojudge(TileImage *p,int x,int y,int *tilex,int *tiley)
{
	*tilex = (x - p->offx) >> 6;
	*tiley = (y - p->offy) >> 6;
}

/** タイルが存在する部分の px 範囲取得
 *
 * 透明な部分は除く。イメージの範囲外部分も含む。
 *
 * rcdst: すべて透明の場合は、(0,0)-(-1,-1)となる。
 * tilenum: NULL 以外で、確保されているタイル数が入る。
 * return: FALSE で範囲なし */

mlkbool TileImage_getHaveImageRect_pixel(TileImage *p,mRect *rcdst,uint32_t *tilenum)
{
	uint8_t **pp;
	mRect rc;
	int x,y;
	uint32_t num = 0;

	mRectEmpty(&rc);

	//確保されているタイルの最小/最大位置

	if(p)
	{
		pp = p->ppbuf;

		for(y = 0; y < p->tileh; y++)
		{
			for(x = 0; x < p->tilew; x++, pp++)
			{
				if(*pp)
				{
					mRectIncPoint(&rc, x, y);
					num++;
				}
			}
		}
	}

	if(tilenum) *tilenum = num;

	//タイル位置 -> px

	if(mRectIsEmpty(&rc))
	{
		*rcdst = rc;
		return FALSE;
	}
	else
	{
		TileImage_tile_to_pixel(p, 0, 0, &x, &y);

		rcdst->x1 = x + (rc.x1 << 6);
		rcdst->y1 = y + (rc.y1 << 6);
		rcdst->x2 = x + (rc.x2 << 6) + 63;
		rcdst->y2 = y + (rc.y2 << 6) + 63;
		
		return TRUE;
	}
}

/** (APDv2/v4 タイル読み込み時) 指定位置のタイルを確保してセット */

mlkerr TileImage_allocTile_fromSave(TileImage *p,int tx,int ty,uint8_t *srcbuf,int to8bit)
{
	uint8_t *buf;

	buf = TileImage_getTileAlloc_atpos(p, tx, ty);
	if(!buf) return MLKERR_ALLOC;

	if(to8bit)
		//8bit 変換
		(g_tileimgwork->colfunc[p->type].convert_16to8)(buf, srcbuf);
	else
		(g_tileimgwork->colfunc[p->type].convert_from_save)(buf, srcbuf);

	return MLKERR_OK;
}

/** (APDv3 タイル読み込み時) 指定位置のタイルを確保してセット
 *
 * to8bit: 16bit->8bit 変換 */

mlkerr TileImage_allocTile_fromSave_APDv3(TileImage *p,int tx,int ty,uint8_t *srcbuf,int to8bit)
{
	uint8_t *buf;

	buf = TileImage_getTileAlloc_atpos(p, tx, ty);
	if(!buf) return MLKERR_ALLOC;

	if(to8bit)
	{
		//8bit 変換
		
		(g_tileimgwork->colfunc[p->type].convert_16to8)(buf, srcbuf);
	}
	else
	{
		if(g_tileimgwork->bits == 8 && p->type == TILEIMAGE_COLTYPE_RGBA)
		{
			//8bit RGBA はそのままコピー (AzPainterB)

			memcpy(buf, srcbuf, 64 * 64 * 4);
		}
		else
			//ほか
			(g_tileimgwork->colfunc[p->type].convert_from_save)(buf, srcbuf);
	}

	return MLKERR_OK;
}

/** (APDv4) イメージ保存時のタイル処理
 *
 * rc: イメージのあるpx範囲
 * buf: タイルサイズ + 4byte */

mlkerr TileImage_saveTiles_apd4(TileImage *p,mRect *rc,uint8_t *buf,
	mlkerr (*func)(TileImage *p,void *param),void *param)
{
	uint8_t **pptile;
	int ix,iy,tx,ty;
	mlkerr ret;
	TileImageColFunc_convert_forSave func_conv;

	func_conv = g_tileimgwork->colfunc[p->type].convert_to_save;

	pptile = p->ppbuf;
	
	ty = (p->offy - rc->y1) >> 6;

	for(iy = p->tileh; iy; iy--, ty++)
	{
		tx = (p->offx - rc->x1) >> 6;
		
		for(ix = p->tilew; ix; ix--, tx++, pptile++)
		{
			if(!(*pptile)) continue;

			//タイル位置

			mSetBufBE16(buf, tx);
			mSetBufBE16(buf + 2, ty);

			//タイルデータ

			(func_conv)(buf + 4, *pptile);

			//呼び出し

			ret = (func)(p, param);
			if(ret) return ret;
		}
	}

	return MLKERR_OK;
}


//=============================
// 合成
//=============================


/* box の px 範囲に相当するタイル範囲 (mRect) を取得
 *
 * rc:  タイル範囲が入る
 * box: イメージの範囲 (px)
 * return: FALSE で、全体がタイル範囲外 */

static mlkbool _get_tilerect_pixelbox(TileImage *p,mRect *rcdst,const mBox *box)
{
	int tx1,ty1,tx2,ty2,f;

	//box の四隅のタイル位置

	TileImage_pixel_to_tile_nojudge(p, box->x, box->y, &tx1, &ty1);
	TileImage_pixel_to_tile_nojudge(p, box->x + box->w - 1, box->y + box->h - 1, &tx2, &ty2);

	//各位置がタイル範囲外かどうかのフラグ

	f = (tx1 < 0) | ((tx2 < 0) << 1)
		| ((tx1 >= p->tilew) << 2) | ((tx2 >= p->tilew) << 3)
		| ((ty1 < 0) << 4) | ((ty2 < 0) << 5)
		| ((ty1 >= p->tileh) << 6) | ((ty2 >= p->tileh) << 7);

	//全体がタイル範囲外か

	if((f & 0x03) == 0x03 || (f & 0x30) == 0x30
		|| (f & 0x0c) == 0x0c || (f & 0xc0) == 0xc0)
		return FALSE;

	//タイル位置調整

	if(f & 1) tx1 = 0;
	if(f & 8) tx2 = p->tilew - 1;
	if(f & 0x10) ty1 = 0;
	if(f & 0x80) ty2 = p->tileh - 1;

	//セット

	rcdst->x1 = tx1, rcdst->y1 = ty1;
	rcdst->x2 = tx2, rcdst->y2 = ty2;

	return TRUE;
}

/* box の px 範囲内のタイルを処理する際の情報を取得
 *
 * return: タイルの開始位置のポインタ (NULL で範囲外) */

static uint8_t **_get_tilerectinfo(TileImage *p,TileImageTileRectInfo *info,const mBox *box)
{
	mRect rc;

	//タイル範囲

	if(!_get_tilerect_pixelbox(p, &rc, box))
		return NULL;

	info->rctile = rc;

	//左上タイルの px 位置

	TileImage_tile_to_pixel(p, rc.x1, rc.y1, &info->pxtop.x, &info->pxtop.y);

	//box -> mRect (クリッピング用)

	info->rcclip.x1 = box->x;
	info->rcclip.y1 = box->y;
	info->rcclip.x2 = box->x + box->w;
	info->rcclip.y2 = box->y + box->h;

	//

	info->tilew = rc.x2 - rc.x1 + 1;
	info->tileh = rc.y2 - rc.y1 + 1;
	info->pitch_tile = p->tilew - info->tilew;

	//タイル位置

	return TILEIMAGE_GETTILE_BUFPT(p, rc.x1, rc.y1);
}

/* 濃度 (0〜100) の値を、色値に変換 */

static int _density_to_colval(int v,mlkbool rev)
{
	int n;
	
	if(g_tileimgwork->bits == 8)
	{
		n = (int)(v / 100.0 * 255 + 0.5);
		if(rev) n = 255 - n;
	}
	else
	{
		n = (int)(v / 100.0 * 0x8000 + 0.5);
		if(rev) n = 0x8000 - n;
	}

	return n;
}

/* トーン化の情報セット */

static void _blend_set_tone_info(TileImageBlendInfo *dst,const TileImageBlendSrcInfo *sinfo)
{
	double dcell,dcos,dsin,rd;

	//固定濃度 (0 でなし)

	if(sinfo->tone_density)
		dst->tone_density = _density_to_colval(sinfo->tone_density, FALSE);

	//セル幅 (px)

	dcell = g_tileimgwork->dpi / (sinfo->tone_lines * 0.1);

	//sin/cos
	
	rd = sinfo->tone_angle / 180.0 * MLK_MATH_PI;

	dcos = cos(rd);
	dsin = sin(rd);

	//1px進むごとに加算する値
	// :本来は dcos / dcell だが、モアレ防止の為、セル1周期がピクセル単位になるように調整。

	dst->tone_fcos = (int64_t)round(1.0 / round(dcell / dcos) * TILEIMG_TONE_FIX_VAL);
	dst->tone_fsin = (int64_t)round(1.0 / round(dcell / dsin) * TILEIMG_TONE_FIX_VAL);
}

/* タイル毎の合成時用の情報セット
 *
 * px,py: タイル左上の px 位置
 * rcclip: px クリッピング範囲 */

static void _set_blendinfo(TileImageBlendInfo *info,int px,int py,const mRect *rcclip)
{
	int dx,dy;

	dx = px, dy = py;

	info->sx = info->sy = 0;
	info->w = info->h = 64;

	//X 左端調整

	if(dx < rcclip->x1)
	{
		info->w  -= rcclip->x1 - dx;
		info->sx += rcclip->x1 - dx;
		dx = rcclip->x1;
	}

	//Y 上端調整

	if(dy < rcclip->y1)
	{
		info->h  -= rcclip->y1 - dy;
		info->sy += rcclip->y1 - dy;
		dy = rcclip->y1;
	}

	//右下調整

	if(dx + info->w > rcclip->x2) info->w = rcclip->x2 - dx;
	if(dy + info->h > rcclip->y2) info->h = rcclip->y2 - dy;

	//

	info->dx = dx, info->dy = dy;
}

/** レイヤ合成
 *
 * - トーン化レイヤ対象外のカラータイプでは、トーン化は常に OFF に指定されていること。
 *
 * boxdst: キャンバスの描画範囲 */

void TileImage_blend(TileImage *p,mImageBuf2 *dst,
	const mBox *boxdst,const TileImageBlendSrcInfo *sinfo)
{
	TileImageTileRectInfo info;
	TileImageBlendInfo binfo;
	uint8_t **pptile;
	int ix,iy,px,py;
	int64_t fxx,fxy,fyx,fyy,fsin64,fcos64;
	TileImageColFunc_blendTile func;

	if(sinfo->opacity == 0) return;

	if(!(pptile = _get_tilerectinfo(p, &info, boxdst)))
		return;

	func = g_tileimgwork->colfunc[p->type].blend_tile;

	//[!] トーン化の値はゼロクリア

	memset(&binfo, 0, sizeof(TileImageBlendInfo));

	binfo.opacity = sinfo->opacity;
	binfo.texitem = sinfo->texitem;
	binfo.func_blend = g_tileimgwork->blendfunc[sinfo->blendmode];
	binfo.tone_repcol = -1;

	//トーン化を行うか

	if(sinfo->tone_lines)
	{
		if(!g_tileimgwork->is_tone_to_gray)
		{
			//通常のトーン表示

			binfo.is_tone = TRUE;
			binfo.is_tone_bkgnd_tp = !(sinfo->ftone_white);
		}
		else
		{
			//トーンをグレイスケール表示
			
			if(sinfo->tone_density)
			{
				//固定濃度ありの場合、濃度から色を取得

				binfo.tone_repcol = _density_to_colval(sinfo->tone_density, TRUE);
			}
			else if(p->type == TILEIMAGE_COLTYPE_ALPHA1BIT)
			{
				//固定濃度なしの場合、GRAYタイプなら通常のグレイスケール表示。
				//A1タイプの場合、黒で固定。

				binfo.tone_repcol = 0;
			}
		}
	}

	//タイルごとに合成
	// :px,py = タイル左上の px 位置

	if(binfo.is_tone)
	{
		//----- トーン化

		//情報セット

		_blend_set_tone_info(&binfo, sinfo);

		//イメージ (0,0) 時点での初期位置
		// :位置によって微妙に形が変わるので、適当な値でずらす。

		fyx = (int64_t)(0.5 * TILEIMG_TONE_FIX_VAL);
		fyy = (int64_t)(0.7 * TILEIMG_TONE_FIX_VAL);

		//左上の位置 (先頭タイルの(0,0)位置)
		// :どのタイルから描画しても同じ位置になるようにする。

		fyx += info.pxtop.x * binfo.tone_fcos - info.pxtop.y * binfo.tone_fsin;
		fyy += info.pxtop.x * binfo.tone_fsin + info.pxtop.y * binfo.tone_fcos;

		fcos64 = binfo.tone_fcos << 6;
		fsin64 = binfo.tone_fsin << 6;

		//

		py = info.pxtop.y;

		for(iy = info.tileh; iy; iy--, py += 64)
		{
			fxx = fyx;
			fxy = fyy;
		
			for(ix = info.tilew, px = info.pxtop.x; ix; ix--, px += 64, pptile++)
			{
				if(*pptile)
				{
					_set_blendinfo(&binfo, px, py, &info.rcclip);

					binfo.tile = *pptile;
					binfo.dstbuf = dst->ppbuf + binfo.dy;
					binfo.tone_fx = fxx;
					binfo.tone_fy = fxy;

					//タイル内の開始位置を加算

					if(binfo.sx || binfo.sy)
					{
						binfo.tone_fx += binfo.sx * binfo.tone_fcos - binfo.sy * binfo.tone_fsin;
						binfo.tone_fy += binfo.sx * binfo.tone_fsin + binfo.sy * binfo.tone_fcos;
					}

					(func)(p, &binfo);
				}

				fxx += fcos64;
				fxy += fsin64;
			}

			pptile += info.pitch_tile;

			fyx -= fsin64;
			fyy += fcos64;
		}
	}
	else
	{
		//----- 通常
		
		py = info.pxtop.y;

		for(iy = info.tileh; iy; iy--, py += 64)
		{
			for(ix = info.tilew, px = info.pxtop.x; ix; ix--, px += 64, pptile++)
			{
				if(!(*pptile)) continue;

				_set_blendinfo(&binfo, px, py, &info.rcclip);

				binfo.tile = *pptile;
				binfo.dstbuf = dst->ppbuf + binfo.dy;

				(func)(p, &binfo);
			}

			pptile += info.pitch_tile;
		}
	}
}

/** 指定範囲のイメージを RGBA 8bit に */

void TileImage_to_RGBA8bit(TileImage *p,mImageBuf2 *dst,const mBox *boxdst)
{
	TileImageTileRectInfo info;
	TileImageBlendInfo binfo;
	uint8_t **pptile;
	int ix,iy,px,py;
	TileImageColFunc_toRGBA8 func;

	if(!(pptile = _get_tilerectinfo(p, &info, boxdst)))
		return;

	memset(&binfo, 0, sizeof(TileImageBlendInfo));

	func = g_tileimgwork->colfunc[p->type].to_rgba8;

	//タイルごとに合成

	py = info.pxtop.y;

	for(iy = info.tileh; iy; iy--, py += 64)
	{
		for(ix = info.tilew, px = info.pxtop.x; ix; ix--, px += 64, pptile++)
		{
			if(!(*pptile)) continue;

			_set_blendinfo(&binfo, px, py, &info.rcclip);

			(func)(p, dst->ppbuf + binfo.dy, binfo.dx, binfo.w, binfo.h,
				*pptile, binfo.sx, binfo.sy);
		}

		pptile += info.pitch_tile;
	}
}


//===========================================
// イメージバッファからタイルイメージに変換
//===========================================
/*
 * - TileImage は、ソースのサイズ分、配列を確保してあること。
 */


/** イメージバッファから TileImage に変換 (RGBA 8bit)
 *
 * APD v1 用 */

mlkbool TileImage_convertFromImage_RGBA8(TileImage *p,uint8_t **ppsrc,int srcw,int srch)
{
	uint8_t **ppdst,*tilebuf,**pps,*pd;
	int ix,iy,xx,yy,w,h,jy,xpos;
	TileImageColFunc_isTransparentTile func_isempty;

	//作業用タイル

	tilebuf = TileImage_allocTile(p);
	if(!tilebuf) return FALSE;

	//

	ppdst = p->ppbuf;

	func_isempty = g_tileimgwork->colfunc[p->type].is_transparent_tile;

	//

	for(iy = p->tileh, yy = 0; iy; iy--, yy += 64)
	{
		h = 64;
		if(yy + 64 > srch) h = srch - yy;
	
		for(ix = p->tilew, xx = 0; ix; ix--, xx += 64, ppdst++)
		{
			w = 64;
			if(xx + 64 > srcw) w = srcw - xx;

			//64x64 でなければクリア

			if(w != 64 || h != 64)
				memset(tilebuf, 0, p->tilesize);
			
			//tilebuf に変換

			pps = ppsrc;
			pd = tilebuf;
			xpos = xx * 4;
			w *= 4;

			for(jy = h; jy; jy--)
			{
				memcpy(pd, *pps + xpos, w);
			
				pps++;
				pd += 64 * 4;
			}

			//すべて透明でなければ、確保してコピー

			if(!(func_isempty)(tilebuf))
			{
				if(!TileImage_allocTile_atptr(p, ppdst))
				{
					mFree(tilebuf);
					return FALSE;
				}

				memcpy(*ppdst, tilebuf, p->tilesize);
			}
		}

		ppsrc += 64;
	}

	mFree(tilebuf);

	return TRUE;
}

/** イメージバッファから TileImage に変換 (アルファ値のみ 8bit)
 *
 * ADW v1 用 */

mlkbool TileImage_convertFromImage_alpha8(TileImage *p,uint8_t **ppsrc,int srcw,int srch)
{
	uint8_t **ppdst,*tilebuf,**pps,*pd;
	int ix,iy,xx,yy,w,h,jy;
	TileImageColFunc_isTransparentTile func_isempty;

	//作業用タイル

	tilebuf = TileImage_allocTile(p);
	if(!tilebuf) return FALSE;

	//

	ppdst = p->ppbuf;

	func_isempty = g_tileimgwork->colfunc[p->type].is_transparent_tile;

	//

	for(iy = p->tileh, yy = 0; iy; iy--, yy += 64)
	{
		h = 64;
		if(yy + 64 > srch) h = srch - yy;
	
		for(ix = p->tilew, xx = 0; ix; ix--, xx += 64, ppdst++)
		{
			w = 64;
			if(xx + 64 > srcw) w = srcw - xx;

			//64x64 でなければクリア

			if(w != 64 || h != 64)
				memset(tilebuf, 0, p->tilesize);
			
			//tilebuf へ変換

			pps = ppsrc;
			pd = tilebuf;

			for(jy = h; jy; jy--)
			{
				memcpy(pd, *pps + xx, w);

				pps++;
				pd += 64;
			}

			//すべて透明でなければ、確保してコピー

			if(!(func_isempty)(tilebuf))
			{
				if(!TileImage_allocTile_atptr(p, ppdst))
				{
					mFree(tilebuf);
					return FALSE;
				}

				memcpy(*ppdst, tilebuf, p->tilesize);
			}
		}

		ppsrc += 64;
	}

	mFree(tilebuf);

	return TRUE;
}

