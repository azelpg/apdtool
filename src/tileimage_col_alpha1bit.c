/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * TileImage
 * A1bit タイプ
 *****************************************/

#include <string.h>

#include "mlk.h"

#include "tileimage.h"
#include "pv_tileimage.h"
#include "tonetable.h"


int TextureItem_getOpacity(TexItem *pi,int x,int y);


/*
 * - 上位ビットから順。
 * - アルファ値が 0 より大きければアルファ値を ON とする。
 */


//===========================
// 8bit/16bit 共通
//===========================


/** すべて透明か */

static mlkbool _is_transparent_tile(uint8_t *tile)
{
	return TileImage_tile_isTransparent_forA(tile, 64 * 64 / 8);
}

/** ファイル保存用タイルデータの変換 (読み込み/保存/8bit変換 共通) */

static void _convert_save(uint8_t *dst,uint8_t *src)
{
	memcpy(dst, src, 64 * 64 / 8);
}

/** (レイヤ画像用) タイルを、RGBA 8bit イメージに変換 */

static void _to_rgba8(TileImage *p,uint8_t **ppbuf,int dx,int w,int h,uint8_t *tile,int sx,int sy)
{
	uint32_t *pd;
	uint8_t *psY,*ps,f,fleft,col[4];
	int ix,iy;

	psY = tile + ((sy << 3) + (sx >> 3));
	fleft = 1 << (7 - (sx & 7));

	col[0] = p->col.c8.r;
	col[1] = p->col.c8.g;
	col[2] = p->col.c8.b;

	for(iy = h; iy; iy--)
	{
		ps = psY;
		pd = (uint32_t *)*ppbuf + dx;
		f = fleft;
	
		for(ix = w; ix; ix--)
		{
			col[3] = (*ps & f)? 255: 0;
		
			*(pd++) = *((uint32_t *)col);

			f >>= 1;
			if(!f) f = 0x80, ps++;
		}

		psY += 8;
		ppbuf++;
	}
}


//===========================
// 8bit
//===========================


/** 合成 */

static void _8bit_blend_tile(TileImage *p,TileImageBlendInfo *infosrc)
{
	TileImageBlendInfo info;
	uint8_t **ppdst,*ps,*psY,*pd,fleft,f,fval;
	int ix,iy,dx,dy,a,dstx,c,cx,cy,thval;
	int32_t src[3],dst[3],r,g,b;
	int64_t fxx,fxy;

	info = *infosrc;

	psY = info.tile + ((info.sy << 3) + (info.sx >> 3));
	ppdst = info.dstbuf;
	dstx = info.dx * 4;

	fleft = 1 << (7 - (info.sx & 7));

	r = p->col.c8.r;
	g = p->col.c8.g;
	b = p->col.c8.b;

	//

	for(iy = info.h, dy = info.dy; iy; iy--, dy++)
	{
		pd = *ppdst + dstx;
		ps = psY;
		f = fleft;
		fval = *(ps++);
		fxx = info.tone_fx;
		fxy = info.tone_fy;
	
		for(ix = info.w, dx = info.dx; ix;
			ix--, dx++, pd += 4, f >>= 1, fxx += info.tone_fcos, fxy += info.tone_fsin)
		{
			if(!f)
				f = 0x80, fval = *(ps++);

			if(!(fval & f)) continue;

			//

			if(info.texitem)
				a = TextureItem_getOpacity(info.texitem, dx, dy);
			else
				a = 255;

			a = a * info.opacity >> 7;

			if(!a) continue;

			//トーン化

			if(!info.is_tone)
			{
				if(info.tone_repcol == -1)
				{
					//通常
					src[0] = r;
					src[1] = g;
					src[2] = b;
				}
				else
				{
					//トーンのグレイスケール化
					src[0] = src[1] = src[2] = info.tone_repcol;
				}
			}
			else
			{
				//固定濃度でなければ、色は常に黒
				c = (info.tone_density)? 255 - info.tone_density: 0;

				cx = fxx >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);
				cy = fxy >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);

				if(c < 128)
				{
					cx += TABLEDATA_TONE_WIDTH / 2;
					cy += TABLEDATA_TONE_WIDTH / 2;
				}

				thval = TABLEDATA_TONE_GETVAL8(cx, cy);

				if(c < 128)
					thval = 255 - thval;
				
				if(c > thval)
				{
					//透明 or 白

					if(info.is_tone_bkgnd_tp) continue;

					src[0] = src[1] = src[2] = 255;
				}
				else
				{
					src[0] = r;
					src[1] = g;
					src[2] = b;
				}
			}

			//色合成

			dst[0] = pd[0];
			dst[1] = pd[1];
			dst[2] = pd[2];

			if((info.func_blend)(src, dst, a) && a != 255)
			{
				//アルファ合成

				src[0] = (src[0] - dst[0]) * a / 255 + dst[0];
				src[1] = (src[1] - dst[1]) * a / 255 + dst[1];
				src[2] = (src[2] - dst[2]) * a / 255 + dst[2];
			}

			//セット

			pd[0] = src[0];
			pd[1] = src[1];
			pd[2] = src[2];
		}

		psY += 8;
		ppdst++;

		info.tone_fx -= info.tone_fsin;
		info.tone_fy += info.tone_fcos;
	}
}


//===========================
// 16bit
//===========================


/** 合成 */

static void _16bit_blend_tile(TileImage *p,TileImageBlendInfo *infosrc)
{
	TileImageBlendInfo info;
	uint8_t *ps,*psY,fleft,f,fval;
	uint16_t **ppdst,*pd;
	int ix,iy,dx,dy,a,dstx,c,cx,cy,thval;
	int32_t src[3],dst[3],r,g,b;
	int64_t fxx,fxy;

	info = *infosrc;

	psY = info.tile + ((info.sy << 3) + (info.sx >> 3));
	ppdst = (uint16_t **)info.dstbuf;
	dstx = info.dx * 4;

	fleft = 1 << (7 - (info.sx & 7));

	r = p->col.c16.r;
	g = p->col.c16.g;
	b = p->col.c16.b;

	//

	for(iy = info.h, dy = info.dy; iy; iy--, dy++)
	{
		pd = *ppdst + dstx;
		ps = psY;
		f = fleft;
		fval = *(ps++);
		fxx = info.tone_fx;
		fxy = info.tone_fy;
		
		for(ix = info.w, dx = info.dx; ix;
			ix--, dx++, pd += 4, f >>= 1, fxx += info.tone_fcos, fxy += info.tone_fsin)
		{
			if(!f)
				f = 0x80, fval = *(ps++);

			if(!(fval & f)) continue;

			//

			if(info.texitem)
				a = (TextureItem_getOpacity(info.texitem, dx, dy) << 15) / 255;
			else
				a = 0x8000;

			a = a * info.opacity >> 7;

			if(!a) continue;

			//トーン化

			if(!info.is_tone)
			{
				if(info.tone_repcol == -1)
				{
					//通常
					src[0] = r;
					src[1] = g;
					src[2] = b;
				}
				else
				{
					//トーンのグレイスケール化
					src[0] = src[1] = src[2] = info.tone_repcol;
				}
			}
			else
			{
				//固定濃度でなければ、色は常に黒
				c = (info.tone_density)? 0x8000 - info.tone_density: 0;

				cx = fxx >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);
				cy = fxy >> (TILEIMG_TONE_FIX_BITS - TABLEDATA_TONE_BITS);

				if(c < 0x4000)
				{
					cx += TABLEDATA_TONE_WIDTH / 2;
					cy += TABLEDATA_TONE_WIDTH / 2;
				}

				thval = TABLEDATA_TONE_GETVAL16(cx, cy);

				if(c < 0x4000)
					thval = 0x8000 - thval;
				
				if(c > thval)
				{
					//透明 or 白

					if(info.is_tone_bkgnd_tp) continue;

					src[0] = src[1] = src[2] = 0x8000;
				}
				else
				{
					src[0] = r;
					src[1] = g;
					src[2] = b;
				}
			}

			//色合成

			dst[0] = pd[0];
			dst[1] = pd[1];
			dst[2] = pd[2];

			if((info.func_blend)(src, dst, a) && a != 0x8000)
			{
				//アルファ合成

				src[0] = ((src[0] - dst[0]) * a >> 15) + dst[0];
				src[1] = ((src[1] - dst[1]) * a >> 15) + dst[1];
				src[2] = ((src[2] - dst[2]) * a >> 15) + dst[2];
			}

			//セット

			pd[0] = src[0];
			pd[1] = src[1];
			pd[2] = src[2];
		}

		psY += 8;
		ppdst++;

		info.tone_fx -= info.tone_fsin;
		info.tone_fy += info.tone_fcos;
	}
}


//==========================
//
//==========================


/** 関数をセット */

void __TileImage_setColFunc_alpha1bit(TileImageColFuncData *p,int bits)
{
	p->is_transparent_tile = _is_transparent_tile;
	p->to_rgba8 = _to_rgba8;
	p->convert_from_save = _convert_save;
	p->convert_to_save = _convert_save;
	p->convert_16to8 = _convert_save;

	if(bits == 8)
	{
		p->blend_tile = _8bit_blend_tile;
	}
	else
	{
		p->blend_tile = _16bit_blend_tile;
	}
}

