/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * オプション処理
 *****************************/

#include <stdio.h>
#include <stdlib.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_string.h"
#include "mlk_argparse.h"
#include "mlk_file.h"

#include "def.h"
#include "tileimage.h"


//---------------

#define VERSION_TEXT "apdtool ver 1.1.2\nCopyright (c) 2020-2022 Azel"

//---------------


/* キーワードの列挙から文字列を検索
 *
 * name: 検索対象の名前
 * list: 列挙リスト (';' で区切る)
 * outname: エラー時に出力するオプション名
 * return: 見つかった場合、list 内のインデックス位置。
 *  見つからなけれな、エラーを表示して終了。 */

static int _get_keyword_index(const char *name,const char *list,const char *optname)
{
	int no;

	no = mStringGetSplitTextIndex(name, -1, list, ';', FALSE);

	if(no == -1)
		puterr_exit("--%s: '%s' is undefined", optname, name);

	return no;
}


//==========================


/* --output */

static void _opt_output(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_work.str_output, arg, -1);

	//既存のディレクトリか
	
	if(mStrIsnotEmpty(&g_work.str_output)
		&& mIsExistDir(g_work.str_output.buf))
		g_work.flags |= FLAGS_OUTPUT_DIR;
}

/* --format */

static void _opt_format(mArgParse *p,char *arg)
{
	g_work.out_format = _get_keyword_index(arg, "png;bmp;psd", "format");
}

/* --blend */

static void _opt_blend(mArgParse *p,char *arg)
{
	g_work.proctype = PROCTYPE_BLEND;
}

/* --apdv4 */

static void _opt_apdv4(mArgParse *p,char *arg)
{
	g_work.proctype = PROCTYPE_APD;
}

/* --layer */

static void _opt_layer(mArgParse *p,char *arg)
{
	g_work.proctype = PROCTYPE_LAYER;
}

/* --info */

static void _opt_info(mArgParse *p,char *arg)
{
	g_work.proctype = PROCTYPE_INFO;
}

/* --texinfo */

static void _opt_texinfo(mArgParse *p,char *arg)
{
	g_work.proctype = PROCTYPE_TEXINFO;
}

/* --gray */

static void _opt_gray(mArgParse *p,char *arg)
{
	g_work.flags |= FLAGS_IMAGE_TO_GRAY;
}

/* --mono */

static void _opt_mono(mArgParse *p,char *arg)
{
	g_work.flags |= FLAGS_IMAGE_TO_MONO;
}

/* --to8bit */

static void _opt_to8bit(mArgParse *p,char *arg)
{
	g_work.flags |= FLAGS_APD_TO_8BIT;
}

/* --layer-prefix */

static void _opt_layer_prefix(mArgParse *p,char *arg)
{
	g_work.flags |= FLAGS_HAVE_LAYER_PREFIX;

	mStrSetText_locale(&g_work.str_layer_prefix, arg, -1);
}

/* --layerno */

static void _opt_layer_no(mArgParse *p,char *arg)
{
	const char *pc,*end,*p2;
	uint8_t *buf;
	int n1,n2,max;

	if(g_work.layerno_flags) return;

	//値のチェック & 最大値を取得

	pc = arg;
	max = -1;

	while(mStringGetNextSplit(&pc, &end, ','))
	{
		//'-' を検索
		
		for(p2 = pc; p2 < end && *p2 != '-'; p2++);

		//値を取得

		if(*p2 == '-' && p2 != pc)
		{
			n1 = atoi(pc);
			n2 = atoi(p2 + 1);
		}
		else
			n1 = n2 = atoi(pc);

		//範囲外

		if(n1 < 0 || n2 < 0 || n1 > n2)
			puterr_exit("--layerno: '%*s' is invalid", end - pc, pc);

		//値の最大値

		if(n2 > max) max = n2;
	
		pc = end;
	}

	//値がない

	if(max == -1)
		puterr_exit("--layerno: invalid value");

	//フラグバッファ確保

	g_work.layerno_flags = (uint8_t *)mMalloc0((max + 8) >> 3);
	if(!g_work.layerno_flags) return;

	g_work.layerno_num = max + 1;

	//フラグをセット

	pc = arg;
	buf = g_work.layerno_flags;

	while(mStringGetNextSplit(&pc, &end, ','))
	{
		for(p2 = pc; p2 < end && *p2 != '-'; p2++);

		if(*p2 != '-')
			n1 = n2 = atoi(pc);
		else
		{
			n1 = atoi(pc);
			n2 = atoi(p2 + 1);
		}

		for(; n1 <= n2; n1++)
			buf[n1 >> 3] |= 1 << (7 - (n1 & 7));
	
		pc = end;
	}
}

/* --tonegray */

static void _opt_tonegray(mArgParse *p,char *arg)
{
	TileImage_global_setToneToGray(TRUE);
}

/* --tex-sys */

static void _opt_texpath_sys(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_work.str_texpath_sys, arg, -1);
}

/* --tex-user */

static void _opt_texpath_user(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_work.str_texpath_user, arg, -1);
}

/* --version */

static void _opt_version(mArgParse *p,char *arg)
{
	puts(VERSION_TEXT);
	exit_app(0);
}

/* ヘルプ表示 */

static void _put_help(const char *exename)
{
	printf("Usage: %s [option] <files...>\n\n", exename);

	puts(
"APD(AzPainter)/ADW(AzDrawing) file tool.\n"
"\n"
"Options:\n"
"  <mode>\n"
"  -b,--blend   blend layers and output image [default]\n"
"  -c,--apdv4   output APD v4 file\n"
"  -l,--layer   output layer images\n"
"  -i,--info    display layer information\n"
"  -x,--texinfo display used texture path\n"
"\n"
"  <output>\n"
"  -o,--output=[DIR or NAME]  output directory or filename\n"
"        (default: current directory)\n"
"        <blend>\n"
"          if filename, determine the format from the extension\n"
"        <layer> always a directory\n"
"  -f,--format=[TYPE]  output image format (blend or layer)\n"
"        TYPE=png/bmp/psd (default:png)\n"
"  -G,--gray     output blend image to grayscale\n"
"  -M,--mono     output blend image to monochrome\n"
"  -t,--tonegray (APDv4) output tone layer to grayscale\n"
"  -D,--to8bit   converts 16-bit color to 8-bit during APD output\n"
"\n"
"  <layer output>\n"
"  -p,--layer-prefix=[PREFIX]\n"
"        output file name prefix\n"
"        default: '[INPUTFILENAME]_'\n"
"  -n,--layerno=[n,n1-n2,...]\n"
"        index number of layer to output\n"
"\n"
"  <others>\n"
"  -s,--tex-sys=[PATH]   system texture path\n"
"        default = /usr/local/share/azpainter3/texture\n"
"  -u,--tex-user=[PATH]  user texture path\n"
"        default = ~/.config/azpainter/texture\n"
"  -V,--version   print program version\n"
"  -h,--help      display this help"
);

	exit_app(0);
}

/* --help */

static void _opt_help(mArgParse *p,char *arg)
{
	_put_help(p->argv[0]);
}

/* オプション値 */

static mArgParseOpt g_opts[] = {
	{"blend",'b',0,_opt_blend},
	{"apdv4",'c',0,_opt_apdv4},
	{"layer",'l',0,_opt_layer},
	{"info",'i',0,_opt_info},
	{"texinfo",'x',0,_opt_texinfo},

	{"output",'o',1,_opt_output},
	{"format",'f',1,_opt_format},
	{"gray",'G',0,_opt_gray},
	{"mono",'M',0,_opt_mono},
	{"to8bit",'D',0,_opt_to8bit},

	{"layer-prefix",'p',1,_opt_layer_prefix},
	{"layerno",'n',1,_opt_layer_no},

	{"tonegray",'t',0,_opt_tonegray},
	{"tex-sys",'s',1,_opt_texpath_sys},
	{"tex-user",'u',1,_opt_texpath_user},

	{"version",'V',0,_opt_version},
	{"help",'h',0,_opt_help},
	{0,0,0,0}
};

/* 出力ファイル名の拡張子からフォーマット判別 */

static int _get_output_format(void)
{
	mStr ext = MSTR_INIT;
	int ret = OUTFORMAT_PNG;

	mStrPathGetExt(&ext, g_work.str_output.buf);

	if(mStrCompareEq_case(&ext, "png"))
		ret = OUTFORMAT_PNG;
	else if(mStrCompareEq_case(&ext, "bmp"))
		ret = OUTFORMAT_BMP;
	else if(mStrCompareEq_case(&ext, "psd"))
		ret = OUTFORMAT_PSD;

	mStrFree(&ext);

	return ret;
}

/** オプション取得
 *
 * return: 通常引数の位置 */

int get_app_options(int argc,char **argv)
{
	mArgParse ap;
	int ind;

	mMemset0(&ap, sizeof(mArgParse));

	ap.argc = argc;
	ap.argv = argv;
	ap.opts = g_opts;

	ind = mArgParseRun(&ap);

	//解析エラー

	if(ind == -1)
		exit_app(1);

	//入力ファイルがない

	if(ind == argc)
		_put_help(argv[0]);

	//合成画像出力で、--output が既存ディレクトリでない (ファイル指定) の場合、
	//拡張子からフォーマット判別

	if(g_work.proctype == PROCTYPE_BLEND
		&& mStrIsnotEmpty(&g_work.str_output)
		&& !(g_work.flags & FLAGS_OUTPUT_DIR))
	{
		g_work.out_format = _get_output_format();
	}

	return ind;
}

