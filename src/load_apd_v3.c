/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * APD v3 読み込み
 ************************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_zlib.h"
#include "mlk_stdio.h"
#include "mlk_tree.h"
#include "mlk_util.h"

#include "def.h"
#include "tileimage.h"
#include "pv_apd_format.h"


//----------------------

//レイヤ情報

typedef struct
{
	int32_t parent_no;	//親の番号 (-1 でルート)
	mRect rc;
	uint32_t blendid,
		col,
		flags;
	uint8_t coltype,
		channels,
		opacity,
		amask;
	char *name,
		*texpath;
}_layerinfo;

//apd

typedef struct
{
	FILE *fp;
	mZlib *zlib;

	uint8_t *tilebuf;	//作業用タイルバッファ
	_layerinfo *layerinfo;	//レイヤ情報データ

	int32_t layernum,
		layer_curno,
		width,
		height,
		dpi,
		to8bit;		//16bit->8bit 変換
	uint32_t thunk_size;	//チャンクのサイズ
	long thunk_toppos;		//チャンクの先頭位置
	fpos_t findthunk_toppos;	//チャンク検索の先頭位置

}_data;

//カラータイプ
enum
{
	APD3_COLTYPE_8BIT,
	APD3_COLTYPE_1BIT,
	APD3_COLTYPE_16BIT,
	APD3_COLTYPE_FOLDER = 255
};

//----------------------



//============================
// sub
//============================


/* 指定チャンクを探す
 *
 * return: 0 で見つかった。-1 で見つからなかった */

static mlkerr _goto_thunk(_data *p,uint32_t thunk_id)
{
	uint32_t id,size;

	fsetpos(p->fp, &p->findthunk_toppos);

	while(1)
	{
		//ID、サイズ
		
		if(mFILEreadBE32(p->fp, &id)
			|| mFILEreadBE32(p->fp, &size))
			break;
	
		if(id == thunk_id)
		{
			p->thunk_toppos = ftell(p->fp);
			p->thunk_size = size;

			return MLKERR_OK;
		}
		else if(id == _MAKE_ID('E','N','D',' '))
			break;

		//スキップ

		if(fseek(p->fp, size, SEEK_CUR))
			return MLKERR_DAMAGED;
	}

	return -1;
}

/* ヘッダの読み込み */

static mlkerr _read_header(_data *p)
{
	uint8_t unit;

	//情報

	if(mFILEreadFormatBE(p->fp, "iibi",
		&p->width, &p->height, &unit, &p->dpi))
		return MLKERR_DAMAGED;
		
	//チャンク先頭位置

	fgetpos(p->fp, &p->findthunk_toppos);

	//dpi

	if(unit != 1 || p->dpi < 1)
		p->dpi = 96;

	return MLKERR_OK;
}

/* レイヤ情報を解放 */

static void _free_layerinfo(_data *p)
{
	_layerinfo *pl;
	int i;

	pl = p->layerinfo;

	if(pl)
	{
		for(i = p->layernum; i; i--, pl++)
		{
			mFree(pl->name);
			mFree(pl->texpath);
		}
	
		mFree(p->layerinfo);
		p->layerinfo = NULL;
	}
}


//==========================
// 読み込み処理
//==========================


/* レイヤヘッダ (LYHD) 読み込み */

static mlkerr _read_layer_header(_data *p)
{
	uint16_t num,cur;
	mlkerr ret;

	ret = _goto_thunk(p, _MAKE_ID('L','Y','H','D'));
	if(ret) return ret;

	if(p->thunk_size < 4)
		return MLKERR_DAMAGED;

	mFILEreadBE16(p->fp, &num);
	mFILEreadBE16(p->fp, &cur);

	//レイヤ数が0
	
	if(num == 0) return MLKERR_INVALID_VALUE;

	//

	p->layernum = num;
	p->layer_curno = cur;

	//レイヤ情報バッファ

	p->layerinfo = (_layerinfo *)mMalloc0(sizeof(_layerinfo) * num);
	if(!p->layerinfo) return MLKERR_ALLOC;

	return MLKERR_OK;
}

/* レイヤツリー読み込み */

static mlkerr _read_layer_tree(_data *p)
{
	_layerinfo *pinfo;
	mlkerr ret;
	int i;
	uint16_t no;

	ret = _goto_thunk(p, _MAKE_ID('L','Y','T','R'));

	pinfo = p->layerinfo;

	if(ret == -1)
	{
		//チャンクがない場合、親はすべてルートとする

		for(i = p->layernum; i; i--, pinfo++)
			pinfo->parent_no = -1;
	}
	else if(ret)
		//エラー
		return ret;
	else
	{
		//先頭レイヤから順に、親のレイヤ番号 (2byte) が並んでいる。
		//0xffff でルート。

		if(p->thunk_size != p->layernum * 2)
			return MLKERR_DAMAGED;

		for(i = p->layernum; i; i--, pinfo++)
		{
			mFILEreadBE16(p->fp, &no);
			
			pinfo->parent_no = (no == 0xffff)? -1: no;
		}
	}

	return MLKERR_OK;
}

/* レイヤ情報の読み込みを開始 */

static mlkerr _begin_layerinfo(_data *p)
{
	uint16_t ver;
	mlkerr ret;

	ret = _goto_thunk(p, _MAKE_ID('L','Y','I','F'));
	if(ret) return ret;

	if(p->thunk_size < 2) return MLKERR_DAMAGED;

	//バージョン

	mFILEreadBE16(p->fp, &ver);

	if(ver != 0) return MLKERR_UNSUPPORTED;

	return MLKERR_OK;
}

/* レイヤ情報を読み込み */

static mlkerr _read_layer_info(_data *p)
{
	FILE *fp = p->fp;
	_layerinfo *pd;
	int i,n,imgbits;
	uint16_t exsize;
	uint32_t exid;
	uint8_t r,g,b;
	mlkerr ret;

	//----- レイヤ情報

	ret = _begin_layerinfo(p);
	if(ret) return ret;

	pd = p->layerinfo;
	imgbits = 8;

	for(i = p->layernum; i; i--, pd++)
	{
		//読み込み

		if(mFILEreadFormatBE(fp, "bbiiiibiibbbb",
			&pd->coltype, &pd->channels,
			&pd->rc.x1, &pd->rc.y1, &pd->rc.x2, &pd->rc.y2,
			&pd->opacity, &pd->blendid, &pd->flags, &pd->amask,
			&r, &g, &b))
			return MLKERR_DAMAGED;

		//カラータイプ

		n = pd->coltype;

		if(n >= 3 && n != 255)
			return MLKERR_UNSUPPORTED;

		//16bit カラー

		if(pd->coltype == 2) imgbits = 16;

		//チャンネル数

		n = pd->channels;

		if(pd->coltype != 255
			&& n != 1 && n != 2 && n != 4)
			return MLKERR_UNSUPPORTED;

		//不透明度

		if(pd->opacity > 128)
			pd->opacity = 128;

		//色

		pd->col = MLK_RGB(r,g,b);

		//拡張データ

		while(1)
		{
			//ID
			mFILEreadBE32(fp, &exid);
			if(exid == 0) break;

			//サイズ
			mFILEreadBE16(fp, &exsize);

			if(exid == _MAKE_ID('n','a','m','e'))
			{
				//名前

				if(exsize)
				{
					pd->name = (char *)mMalloc0(exsize + 1);
					
					fread(pd->name, 1, exsize, fp);
				}
			}
			else if(exid == _MAKE_ID('t','e','x','r'))
			{
				//テクスチャパス

				if(exsize)
				{
					pd->texpath = (char *)mMalloc0(exsize + 1);

					fread(pd->texpath, 1, exsize, fp);
				}
			}
			else
				fseek(fp, exsize, SEEK_CUR);
		}
	}

	//----- イメージ情報
	// :レイヤ情報をすべて読み込まないと、8bit/16bit を判断できない。
	// :A1bit または、フォルダのみの場合は、8bit として読み込む。

	set_image_info(p->width, p->height,
		(g_work.flags & FLAGS_APD_TO_8BIT)? 8: imgbits, p->dpi, p->layernum);

	g_work.img.srcbits = imgbits;

	//8bit 変換を行うか

	p->to8bit = (imgbits == 16 && (g_work.flags & FLAGS_APD_TO_8BIT));

	return MLKERR_OK;
}

/* レイヤを作成 */

static mlkerr _create_layers(_data *p)
{
	_layerinfo *info;
	LayerItem *pi;
	int i,j,coltype,type;
	uint32_t u32;

	info = p->layerinfo;

	for(i = p->layernum; i; i--, info++)
	{
		//レイヤ作成

		pi = layer_add_on_parent(info->parent_no);
		if(!pi) return MLKERR_ALLOC;

		coltype = info->coltype;

		//データ (共通)

		pi->name = mStrdup(info->name);
		pi->texpath = mStrdup(info->texpath);

		pi->opacity = info->opacity;
		pi->alphamask = info->amask;
		pi->col = info->col;
		pi->flags = info->flags;

		pi->is_folder = (coltype == APD3_COLTYPE_FOLDER);

		//フォルダ時

		if(coltype == APD3_COLTYPE_FOLDER)
			continue;

		//---- イメージレイヤ

		//レイヤタイプ

		if(info->channels == 4 && (coltype == APD3_COLTYPE_8BIT || coltype == APD3_COLTYPE_16BIT))
			//RGBA: 8,16bit
			type = LAYERTYPE_RGBA;
		else if(info->channels == 2 && coltype == APD3_COLTYPE_16BIT)
			//GRAY: 16bit
			type = LAYERTYPE_GRAY;
		else
		{
			if(coltype == APD3_COLTYPE_16BIT)
				//A16
				type = LAYERTYPE_ALPHA;
			else if(coltype == APD3_COLTYPE_1BIT)
				//A1
				type = LAYERTYPE_ALPHA1BIT;
			else
				return MLKERR_INVALID_VALUE;
		}

		//合成モード

		u32 = info->blendid;

		for(j = 0; g_blendmode_id[j]; j++)
		{
			if(u32 == g_blendmode_id[j])
			{
				pi->blendmode = j;
				break;
			}
		}

		//イメージ作成

		if(PROC_IS_NEEDIMAGE)
		{
			pi->img = TileImage_newFromRect_forFile(type, &info->rc);
			if(!pi->img) return MLKERR_ALLOC;

			pi->type = type;

			TileImage_setColor(pi->img, pi->col);
		}
	}

	return MLKERR_OK;
}

/* レイヤタイルの読み込み開始 */

static mlkerr _begin_layer_tile(_data *p)
{
	uint16_t tsize;
	uint8_t type;
	mlkerr ret;

	//壊れている場合でも、途中まで読み込む

	ret = _goto_thunk(p, _MAKE_ID('L','Y','T','L'));
	if(ret) return ret;

	if(p->thunk_size < 3)
		return MLKERR_DAMAGED;

	mFILEreadBE16(p->fp, &tsize); //タイルのpxサイズ
	mFILEreadByte(p->fp, &type);  //圧縮タイプ

	if(tsize != 64 || type != 0)
		return MLKERR_UNSUPPORTED;

	return MLKERR_OK;
}

/* 各レイヤのタイルイメージを読み込み (フォルダの分も含む) */

static mlkerr _read_layer_tile(_data *p,LayerItem *item)
{
	TileImage *img;
	uint8_t *tilebuf;
	uint32_t num,encsize;
	int tx,ty,tilesize;
	mlkerr ret;

	//タイル数、圧縮サイズ

	if(mFILEreadBE32(p->fp, &num)
		|| mFILEreadBE32(p->fp, &encsize))
		return MLKERR_DAMAGED;

	//タイルなし

	if(num == 0)
		return MLKERR_OK;

	//タイルがあるのにイメージがない場合、エラー

	img = item->img;
	if(!img) return MLKERR_INVALID_VALUE;

	//------ タイル

	tilebuf = p->tilebuf;

	if(p->to8bit)
		tilesize = TileImage_global_getTileSize(img->type, 16);
	else
		tilesize = img->tilesize;

	mZlibDecReset(p->zlib);
	mZlibDecSetSize(p->zlib, encsize);

	for(; num; num--)
	{
		//タイル位置 + タイルイメージ

		ret = mZlibDecRead(p->zlib, tilebuf, tilesize + 4);
		if(ret) return ret;

		tx = mGetBufBE16(tilebuf);
		ty = mGetBufBE16(tilebuf + 2);

		//セット

		ret = TileImage_allocTile_fromSave_APDv3(img, tx, ty, tilebuf + 4, p->to8bit);
		if(ret) return ret;
	}

	return mZlibDecFinish(p->zlib);
}


//=============================
// main
//=============================


/* メイン処理 */

static mlkerr _main_proc(_data *p)
{
	LayerItem *pi;
	mlkerr ret;

	//ヘッダ

	ret = _read_header(p);
	if(ret) return ret;

	//レイヤヘッダ

	ret = _read_layer_header(p);
	if(ret) return ret;

	//レイヤツリー

	ret = _read_layer_tree(p);
	if(ret) return ret;

	//レイヤ情報

	ret = _read_layer_info(p);
	if(ret) return ret;

	//レイヤ作成

	ret = _create_layers(p);
	if(ret) return ret;

	_free_layerinfo(p);

	//レイヤタイル

	if(PROC_IS_NEEDIMAGE)
	{
		ret = _begin_layer_tile(p);
		if(ret) return ret;

		pi = (LayerItem *)g_work.list_layer.top;

		for(; pi; pi = (LayerItem *)mTreeItemGetNext(MTREEITEM(pi)))
		{
			ret = _read_layer_tile(p, pi);
			if(ret) return ret;
		}
	}

	return MLKERR_OK;
}

/** APD v3 読み込み */

mlkerr load_apd_v3(FILE *fp)
{
	_data dat;
	mlkerr ret;

	mMemset0(&dat, sizeof(_data));

	dat.fp = fp;

	ret = init_zlib(&dat.zlib, fp, FALSE);
	if(ret) return ret;

	if(PROC_IS_NEEDIMAGE)
	{
		dat.tilebuf = (uint8_t *)mMalloc(64 * 64 * 2 * 4 + 4);
		if(!dat.tilebuf)
		{
			ret = MLKERR_ALLOC;
			goto ERR;
		}
	}

	//

	ret = _main_proc(&dat);

	//

ERR:
	mZlibFree(dat.zlib);
	mFree(dat.tilebuf);
	_free_layerinfo(&dat);

	return ret;
}

