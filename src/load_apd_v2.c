/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * APD ver2 読み込み
 *****************************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_zlib.h"
#include "mlk_stdio.h"

#include "def.h"
#include "tileimage.h"


//------------------

typedef struct
{
	FILE *fp;
	mZlib *zlib;
	uint8_t *tilebuf;
	int to8bit;
}_data;

//--------------



/* レイヤイメージ読み込み */

static mlkerr _read_layer_image(_data *p,TileImage *img,uint32_t tilenum)
{
	FILE *fp = p->fp;
	uint8_t *tilebuf;
	uint32_t pos,size,tilesize;
	int tilew;
	mlkerr ret;

	tilew = img->tilew;
	tilebuf = p->tilebuf;

	if(p->to8bit)
		tilesize = TileImage_global_getTileSize(img->type, 16);
	else
		tilesize = img->tilesize;

	for(; tilenum; tilenum--)
	{
		//タイル情報
		
		if(mFILEreadFormatBE(fp, "ii", &pos, &size)
			|| size > tilesize)
			return MLKERR_DAMAGED;

		//タイル読み込み

		if(size == tilesize)
		{
			if(mFILEreadOK(fp, tilebuf, size))
				return MLKERR_DAMAGED;
		}
		else
		{
			mZlibDecReset(p->zlib);

			ret = mZlibDecReadOnce(p->zlib, tilebuf, tilesize, size);
			if(ret) return ret;
		}

		//タイル確保 & セット

		ret = TileImage_allocTile_fromSave(img, pos % tilew, pos / tilew, tilebuf, p->to8bit);
		if(ret) return ret;
	}

	return MLKERR_OK;
}

/* タイルイメージスキップ */

static mlkerr _skip_tiles(FILE *fp,uint32_t tilenum)
{
	uint32_t pos,size;

	for(; tilenum; tilenum--)
	{
		if(mFILEreadFormatBE(fp, "ii", &pos, &size)
			|| fseek(fp, size, SEEK_CUR))
			return MLKERR_DAMAGED;
	}

	return MLKERR_OK;
}

/* レイヤフラグ変換 */

static uint32_t _convert_layerflags(uint32_t flags)
{
	uint32_t f = 0;

	if(flags & (1<<0))
		f |= LAYERITEM_F_VISIBLE;
	
	if(flags & (1<<1))
		f |= LAYERITEM_F_FOLDER_OPENED;

	if(flags & (1<<2))
		f |= LAYERITEM_F_LOCK;

	if(flags & (1<<3))
		f |= LAYERITEM_F_FILLREF;

	if(flags & (1<<5))
		f |= LAYERITEM_F_MASK_UNDER;

	if(flags & (1<<7))
		f |= LAYERITEM_F_CHECKED;

	return f;
}

/* レイヤ読み込み
 *
 * [!] 上層のレイヤから順に並んでいる */

static mlkerr _read_layer(FILE *fp,_data *p)
{
	LayerItem *pl;
	char *name;
	uint8_t tflags,coltype,opacity,blendmode,amask,is_folder;
	uint16_t parent,tilew,tileh;
	uint32_t col,flags,tilenum;
	int32_t offx,offy,i;
	mRect rc;
	mlkerr ret;

	for(i = g_work.img.layernum; i; i--)
	{
		//親のレイヤ位置
		// :0xfffe で終了、0xffff でルート

		if(mFILEreadBE16(fp, &parent))
			return MLKERR_DAMAGED;

		if(parent == 0xfffe) break;

		//情報

		name = NULL;

		if(mFILEreadByte(fp, &tflags)
			|| mFILEreadStr_variable(fp, &name) < 0
			|| mFILEreadFormatBE(fp, "bbbbii", &coltype, &opacity, &blendmode, &amask, &col, &flags)
			|| coltype > 3)
		{
			mFree(name);
			return MLKERR_DAMAGED;
		}

		is_folder = (tflags & 1);

		//イメージ情報

		if(!is_folder)
		{
			if(mFILEreadFormatBE(fp, "iihhi",
				&offx, &offy, &tilew, &tileh, &tilenum))
			{
				mFree(name);
				return MLKERR_DAMAGED;
			}
		}

		//レイヤ追加

		pl = layer_add_on_parent((parent == 0xffff)? -1: parent);
		if(!pl)
		{
			mFree(name);
			return MLKERR_ALLOC;
		}

		//情報セット

		pl->name = name;
		pl->type = coltype;
		pl->opacity = opacity;
		pl->blendmode = (blendmode >= 17)? 0: blendmode;
		pl->alphamask = amask;
		pl->col = col;
		pl->flags = _convert_layerflags(flags);

		pl->is_folder = is_folder;

		//タイルイメージ

		if(!is_folder)
		{
			if(PROC_IS_SKIPIMAGE)
			{
				ret = _skip_tiles(fp, tilenum);
				if(ret) return ret;
			}
			else
			{
				//イメージ作成

				if(tilenum == 0)
				{
					rc.x1 = rc.y1 = 0;
					rc.x2 = rc.y2 = 0;
				}
				else
				{
					rc.x1 = offx;
					rc.y1 = offy;
					rc.x2 = offx + tilew * 64;
					rc.y2 = offy + tileh * 64;
				}

				pl->img = TileImage_newFromRect_forFile(coltype, &rc);
				if(!pl->img) return MLKERR_ALLOC;

				TileImage_setColor(pl->img, col);

				//読み込み

				ret = _read_layer_image(p, pl->img, tilenum);
				if(ret) return ret;
			}
		}
	}

	return MLKERR_OK;
}

/* 読み込みメイン */

static mlkerr _main_proc(FILE *fp,_data *p)
{
	uint8_t sig,finfo = 0;
	uint32_t size;
	uint16_t imgw,imgh,dpi;
	mlkerr ret;

	while(mFILEreadByte(fp, &sig) == 0)
	{
		if(sig == 255) break;
		
		if(sig == 'L')
		{
			//---- レイヤ

			if(!finfo) return MLKERR_INVALID_VALUE;

			//レイヤ数

			if(mFILEreadBE16(fp, &imgw))
				return MLKERR_DAMAGED;

			g_work.img.layernum = imgw;

			//レイヤ読み込み

			ret = _read_layer(fp, p);
			if(ret) return ret;
		}
		else
		{
			if(mFILEreadBE32(fp, &size))
				return MLKERR_DAMAGED;

			if(sig == 'I' && !finfo)
			{
				//イメージ情報

				if(mFILEreadFormatBE(fp, "hhh", &imgw, &imgh, &dpi))
					return MLKERR_DAMAGED;

				set_image_info(imgw, imgh,
					(g_work.flags & FLAGS_APD_TO_8BIT)? 8: 16, dpi, -1);

				g_work.img.srcbits = 16;

				p->to8bit = (g_work.flags & FLAGS_APD_TO_8BIT);

				//

				finfo = 1;
				size -= 6;
			}

			if(fseek(fp, size, SEEK_CUR))
				return MLKERR_DAMAGED;
		}
	}

	return MLKERR_OK;
}

/** APD v2 読み込み (BigEndian) */

mlkerr load_apd_v2(FILE *fp)
{
	_data dat;
	mlkerr ret;

	mMemset0(&dat, sizeof(_data));

	dat.fp = fp;

	ret = init_zlib(&dat.zlib, fp, FALSE);
	if(ret) return ret;

	if(PROC_IS_NEEDIMAGE)
	{
		dat.tilebuf = (uint8_t *)mMalloc(64 * 64 * 2 * 4);
		if(!dat.tilebuf)
		{
			ret = MLKERR_ALLOC;
			goto ERR;
		}
	}

	//

	ret = _main_proc(fp, &dat);

	//

ERR:
	mZlibFree(dat.zlib);
	mFree(dat.tilebuf);

	return ret;
}
