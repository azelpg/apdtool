/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * 合成モード
 *****************************/

typedef mlkbool (*BlendColorFunc)(int32_t *src,int32_t *dst,int a);

enum
{
	BLENDMODE_NORMAL,
	BLENDMODE_MUL,
	BLENDMODE_ADD,
	BLENDMODE_SUB,
	BLENDMODE_SCREEN,
	BLENDMODE_OVERLAY,
	BLENDMODE_HARD_LIGHT,
	BLENDMODE_SOFT_LIGHT,
	BLENDMODE_DODGE,
	BLENDMODE_BURN,
	BLENDMODE_LINEAR_BURN,
	BLENDMODE_VIVID_LIGHT,
	BLENDMODE_LINEAR_LIGHT,
	BLENDMODE_PIN_LIGHT,
	BLENDMODE_DARKER,
	BLENDMODE_LIGHTEN,
	BLENDMODE_DIFF,
	BLENDMODE_LUM_ADD,
	BLENDMODE_LUM_DODGE,

	//APDv1
	BLENDMODE_LUM_SUB,
	BLENDMODE_HUE,
	BLENDMODE_SATURATION,
	BLENDMODE_COLOR,
	BLENDMODE_LUM,

	BLENDMODE_FULL_NUM,
	BLENDMODE_APDv4_NUM = BLENDMODE_LUM_DODGE + 1
};

