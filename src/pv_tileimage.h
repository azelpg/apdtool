/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * TileImage 内部定義
 ************************************/

#include "blendmode.h"

#define COLCONV_16TO8(n)  (((n) * 255 + 0x4000) >> 15)

typedef struct _TileImageBlendInfo TileImageBlendInfo;

typedef void (*TileImageColFunc_toRGBA8)(TileImage *p,uint8_t **ppbuf,int dx,int w,int h,uint8_t *tile,int sx,int sy);
typedef mlkbool (*TileImageColFunc_isTransparentTile)(uint8_t *tile);
typedef void (*TileImageColFunc_blendTile)(TileImage *p,TileImageBlendInfo *info);
typedef void (*TileImageColFunc_convert_forSave)(uint8_t *dst,uint8_t *src);
typedef void (*TileImageColFunc_convert_16to8bit)(uint8_t *dst,uint8_t *src);

/* カラータイプ用関数データ */

typedef struct
{
	TileImageColFunc_toRGBA8 to_rgba8;
	TileImageColFunc_isTransparentTile is_transparent_tile;
	TileImageColFunc_blendTile blend_tile;
	TileImageColFunc_convert_forSave convert_from_save,
		convert_to_save;
	TileImageColFunc_convert_16to8bit convert_16to8;
}TileImageColFuncData;

/* 作業用データ */

typedef struct
{
	int bits,
		dpi,
		is_tone_to_gray;	//トーンをグレイスケール化
	TileImageColFuncData colfunc[4];	//各カラータイプの関数 (現在のビット用)
	BlendColorFunc blendfunc[BLENDMODE_FULL_NUM];	//色合成関数 (現在のビット用)
}TileImageWorkData;

extern TileImageWorkData *g_tileimgwork;

/* 合成用情報 */

struct _TileImageBlendInfo
{
	uint8_t *tile,	//タイルバッファ
		**dstbuf;	//キャンバスバッファ (Y位置)
	TexItem *texitem;	//テクスチャイメージ
	int sx,sy,			//タイル内の開始位置 (64x64 内)
		dx,dy,			//描画先の開始位置 (px 位置)
		w,h,			//処理幅
		opacity,		//不透明度 (0-128)
		tone_repcol,	//トーンをグレイスケール表示時、置き換える色 (-1 で置き換えない)
		tone_density;	//トーン:固定濃度 (0でなし、MAX=255 or 0x8000)
	uint8_t is_tone,	//トーン化あり
		is_tone_bkgnd_tp;	//トーン:背景は透明
	int64_t tone_fx,tone_fy,	//タイル開始位置でのセル位置
		tone_fcos,tone_fsin;
	BlendColorFunc func_blend;
};

#define TILEIMG_TONE_FIX_BITS  28
#define TILEIMG_TONE_FIX_VAL   (1 << TILEIMG_TONE_FIX_BITS)

