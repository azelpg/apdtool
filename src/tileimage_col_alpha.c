/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * TileImage
 * アルファ値のみタイプ
 *****************************************/

#include <string.h>

#include "mlk.h"

#include "tileimage.h"
#include "pv_tileimage.h"


int TextureItem_getOpacity(TexItem *pi,int x,int y);


//==========================
// 8bit
//==========================


/** すべて透明か */

static mlkbool _8bit_is_transparent_tile(uint8_t *tile)
{
	return TileImage_tile_isTransparent_forA(tile, 64 * 64);
}

/** 合成 */

static void _8bit_blend_tile(TileImage *p,TileImageBlendInfo *infosrc)
{
	TileImageBlendInfo info;
	uint8_t **ppdst,*ps,*pd;
	int pitchs,ix,iy,dx,dy,a,dstx;
	int32_t src[3],dst[3],r,g,b;

	info = *infosrc;

	ps = info.tile + info.sy * 64 + info.sx;
	ppdst = info.dstbuf;
	dstx = info.dx * 4;
	pitchs = 64 - info.w;

	r = p->col.c8.r;
	g = p->col.c8.g;
	b = p->col.c8.b;

	//

	for(iy = info.h, dy = info.dy; iy; iy--, dy++)
	{
		pd = *ppdst + dstx;
		
		for(ix = info.w, dx = info.dx; ix; ix--, dx++, ps++, pd += 4)
		{
			a = *ps;
			if(!a) continue;

			if(info.texitem)
				a = a * TextureItem_getOpacity(info.texitem, dx, dy) / 255;

			a = a * info.opacity >> 7;
			if(!a) continue;

			//色合成

			src[0] = r;
			src[1] = g;
			src[2] = b;

			dst[0] = pd[0];
			dst[1] = pd[1];
			dst[2] = pd[2];

			if((info.func_blend)(src, dst, a) && a != 255)
			{
				//アルファ合成

				src[0] = (src[0] - dst[0]) * a / 255 + dst[0];
				src[1] = (src[1] - dst[1]) * a / 255 + dst[1];
				src[2] = (src[2] - dst[2]) * a / 255 + dst[2];
			}

			//セット

			pd[0] = src[0];
			pd[1] = src[1];
			pd[2] = src[2];
		}

		ps += pitchs;
		ppdst++;
	}
}

/** (レイヤ画像用) タイルを、RGBA 8bit イメージに変換 */

static void _8bit_to_rgba8(TileImage *p,uint8_t **ppbuf,int dx,int w,int h,uint8_t *tile,int sx,int sy)
{
	uint32_t *pd;
	uint8_t *ps,col[4];
	int ix,iy,pitchs;

	ps = tile + (sy * 64 + sx);
	pitchs = 64 - w;

	col[0] = p->col.c8.r;
	col[1] = p->col.c8.g;
	col[2] = p->col.c8.b;

	for(iy = h; iy; iy--)
	{
		pd = (uint32_t *)*ppbuf + dx;
	
		for(ix = w; ix; ix--)
		{
			col[3] = *(ps++);
		
			*(pd++) = *((uint32_t *)col);
		}

		ps += pitchs;
		ppbuf++;
	}
}

/** ファイル保存用タイルデータの変換 (読み込み/保存共通) */

static void _8bit_convert_save(uint8_t *dst,uint8_t *src)
{
	memcpy(dst, src, 64 * 64);
}


//==========================
// 16bit
//==========================


/** すべて透明か */

static mlkbool _16bit_is_transparent_tile(uint8_t *tile)
{
	return TileImage_tile_isTransparent_forA(tile, 64 * 64 * 2);
}

/** ImageCanvas に合成 */

static void _16bit_blend_tile(TileImage *p,TileImageBlendInfo *infosrc)
{
	TileImageBlendInfo info;
	uint16_t **ppdst,*ps,*pd;
	int pitchs,ix,iy,dx,dy,a,dstx;
	int32_t src[3],dst[3],r,g,b;

	info = *infosrc;

	ps = (uint16_t *)info.tile + info.sy * 64 + info.sx;
	ppdst = (uint16_t **)info.dstbuf;
	dstx = info.dx * 4;
	pitchs = 64 - info.w;

	r = p->col.c16.r;
	g = p->col.c16.g;
	b = p->col.c16.b;
	
	//

	for(iy = info.h, dy = info.dy; iy; iy--, dy++)
	{
		pd = *ppdst + dstx;
		
		for(ix = info.w, dx = info.dx; ix; ix--, dx++, ps++, pd += 4)
		{
			a = *ps;
			if(!a) continue;

			if(info.texitem)
				a = a * TextureItem_getOpacity(info.texitem, dx, dy) / 255;

			a = a * info.opacity >> 7;

			if(!a) continue;

			//RGB 合成

			src[0] = r;
			src[1] = g;
			src[2] = b;

			dst[0] = pd[0];
			dst[1] = pd[1];
			dst[2] = pd[2];

			if((info.func_blend)(src, dst, a) && a != 0x8000)
			{
				//アルファ合成

				src[0] = ((src[0] - dst[0]) * a >> 15) + dst[0];
				src[1] = ((src[1] - dst[1]) * a >> 15) + dst[1];
				src[2] = ((src[2] - dst[2]) * a >> 15) + dst[2];
			}

			//セット

			pd[0] = src[0];
			pd[1] = src[1];
			pd[2] = src[2];
		}

		ps += pitchs;
		ppdst++;
	}
}

/** (レイヤ画像用) タイルを、RGBA 8bit イメージに変換 */

static void _16bit_to_rgba8(TileImage *p,uint8_t **ppbuf,int dx,int w,int h,uint8_t *tile,int sx,int sy)
{
	uint32_t *pd;
	uint16_t *ps;
	uint8_t col[4];
	int ix,iy,pitchs;

	ps = (uint16_t *)tile + (sy * 64 + sx);
	pitchs = 64 - w;

	col[0] = p->col.c8.r;
	col[1] = p->col.c8.g;
	col[2] = p->col.c8.b;

	for(iy = h; iy; iy--)
	{
		pd = (uint32_t *)*ppbuf + dx;
	
		for(ix = w; ix; ix--, ps++)
		{
			col[3] = COLCONV_16TO8(*ps);
		
			*(pd++) = *((uint32_t *)col);
		}

		ps += pitchs;
		ppbuf++;
	}
}

/** ファイル保存用タイルデータからタイルセット
 *
 * 64x64 分 BE で並んでいる */

static void _16bit_convert_from_save(uint8_t *dst,uint8_t *src)
{
	uint16_t *pd = (uint16_t *)dst;
	int i;

	for(i = 64 * 64; i; i--, pd++, src += 2)
		*pd = (src[0] << 8) | src[1];
}

/** ファイル保存用にタイルデータを変換 (BE) */

static void _16bit_convert_to_save(uint8_t *dst,uint8_t *src)
{
	int i;
	uint16_t *ps;

	ps = (uint16_t *)src;

	for(i = 64 * 64; i; i--, ps++, dst += 2)
	{
		dst[0] = *ps >> 8;
		dst[1] = *ps & 255;
	}
}

/** ファイル保存用タイルデータから 16bit->8bit 変換してセット */

static void _convert_16to8(uint8_t *dst,uint8_t *src)
{
	int i;

	for(i = 64 * 64; i; i--, src += 2)
	{
		*(dst++) = COLCONV_16TO8((src[0] << 8) | src[1]);
	}
}


//==========================
// 関数をセット
//==========================


/** 関数をセット */

void __TileImage_setColFunc_alpha(TileImageColFuncData *p,int bits)
{
	p->convert_16to8 = _convert_16to8;

	if(bits == 8)
	{
		p->to_rgba8 = _8bit_to_rgba8;
		p->is_transparent_tile = _8bit_is_transparent_tile;
		p->blend_tile = _8bit_blend_tile;
		p->convert_from_save = _8bit_convert_save;
		p->convert_to_save = _8bit_convert_save;
	}
	else
	{
		p->to_rgba8 = _16bit_to_rgba8;
		p->is_transparent_tile = _16bit_is_transparent_tile;
		p->blend_tile = _16bit_blend_tile;
		p->convert_from_save = _16bit_convert_from_save;
		p->convert_to_save = _16bit_convert_to_save;
	}
}


