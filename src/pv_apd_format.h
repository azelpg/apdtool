/*$
apdtool
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/************************************
 * APD 読み書き 内部用 (v3,v4 共通)
 ************************************/

#define _MAKE_ID(a,b,c,d)  ((a << 24) | (b << 16) | (c << 8) | d)

//合成モードID
static uint32_t g_blendmode_id[] = {
	_MAKE_ID('n','o','r','m'), _MAKE_ID('m','u','l',' '), _MAKE_ID('a','d','d',' '), _MAKE_ID('s','u','b',' '),
	_MAKE_ID('s','c','r','n'), _MAKE_ID('o','v','r','l'), _MAKE_ID('h','d','l','g'), _MAKE_ID('s','t','l','g'),
	_MAKE_ID('d','o','d','g'), _MAKE_ID('b','u','r','n'), _MAKE_ID('l','b','u','n'), _MAKE_ID('v','i','v','l'),
	_MAKE_ID('l','i','n','l'), _MAKE_ID('p','i','n','l'), _MAKE_ID('l','i','g','t'), _MAKE_ID('d','a','r','k'),
	_MAKE_ID('d','i','f','f'), _MAKE_ID('l','m','a','d'), _MAKE_ID('l','m','d','g'), 0
};

