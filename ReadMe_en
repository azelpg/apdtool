**********************************************
  apdtool
  Copyright (c) 2020-2022 Azel

  http://azsky2.html.xdomain.jp/
  https://gitlab.com/azelpg/apdtool
  <azelpg@gmail.com>
**********************************************

This software is released under the MIT License.
Please see the file COPYING for details.


=====================================
 * Overview
=====================================

APD(AzPaineter)/ADW(AzDrawing) file tool.

* If a layer texture is used, a separate texture image is required.
  In that case, please specify the directory of the texture image in the option.


=====================================
 * Operating environment
=====================================

Linux, macOS others


=====================================
 * What you need to compile
=====================================

- C compiler
- ninja (build tool)
- pkg-config
- development files for various libraries:
  zlib libpng
  other than linux: libiconv


--- Required packages  ---

(Debian/Ubuntu)

  gcc or clang, ninja-build pkg-config zlib1g-dev libpng-dev

(RedHat)

  gcc or clang, ninja-build pkg-config zlib-devel libpng-devel

(Arch Linux)

  gcc or clang, ninja pkgconf libpng

(macOS)

  xcode ninja pkg-config libpng


=====================================
 * Compile and install
=====================================

After extracting the archive, go to the extracted directory and do the following.

$ ./configure
$ cd build
$ ninja
# ninja install


- By default, it will be installed under "/usr/local".
- You can get help with $ ./configure --help.

## Install in /usr
$ ./configure --prefix=/usr

## Uninstall (build/)
# ninja uninstall

## Generate files for package file (build/)
$ DESTDIR=<DIR> ninja install


=====================================
 * Command line options
=====================================

$ apdtool [opt] <FILES(APD/APW)...>


You can specify multiple input files (APD/ADW).

By default, the blend image is output to the current directory in PNG format.


<Mode>

* If multiple modes are specified at the same time, the last optional mode will be valid.

-b,--blend

    Outputs a single picture image that combines layers. (Default)

-c, --apdv4

    Converts files other than APD v4 to APD v4 and outputs them.
    (APD v4 files will be skipped)

    * If the input is APD and the output destination is the current directory, it will be overwritten.

-l,--layer

    Output each layer as an independent image in RGBA color.
    By default, all layers except folders are targeted.

    By default, the output file name is "input file name + '_' + serial number + extension".

-i,--info

    Only the information of each layer is output.

-x,--texinfo

    Only the path of the layer texture image file is output.
    If there are no layer textures, nothing will be displayed.


<Output>

-o,--output=[DIR or FILENAME]

    Specify the image output directory or output file name.
    Output to the current directory by default.

    << --blend, --apdv4 >>
    If the specified path is not the path of an existing directory,
    it will be treated as the output file name.
    In that case, when --blend, the output format is determined
    from the extension of the specified file name.
    If you do not specify an output file name,
    the output file name will be "input file name + image extension".
 
    << --layer >>
    The directory is always specified.
 
-f,--format=[png|bmp|psd]

    Specify the output image format for --blend and --layer.
    default:png

-G,--gray

    When outputting PNG/PSD with --blend, it is output in grayscale color.

-M,--mono

    When outputting PNG/PSD with --blend, it is output in monochrome color (1bit).

-t,--tonegray

    When --blend, grayscale the tone layer (at APD v4).

-D,--to8bit

    When --apdv4, 16bit color is converted to 8bit color and output (APD v2,v3)


<Layer output>

-p,--layer-prefix=[PREFIX]

    Specify the prefix of the output file name when outputting layers.
    * Specify the output directory with --output.

    The output file name will be "PREFIX + serial number + extension".
    If you specify an empty string with '-p ""', you can use only the serial number.

-n,--layerno=[n,n1-n2,...]

    When outputting a layer, specify multiple index numbers of the target layer.

    The numbers start at 0 and can be specified as a single unit,
    such as "0,1,2", or as a range, such as "0-5,10-20".
    Separate each value with ','.

    The layer index number can be checked with --info.


<Layer texture>

-s,--tex-sys=[PATH]

    Specify the directory path of the system texture image.
    The default is "/usr/local/share/azpainter3/texture".

-u,--tex-user=[PATH]

    Specify the directory path of the texture image of the user.
    The default is "~/.azpainter/texture".


<Others>

-V,--version

    Displays version information.

-h,--help

    Displays help.
